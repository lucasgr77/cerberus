from sqlalchemy import Column, Integer, String, Float, DateTime, Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from .settings import Base


class ControleRequest(Base):
    __tablename__ = 'controle_request'

    id = Column(Integer, primary_key=True)
    metodo_origem = Column(Text)
    tipo = Column(Text)
    url = Column(Text)
    payload_enviado = Column(Text)
    payload_recebido = Column(Text)
    data_hora = Column(DateTime)

    def __init__(self, metodo_origem, tipo, url, payload_enviado, payload_recebido, data_hora):
            self.metodo_origem = metodo_origem
            self.tipo = tipo
            self.url = url
            self. payload_enviado = payload_enviado
            self.payload_recebido = payload_recebido
            self.data_hora = data_hora
 