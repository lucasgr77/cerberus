# encoding: utf-8
import os
import datetime
from source.precificacao.mercado_livre import (
    get_cadastro_ml
)
from source.api import req_mercado_livre, MlException
from source.catalogo_mercado_livre.models import tem_variacao_obrigatoria, salvar
from multiprocessing.pool import ThreadPool as Pool
from sqlalchemy import and_, func
from .. import log_server, extensions
from . import models as model
from math import ceil
from .. import extensions as ext

URL_CATALOGO = 'sites/MLB/search?seller_id=307700862&offset='


def download_catalogo():
    mlids = get_todos_mlids()
    pool = Pool(5)
    promises = []

    # request
    for mlid in mlids:
        async_call = pool.apply_async(async_salva_produto_ml, (mlid , ))
        promises.append(async_call)
    pool.close()
    pool.join()

    for promise in promises:
        promise.get()


def get_todos_mlids():
    pool = Pool(10)
    promises = []
    url_base = os.getenv('MERCADOLIVRE_URL_BASE')
    offset = 0
    url = url_base + URL_CATALOGO + str(offset)
    response = req_mercado_livre(url)
    if 'message' in response:
        raise MlException(response['message'])
    total_produtos = response['paging']['total']
    count_requests = ceil(float(total_produtos) / 50)
    mlids = list(map(lambda x: x['id'], response['results']))

    # request
    for x in range(int(count_requests)):
        x += 1
        offset = x * 50
        async_call = pool.apply_async(async_req_pagina_catalogo, (offset, ))
        promises.append(async_call)
    pool.close()
    pool.join()

    # retrieve
    for promise in promises:
        mlids.append(map(lambda x: x, promise.get()))

    return flatten_list(mlids)


def async_req_pagina_catalogo(offset):
    url_base = os.getenv('MERCADOLIVRE_URL_BASE')
    url = url_base + URL_CATALOGO + str(offset)
    response = req_mercado_livre(url)
    if 'message' in response:
        raise MlException(response['message'])
    mlids = list(map(lambda x: x['id'], response['results']))
    return mlids


def async_salva_produto_ml(mlid):
    try:
        cadastro = get_cadastro_ml(mlid)
        tem_variacao = len(cadastro['variations']) > 0
        variacoes = []
        if tem_variacao:
            variacoes = list(map(lambda x: x['id'], cadastro['variations']))
        model.salvar(mlid, get_sku_from_cadastro(cadastro), variacoes)
    except Exception as ex:
        ext.exception_notifier(ex)


def get_sku_from_cadastro(cadastro):
    SELLER_SKU = 'SELLER_SKU'
    PART_NUMBER = 'PART_NUMBER'
    MODEL = 'MODEL'
    sku = None
    if 'attributes' in cadastro:
        for atributo in cadastro['attributes']:
            if atributo['id'] == SELLER_SKU:
                sku = atributo['value_name']
            elif atributo['id'] == PART_NUMBER and sku is None:
                sku = atributo['value_name']
            elif atributo['id'] == MODEL and sku is None:
                sku = atributo['value_name']
    return sku


def flatten_list(_2d_list):
    flat_list = set()
    # Iterate through the outer list
    for element in _2d_list:
        if type(element) is list:
            # If the element is of type list, iterate through the sublist
            for item in element:
                flat_list.add(item)
        else:
            flat_list.add(element)
    return flat_list
