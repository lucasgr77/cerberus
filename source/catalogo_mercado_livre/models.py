from source.database import LocalStorage
from ..produtos.models import ProdutoTiny
from sqlalchemy import Column, Integer, String, Float, String, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from ..settings import Base


class MlCadastro(Base):
    __tablename__ = 'ml_cadastro'
    mlid = Column(String, primary_key=True)
    sku = Column(String)
    variacoes = relationship("MlVariacao")

    def __init__(self, mlid, sku):
        self.mlid = mlid
        self.sku = sku
        self.variacoes = []

    def __repr__(self):
        return self.mlid

    def igual_variacoes(self, outras_variacoes):
        if len(self.variacoes) != len(outras_variacoes):
            return False
        comparacoes = []
        for variacao_salva in self.variacoes:
            for outra_variacao in outras_variacoes:
                if variacao_salva.id_variacao == outras_variacoes:
                    comparacoes.append(True)
            if len(comparacoes) == 0:
                return False
        return len(comparacoes) == len(self.variacoes)


class MlVariacao(Base):
    __tablename__ = 'ml_variacao'
    mlid = Column(String, ForeignKey('ml_cadastro.mlid'))
    id_variacao = Column(Integer, primary_key=True)
    cadastro_mercado_livre = relationship("MlCadastro",back_populates="variacoes")

    def __init__(self, mlid, id_variacao):
        self.mlid = mlid
        self.id_variacao = id_variacao

    def __repr__(self):
        return self.id_variacao


def tem_variacao_obrigatoria(sku):
    if sku is None or sku == '':
        raise Exception('Produto invalido')
    session = LocalStorage.get_session()
    produto = session.query(MlCadastro).filter(MlCadastro.sku == sku).first()
    if produto is None:
        session.close()
        return False
    tem_variacao_obrigatoria = len(produto.variacoes) > 0
    session.close()
    return tem_variacao_obrigatoria


def salvar(mlid, sku, variacoes):
    session = LocalStorage.get_session()
    if mlid is None or mlid == '':
        raise Exception('Sem mlid')
    produto = session.query(MlCadastro).filter(MlCadastro.mlid == mlid).first()
    if produto is not None:
        if produto.sku != sku or not produto.igual_variacoes(variacoes):
            produto.sku = sku
            lista_variacao = []

            # deletando
            for variacao_salva in produto.variacoes:
                session.delete(variacao_salva)

            # inserindo
            for variacao in variacoes:
                item_variacao = MlVariacao(mlid, variacao)
                lista_variacao.append(item_variacao)
            produto.variacoes = lista_variacao
    else:
        lista_variacao = []
        cadastro_mercado_livre = MlCadastro(mlid, sku)
        for variacao in variacoes:
            item_variacao = MlVariacao(mlid, variacao)
            lista_variacao.append(item_variacao)
        cadastro_mercado_livre.variacoes = lista_variacao
        session.add(cadastro_mercado_livre)
    session.commit()
    session.close()


