# -*- coding: utf-8 -*-
import smtplib
import os
import shelve
import inspect
import datetime as dtime
from functools import wraps
from sistema.models import JobExecucao
from database import LocalStorage
from source.meta.models import ControleJobs
from datetime import time, datetime
from database import Firebird
import log_server
import traceback
import sys
from sentry_sdk import capture_exception
from sqlalchemy import and_, func as fnc
from app import Singleton

def exception_notifier(exception):
    traceback.print_exception(*sys.exc_info())
    print('Caputrando exceção: ' + str(exception))
    capture_exception(exception)


def string_to_datetime(data):
    modelo = '%d/%m/%Y'
    return dtime.datetime.strptime(data, modelo)


def cached_file(func):
    func.cache = {}
    @wraps(func)
    def wrapper(*args, **kwargs):
        CACHE_ARQUIVO = os.getenv('CACHE_ARQUIVO')
        if not CACHE_ARQUIVO:
            return func(*args)
        db = shelve.open(CACHE_ARQUIVO)
        try:
            key = "{0}-{1}".format(func.func_name, args)
            return db[key]
        except KeyError:
            print('Processando chamada api externa')
            result = func(*args)
            if result is not None:
                db[key] = result
                print('saving new data into cachedb {0}'.format(result))
        finally:
            db.close()
        return result   
    return wrapper


def timer(func):
    @wraps(func)
    def wrapper(*args):
        execucao = JobExecucao(inicio=datetime.now(), nome=func.func_name)
        func(*args)
        execucao.fim = datetime.now()
        execucao.duracao = (execucao.fim - execucao.inicio).seconds
        session = LocalStorage.get_session()
        session.add(execucao)
        session.commit()
        session.close()
    return wrapper


def uma_vez_no_dia(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        nome_job = f.__name__.upper()
        hora = os.getenv(nome_job + '_HORARIO_EXEC')
        inst = Singleton()
        if hora is None:
            hora = 10
        if __horario_aprovado__(hora) \
            and not __executou_hoje__(nome_job) or inst.single_job_run:
            f(*args, **kwargs)
            __finalizar_exeucao_hoje__(nome_job)
        return
    return wrapper


def __horario_aprovado__(hora_liberada):
    now = dtime.datetime.now()
    return now.hour >= hora_liberada


def __executou_hoje__(nome):
    session = LocalStorage.get_session()
    hoje = (dtime.datetime.now()).strftime('%Y-%m-%d')
    execucoes_job = session.query(ControleJobs).filter(
        fnc.date(ControleJobs.data) >= hoje,
        fnc.date(ControleJobs.data) <= hoje,
        ControleJobs.nome == nome).first()
    session.close()
    return execucoes_job is not None


def __finalizar_exeucao_hoje__(nome):
    session = LocalStorage.get_session()
    data = dtime.datetime.now()
    report = ControleJobs(data, nome)
    session.add(report)
    session.commit()
    session.close()
