

class TiException(Exception):
    pass


class SkyHubException(Exception):
    pass


class CerberusException(Exception):
    pass


class OcException(Exception):
    pass


class MlException(Exception):
    pass


def __get_chamada__(message, url):
    return message + ', Chamada: ' + url