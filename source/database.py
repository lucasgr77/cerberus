# -*- coding: utf-8 -*-
import os
import fdb
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from pedidos.models import Base


class Firebird:
    @staticmethod
    def connect():
        url = os.getenv('FDB_URL')
        user = os.getenv('FDB_USER')
        password = os.getenv('FDB_PASSWORD')
        dll_path = os.getenv('FDB_DLL_PATH')
        fdb.load_api(dll_path)
        Firebird.con = fdb.connect(dsn=url, user=user,
                                   password=password, charset='latin1')

    @staticmethod
    def get_connection():
        if not Firebird.con or Firebird.con.closed:
            Firebird.connect()
        return Firebird.con.cursor()

    @staticmethod
    def commit():
        return Firebird.con.commit()

    @staticmethod
    def close():
        return Firebird.con.close()

    @staticmethod
    def tabela_existe(nome_tabela):
        cursor = Firebird.get_connection()
        q = 'SELECT 1 FROM {0}'.format(nome_tabela)
        try:
            cursor.execute(q)
            row = cursor.fetchone()
            cursor.close()
            if row:
                return row[0]
        except Exception as e:
            print(e)
            return False
        return True


class LocalStorage:
    @staticmethod
    def connect(echo=True):
        local_url = os.getenv('LOCALSTORAGE_URL')
        engine = create_engine('sqlite:///{0}'.format(local_url), echo=echo)
        Base.metadata.create_all(engine)
        LocalStorage.SessionMaker = sessionmaker(bind=engine)

    @staticmethod
    def drop():
        local_url = os.getenv('LOCALSTORAGE_URL')
        os.remove(local_url)

    @staticmethod
    def get_session():
        return LocalStorage.SessionMaker()
