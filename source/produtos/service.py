# encoding: utf-8
import os
import datetime
from .. import extensions as ext
from .. import log_server
from ..database import LocalStorage
from ..exceptions import TiException, __get_chamada__
from ..extensions import cached_file
from ..produtos.models import ProdutoTiny
from ..api import req_opencart, req_tiny


def get_produtos_tiny_incompletos(produtos_associados, **kwargs):
    produtos_incompletos = []
    session = LocalStorage.get_session()

    produtos_tiny_atualizados = session.query(ProdutoTiny).all()
    products_id_tiny = set(map(lambda x: x.product_id, produtos_tiny_atualizados))
    for prod in produtos_associados:
        if prod is None:
            continue
        if prod['product_id'] not in products_id_tiny:
            if prod['product_option_value_id'] is None:
                produtos_incompletos.append(prod)
            elif prod['product_id'] not in list(map(lambda x: x['product_id'], produtos_incompletos)):
                produtos_incompletos.append(prod)

    return produtos_incompletos


@cached_file
def get_tiny_id_from_sku(sku, **kwargs):
    logger = kwargs['logger'] if 'logger'\
        in kwargs else log_server.cerb_logger(__name__)

    url_base = os.getenv('TINY_URL_PRODUTOS_PESQUISA')
    token = os.getenv('TINY_TOKEN')
    url = '{0}?token={1}&formato=json&pesquisa={2}'.format(
        url_base, token, sku
    )
    payload = req_tiny(url)
    if len(payload['retorno']['produtos']) == 1:
        return payload['retorno']['produtos'][0]['produto']['id']
    else:
        produto = list(filter(lambda x: x['produto']['codigo'] == sku, payload['retorno']['produtos']))
        if len(produto) == 1:
            return produto[0]['produto']['id']
        raise TiException(__get_chamada__('Retorno invalido SKU: {0}'.format(sku), url))


@cached_file
def get_oc_id_from_sku(sku, **kwargs):
    PRODUCT_ID_COLUNA = 0
    PRIMEIRO_RECORD = 0
    url_base = os.getenv('OC_URL_OC_PRODUCT')
    url = '{0}?filter=sku,eq,{1}'.format(url_base, sku)
    response_payload = req_opencart(url, **kwargs)
    if len(response_payload['oc_product']['records']) == 0:
        return None
    return response_payload['oc_product']['records'][PRIMEIRO_RECORD][PRODUCT_ID_COLUNA]


def envia_ab_products(produtos, **kwargs):
    logger = kwargs['logger'] if 'logger'\
        in kwargs else log_server.cerb_logger(__name__)
    PRODUTO_NAO_ATUALIZADO = 0
    cargas = split_array(produtos, 100)
    url = os.getenv('OC_URL_AB_PRODUCTS') + '/'
    for carga in cargas:
        temp_url = url
        payload = []
        for prod in carga:
            temp_url += str(prod['CODPRODUTO']) + ','
            payload.append({
                "CODPRODUTO": prod['CODPRODUTO'],
                "CODBARRA": prod['CODBARRA'],
                "CODIGO_FABRICA": prod['CODIGO_FABRICA'],
                "DESCRICAO": prod['DESCRICAO'],
                "APLICACAO": prod['APLICACAO'],
                "PRECO_COMPRA": float(prod['PRECO_COMPRA']),
                "PRECO_CUSTO": float(prod['PRECO_CUSTO']),
                "PRECO_VENDA": float(prod['PRECO_VENDA']),
                "NCM": prod['NCM'],
                "CEST": prod['CEST']
            })
        temp_url = temp_url[0:len(temp_url)-1]
        response = None
        try:
            response = req_opencart(temp_url, 'PUT', data=payload)
            logger.info('Enviando produtos: ' + temp_url)
        except Exception as e:
            ext.exception_notifier(e)
            continue
        if len(response) > 1:
            i = 0
            for produto_atualizado in response:
                if produto_atualizado == PRODUTO_NAO_ATUALIZADO:
                    prod = carga[i]
                    post_produto_ab_products(prod)
                i += 1
        else:
            post_produto_ab_products(prod)


def post_produto_ab_products(prod, **kwargs):
    logger = kwargs['logger'] if 'logger'\
        in kwargs else log_server.cerb_logger(__name__)
    url = os.getenv('OC_URL_AB_PRODUCTS') + '/'
    payload = {
        "CODPRODUTO": prod['CODPRODUTO'],
        "CODBARRA": prod['CODBARRA'],
        "CODIGO_FABRICA": prod['CODIGO_FABRICA'],
        "DESCRICAO": prod['DESCRICAO'],
        "APLICACAO": prod['APLICACAO'],
        "PRECO_COMPRA": float(prod['PRECO_COMPRA']),
        "PRECO_CUSTO": float(prod['PRECO_CUSTO']),
        "PRECO_VENDA": float(prod['PRECO_VENDA']),
        "NCM": prod['NCM'],
        "CEST": prod['CEST']
    }
    try:
        req_opencart(url, 'POST', data=payload)
        logger.info('Enviando produtos: ' + url)
    except Exception as e:
        ext.exception_notifier(e)


def envia_ab_estoque(produtos, **kwargs):
    logger = kwargs['logger'] if 'logger'\
        in kwargs else log_server.cerb_logger(__name__)
    PRODUTO_NAO_ATUALIZADO = 0
    cargas = split_array(produtos, 100)
    url = os.getenv('OC_URL_AB_ESTOQUE') + '/'
    hoje = (datetime.datetime.now()).strftime('%Y-%m-%d')
    for carga in cargas:
        temp_url = url
        payload = []
        for prod in carga:
            temp_url += str(prod['CODPRODUTO']) + 'F' + str(prod['CODFILIAL']) + ','
            payload.append({
                'CODPRODUTO': prod['CODPRODUTO'],
                'FILIAL': prod['CODFILIAL'],
                "SALDO": int(prod['SALDO']),
                "DATA_ATUALIZACAO": hoje
            })
        temp_url = temp_url[0:len(temp_url)-1]
        response = None
        try:
            response = req_opencart(temp_url, 'PUT', data=payload)
            logger.info('Enviando produtos: ' + temp_url)
        except Exception as e:
            ext.exception_notifier(e)
            continue

        i = 0
        for produto_atualizado in response:
            if produto_atualizado == PRODUTO_NAO_ATUALIZADO:
                prod = carga[i]
                payload = {
                    'ID': str(prod['CODPRODUTO']) + 'F' + str(prod['CODFILIAL']),
                    "CODPRODUTO": prod['CODPRODUTO'],
                    "FILIAL": prod['CODFILIAL'],
                    "SALDO": int(prod['SALDO']),
                    "DATA_ATUALIZACAO": hoje
                }
                try:
                    response = req_opencart(url, 'POST', data=payload)
                    logger.info('Enviando produtos: ' + url)
                except Exception as e:
                    ext.exception_notifier(e)
            i += 1


def split_array(array, tamanho):
    for i in range(0, len(array), tamanho):
        yield array[i:i + tamanho]
