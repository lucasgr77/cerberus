from sqlalchemy import Column, Integer, String, Float, ForeignKey, and_, func
from sqlalchemy.orm import relationship
from source.database import LocalStorage
from source.pedidos.models import PedidoTiny, ItemPedidoTiny
from sqlalchemy.ext.declarative import declarative_base
from source.api import req_opencart
from ..settings import Base
from ..database import Firebird
import os
import requests
import datetime


class ProdutoTiny(Base):
     __tablename__ = 'produto_tiny'

     product_id = Column(Integer, primary_key=True)
     codproduto = Column(Integer)

     def __repr__(self):
        return "<ProductTiny: product_id:{0} codproduto:{1}>".format(self.product_id, self.codproduto)


def get_ncm(codproduto):
    cursor = Firebird.get_connection()
    q = 'SELECT NCM FROM PRODUTO WHERE CODPRODUTO = {0}'.format(codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    if row:
        return row[0]
    return None


def get_produto(codproduto, loja):
    cursor = Firebird.get_connection()
    q = '''SELECT  p.DESCRICAO, p.CODIGO_FABRICA,  pe.LOCALIZACAO 
            FROM PRODUTO p, PRODUTO_LOJA pl, PRODUTO_ESTOQUE pe 
            WHERE p.CODPRODUTO = {0}
            AND P.CODPRODUTO  = pl.CODPRODUTO 
            AND pl.CODFILIAL  = {1}
            AND pl.CODPRODUTOLOJA = pe.CODPRODUTOLOJA '''.format(codproduto, loja)
    cursor.execute(q)
    try:
        row = cursor.fetchone()
    except Exception as e:
        print(e)
        return None
    finally:
        cursor.close()
    if row:
        return {
            'descricao': row[0],
            'codigo_fabrica': row[1],
            'localizacao' : row[2]
        }
    return None


def atualizar_ean(codproduto, ean):
    cursor = Firebird.get_connection()
    q = '''UPDATE PRODUTO SET CODBARRA = '{0}' \
        WHERE CODPRODUTO = {1}'''.format(ean, codproduto)
    cursor.execute(q)
    Firebird.commit()
    cursor.close()


def get_all_ab_products():
    CODPRODUTO = 0
    CODBARRA = 1
    CODIGO_FABRICA = 2
    DESCRICAO = 3
    APLICACAO = 4
    PRECO_COMPRA = 5
    PRECO_CUSTO = 6
    PRECO_VENDA = 7
    NCM = 8
    CEST = 9
    cursor = Firebird.get_connection()
    data = []
    q = '''SELECT DISTINCT
            P.CODPRODUTO, P.CODBARRA, P.CODIGO_FABRICA, P.DESCRICAO,
            P.APLICACAO, PP.PRECO_COMPRA, PP.PRECO_CUSTO, PP.PRECO_VENDA,
            P.NCM, P.CEST
            FROM PRODUTO P, PRODUTO_PRECO PP
            WHERE ATIVO  = 'S'
            AND P.CODPRODUTO  = PP.CODPRODUTO
            ORDER BY DESCRICAO'''
    cursor.execute(q)
    rows = cursor.fetchall()
    for row in rows:
        data.append({
            'CODPRODUTO': row[CODPRODUTO],
            'DESCRICAO': row[DESCRICAO],
            'APLICACAO': row[APLICACAO],
            'CODBARRA': row[CODBARRA],
            'CODIGO_FABRICA': row[CODIGO_FABRICA],
            'PRECO_COMPRA': row[PRECO_COMPRA],
            'PRECO_CUSTO': row[PRECO_CUSTO],
            'PRECO_VENDA': row[PRECO_VENDA],
            'NCM': row[NCM],
            'CEST': row[CEST]
        })
    cursor.close()
    return data


def get_all_estoque():
    SALDO = 0
    CODPRODUTO = 1
    CODFILIAL = 2
    cursor = Firebird.get_connection()
    data = []
    q = '''SELECT PE.SALDOATUAL, P.CODPRODUTO , PL.CODFILIAL 
            FROM PRODUTO_ESTOQUE PE, PRODUTO_LOJA PL, PRODUTO P
            WHERE PE.CODPRODUTOLOJA  = PL.CODPRODUTOLOJA 
            AND PL.CODPRODUTO = P.CODPRODUTO 
            AND P.ATIVO = 'S'
            AND PL.CODFILIAL IN (1, 7)'''

    cursor.execute(q)
    rows = cursor.fetchall()
    for row in rows:
        saldo = row[SALDO]
        if saldo is None:
            saldo = 0
        data.append({
            'SALDO': saldo,
            'CODPRODUTO': row[CODPRODUTO],
            'CODFILIAL': row[CODFILIAL]
        })
    cursor.close()
    return data


def get_oc_product_description(product_id, **kwargs):
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT_DESCRIPTION')
    url = '{0}/{1}'.format(BASE_URL, product_id)
    response_payload = req_opencart(url, **kwargs)
    return response_payload


def consulta_produtos_top_50_loja():
    CODPRODUTO = 0
    QUANTIDADE = 1
    produtos = []
    cursor = Firebird.get_connection()
    data_de_hoje = datetime.datetime.now().date()
    sete_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%Y-%m-%d')
    hoje = (datetime.datetime.now()).strftime('%Y-%m-%d')
    q = ''' SELECT first 50  vi.CODPRODUTO, sum(vi.QTDE) AS qtde  FROM VENDA v, VENDA_ITEM vi 
            WHERE DATA BETWEEN '{0}' AND '{1}'
            AND v.codvenda = vi.CODVENDA
            AND CODFILIAL  = 7
            GROUP BY vi.CODPRODUTO 
            ORDER BY qtde desc '''.format(sete_dias_atras, hoje)
    cursor.execute(q)
    rows = cursor.fetchall()
    for row in rows:
        localizacao = get_localizacao(row[CODPRODUTO])
        product_id = get_porduct_id_by_codproduto(row[CODPRODUTO])
        produtos.append({
            'codproduto': row[CODPRODUTO],
            'localizacao': localizacao,
            'product_id': product_id
        })
    cursor.close()
    return produtos


def consulta_produtos_top_50_site():
    PEDIDO_TINY = 0
    lista_mais_vendidos = []
    session = LocalStorage.get_session()
    data_de_hoje = datetime.datetime.now().date()
    domingo_passado = (data_de_hoje - datetime.timedelta(days=1))
    sabado_passado = (data_de_hoje - datetime.timedelta(days=9))
    top_50_produtos = session.query(PedidoTiny, func.sum(ItemPedidoTiny.codigo)).join(ItemPedidoTiny, ItemPedidoTiny.pedido_tiny_id == PedidoTiny.id).group_by(ItemPedidoTiny.codigo)\
    .filter(and_(PedidoTiny.data > domingo_passado, PedidoTiny.data < sabado_passado)).limit(50).all()
    for selecao_produtos in top_50_produtos: # for para pegar todos os 50 produtos
        produto = session.query(ItemPedidoTiny).filter(ItemPedidoTiny.pedido_tiny_id == selecao_produtos[PEDIDO_TINY].id).first()
        product_id = consulta_product_id(produto.codigo)
        codproduto = valida_produto_top_50(product_id)
        if codproduto != None: # adiciona na lista apenas os produtos com vinculo na products_association
            localizacao = get_localizacao(codproduto)
            lista_mais_vendidos.append({
                'product_id': product_id,
                'codproduto': codproduto,
                'localizacao' : localizacao,
            })
    session.commit()
    session.close()
    return lista_mais_vendidos


def consulta_product_id(sku):
    PRODUCT_ID = 0
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT')
    url = '{0}?filter=sku,eq,{1}'.format(BASE_URL,sku)
    response = req_opencart(url)
    if len(response['oc_product']['records']) is 0:
        return None
    product_id = response['oc_product']['records'][0][PRODUCT_ID]
    return product_id


def valida_produto_top_50(product_id):
    CODPRODUTO = 0
    BASE_URL = os.getenv('OC_URL_PRODUCTS_ASSOCIATION')
    url = '{0}?filter=product_id,eq,{1}'.format(BASE_URL, product_id)
    response = req_opencart(url)
    if len(response['products_association']['records']) is 0:
        return None
    codproduto = response['products_association']['records'][0][CODPRODUTO]
    return codproduto


def get_localizacao(codproduto):
    LOCALIZACAO = 0 
    cursor = Firebird.get_connection()
    q = '''SELECT LOCALIZACAO FROM PRODUTO_ESTOQUE  WHERE CODPRODUTO = {0}'''.format(codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    if row is None:
        return None
    return row[LOCALIZACAO]


def get_porduct_id_by_codproduto(codproduto):
    PRODUCT_ID = 1
    BASE_URL = os.getenv('OC_URL_PRODUCTS_ASSOCIATION')
    url = '{0}?filter=codproduto,eq,{1}'.format(BASE_URL, codproduto)
    response = req_opencart(url)
    if  response['products_association']['records'] == []:
        return None
    product_id = response['products_association']['records'][0][PRODUCT_ID]
    return product_id