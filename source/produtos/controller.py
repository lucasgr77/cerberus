import time
from datetime import datetime
from source.api import req_opencart
from source.produtos.models import consulta_produtos_top_50_loja, consulta_produtos_top_50_site
import os
import requests
import collections
import datetime


def data_semana_conferencia():
    data_de_hoje = datetime.datetime.now().date()
    domingo_passado = (data_de_hoje - datetime.timedelta(days=1)).strftime('%m/%d')
    sabado_passado = (data_de_hoje - datetime.timedelta(days=9)).strftime('%m/%d')
    semana = domingo_passado + ' a ' + sabado_passado
    return semana


def dia_da_semana_atual():
    return datetime.datetime.now().weekday()


def nova_conferencia():
    LOJA_DE_ORIGEM = 2
    url = os.getenv('OC_URL_SN_HEADER')
    body = {
        'titulo': 'Semana das Vendas ' + data_semana_conferencia(),
        'loja': LOJA_DE_ORIGEM
    }
    response = req_opencart(url, 'POST', data=body)
    return response

def gerar_nova_conferencia(produtos_loja, produtos_site):
    url = os.getenv('OC_URL_SN_CONFERENCE')
    list_prod_final = []
    for p_loja,  p_site in zip(produtos_loja, produtos_site):
        if collections.Counter(p_loja) != collections.Counter(p_site):
            list_prod_final.append(p_loja)
            list_prod_final.append(p_site)
    id_conferencia = nova_conferencia()
    for add_produto in list_prod_final:
        body = {
            'product_id': add_produto['product_id'],
            'codproduto': add_produto['codproduto'],
            'localizacao': add_produto['localizacao'],
            'id_conferencia': id_conferencia
        }
        response = req_opencart(url, 'POST', data=body)


def carga_conferencia():
    SEGUNDA_FEIRA = 0
    dia = dia_da_semana_atual()
    if dia == SEGUNDA_FEIRA:
        top_50_loja = consulta_produtos_top_50_loja()
        top_50_site = consulta_produtos_top_50_site()
        gerar_nova_conferencia(top_50_loja, top_50_site)