# -*- coding:utf-8 -*-
import logging
import platform
from source.log_server import cerb_logger
from dotenv import load_dotenv, find_dotenv
import os
from sqlalchemy.ext.declarative import declarative_base
import sys
from sentry_sdk import init

plt = platform.system()

Base = declarative_base()
if plt == "Windows":
    print("Your system is Windows")
    BASEDIR = 'D:/cerberus'
    load_dotenv(os.path.join(BASEDIR, '.env'))
else:
    print("Your system is Linux")
    load_dotenv()

AMBIENTE = os.getenv('AMBIENTE')
VERSAO = os.getenv('VERSAO')
LOG = cerb_logger('cerberus')
LOG.info('Rodando versao: {0} - {1}'.format(AMBIENTE, VERSAO))
reload(sys)
sys.setdefaultencoding("utf8")


sentry_url = os.getenv('SENTRY_URL')
if sentry_url is not None:
    print('Iniciando serviço sentry: ' + sentry_url)
    init(sentry_url)
