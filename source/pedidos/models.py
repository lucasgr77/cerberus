from sqlalchemy import Column, Integer, String, Float, ForeignKey, Date
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from ..settings import Base


class PedidoTiny(Base):
    __tablename__ = 'pedido_tiny'

    id = Column(Integer, primary_key=True)
    valor = Column(Float)
    data = Column(Date)
    nome_vendedor = Column(String)
    itens = relationship("ItemPedidoTiny")

    def __repr__(self):
       return "<pedido_tiny(id='%s', valor='%s', nome_vendedor='%s')>" % (
                            self.id, self.valor, self.nome_vendedor)


class ItemPedidoTiny(Base):
    __tablename__ = 'item_pedido_tiny'

    id_produto = Column(Integer, primary_key=True)
    codigo = Column(String)
    quantidade = Column(Float)
    valor_unitario = Column(Float)
    pedido_tiny_id = Column(Integer, ForeignKey('pedido_tiny.id'), primary_key=True)
    pedido_tiny = relationship("PedidoTiny", back_populates="itens")
    separacao_loja = Column(String, default="n")
    loja = Column(Integer)
