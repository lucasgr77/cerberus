# -*- coding:utf-8 -*-
import requests
import os
import datetime

from ..exceptions import TiException, __get_chamada__
from ..extensions import string_to_datetime
from .. import log_server
from ..database import LocalStorage
from source.estoque.tiny import pedido_obter, req_tiny
from .models import PedidoTiny, ItemPedidoTiny


def pesquisa_pedidos(**kwargs):
    recentes = pedidos_recentes(**kwargs)
    atrasados = pedidos_atrasados(**kwargs)
    return recentes + atrasados


def pedidos_recentes(**kwargs):
    dias_pesquisa = kwargs['dias_pesquisa'] if 'dias_pesquisa' in kwargs \
        else 7
    data_final = kwargs['data_final'] if 'data_final' in kwargs else False
    pedidos = kwargs['pedidos'] if 'pedidos' in kwargs \
        else list()
    pagina = kwargs['pagina'] if 'pagina' in kwargs \
        else 1
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)

    url_base = os.getenv('TINY_URL_PEDIDOS_PESQUISA')
    token = os.getenv('TINY_TOKEN')
    hoje = datetime.datetime.now().date()
    periodo = (hoje - datetime.timedelta(days=dias_pesquisa)).strftime('%d/%m/%Y')

    parametro_data_final = ''
    if data_final:
        parametro_data_final = '&dataFinal={0}'.format(periodo)

    url = '{0}/?token={1}&formato=json&dataInicial={2}&pagina={3}{4}'.format(
        url_base, token, periodo, pagina, parametro_data_final)
    try:
        payload = req_tiny(url)
        if payload['retorno']['status'] != 'Erro':
            pedidos = pedidos_recentes(pedidos=pedidos, pagina=pagina + 1,
                                       dias_pesquisa=dias_pesquisa, data_final=data_final)
    except TiException as e:
        if str(e) == 'A consulta Nao retornou registros' or \
                     'tentando obter' in str(e):
            return []
        else:
            raise e
    return __build_pedido__(pedidos, payload, logger=logger)


def pedidos_atrasados(**kwargs):
    pedidos = []
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)

    url_base = os.getenv('TINY_URL_PEDIDOS_PESQUISA')
    token = os.getenv('TINY_TOKEN')
    hoje = datetime.datetime.now().date()
    atrasados = (hoje - datetime.timedelta(days=31)).strftime('%d/%m/%Y')
    url = '{0}/?token={1}&formato=json&dataInicial={2}&situacao=faturado'.format(
        url_base, token, atrasados)
    try:
        payload = req_tiny(url)
    except TiException as e:
        if str(e) == 'A consulta Nao retornou registros' or \
                     'tentando obter' in str(e) or\
                     'retornou registros' in str(e):
            return []
        else:
            raise e
    return __build_pedido__(pedidos, payload, logger=logger)


def __build_pedido__(pedidos, payload, **kwargs):
    for prod in payload["retorno"]["pedidos"]:
        numero_pedido = prod['pedido']['id']
        itens_pedido, frete, total_produtos = \
            pedido_obter(numero_pedido, **kwargs)
        obg = {
            'id': prod['pedido']['id'],
            'valor': prod['pedido']['valor'],
            'nome_vendedor': prod['pedido']['nome_vendedor'],
            'data': prod['pedido']['data_pedido'],
            'itens': itens_pedido,
            'frete': frete,
            'total_produtos': total_produtos
        }
        pedidos.append(obg)
    return pedidos


def salva_pedido(pedido, **kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs else log_server.cerb_logger(__name__)
    session = LocalStorage.get_session()
    logger.info('Salvando pedido: ' + str(pedido))
    pedido_salvo = PedidoTiny(id=pedido['id'], 
        valor=pedido['valor'],
        nome_vendedor=pedido['nome_vendedor'],
        data=string_to_datetime(pedido['data']),
        itens=list(map(
            lambda item: ItemPedidoTiny(id_produto=item['id_produto_tiny'],
                                        codigo=item['sku'],
                                        quantidade=item['quantidade'],
                                        valor_unitario=item['valor_unitario'],
                                        loja=item['origens'][0]['loja'],
                                        separacao_loja=item['origens'][0]['loja'] != 0)

            , pedido['itens'])))
    session.add(pedido_salvo)
    session.commit()
    session.close()
