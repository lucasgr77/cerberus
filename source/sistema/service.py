import requests
import os
import time
import inspect
from source.estoque.models import MovimentacaoEstoque
from datetime import datetime
from .. import log_server
from ..sistema.models import JobExecucao, test_fb_connection
from ..database import LocalStorage, Firebird


class MovimentacaoException(Exception):
    pass


def check_connections():
    logger = log_server.cerb_logger('cerberus')
    test_connections = os.getenv('TEST_CONNECTIONS')
    if test_connections:
        URL_OC = os.getenv('OC_URL_TEST_CONNECTION')
        URL_TINY = os.getenv('TINY_URL_TEST_CONNECTION')

        oc_response = requests.get(URL_OC)
        if oc_response.status_code != 200:
            raise RuntimeError("Sem conexao com Open Cart!")
        logger.info("Conexao com OPEN CART==========OK")

        oc_response = requests.get(URL_TINY)
        if oc_response.status_code != 200:
            raise RuntimeError("Sem conexao com o Tiny!")
        logger.info("Conexao com TINY===============OK")

        #test database sqlite
        execucao = JobExecucao(inicio=datetime.now(), nome='Inicializacao Cerberus')
        execucao.fim = datetime.now()
        execucao.duracao = (execucao.fim - execucao.inicio).seconds
        session = LocalStorage.get_session()
        session.add(execucao)
        session.commit()
        session.close()
        logger.info("Conexao com SQLITE=============OK")

        #test firebird
        Firebird.connect()
        result = test_fb_connection()
        if result != 1:
            raise RuntimeError("Sem conexao com Firebird")
        logger.info("Conexao com FIREBIRD===========OK")
        Firebird.close()


def log_movimentacao_estoque(product_id, loja, sistema, saldo_anterior, saldo_novo, tipo, opcao_id=None, codproduto=None):
    session = LocalStorage.get_session()
    if type(loja) is str or loja is None:
        raise MovimentacaoException('Campo loja invalido')
    elif type(sistema) is int or sistema is None:
        raise MovimentacaoException('Campo sistema invalido')
    elif type(saldo_anterior) is str or saldo_anterior is None:
        raise MovimentacaoException('Campo saldo_anterior invalido')
    elif type(saldo_novo) is str or saldo_novo is None:
        raise MovimentacaoException('Campo saldo_novo invalido')
    elif type(tipo) is not str or saldo_novo is None:
        raise MovimentacaoException('Campo tipo invalido')
    else:
        fluxo = 'E' if saldo_anterior < saldo_novo else 'S'
        salvando_item = MovimentacaoEstoque(
            product_id=product_id,
            opcao_id=opcao_id,
            codproduto=codproduto,
            loja=loja,
            sistema=sistema,
            job=__get_job_name_from_stack__(),
            saldo_anterior=saldo_anterior,
            saldo_novo=saldo_novo,
            fluxo=fluxo,
            data=datetime.now(),
            tipo=tipo)

        session.add(salvando_item)
        session.commit()
        session.close()


def __get_job_name_from_stack__():
    METHOD_NAME = 3
    curframe = inspect.currentframe()
    stacks = inspect.getouterframes(curframe, 2)
    for stack in stacks:
        if stack[METHOD_NAME][:4] == 'job_' or stack[METHOD_NAME][:6] == 'async_':
            return stack[METHOD_NAME]
    return 'nao identificado'