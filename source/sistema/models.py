from sqlalchemy import Column, Integer, String, Float, DateTime
from ..settings import Base
from ..database import Firebird


def test_fb_connection():
    cursor = Firebird.get_connection()
    q = 'SELECT 1 FROM PRODUTO'
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    if row:
        return row[0]


class JobExecucao(Base):
    __tablename__ = 'job_execucao'

    id = Column(Integer, primary_key=True)
    nome = Column(String)
    inicio = Column(DateTime)
    fim = Column(DateTime)
    duracao = Column(Float)

    def __repr__(self):
        return "<Job(id='%s', nome='%s', duracao='%s')>" % (
                            self.id, self.Job, self.duracao)


class MercadoLivreToken(Base):
    __tablename__ = 'mercado_livre_token'

    id = Column(Integer, primary_key=True)
    access_token = Column(String)
    refresh_token = Column(String)
