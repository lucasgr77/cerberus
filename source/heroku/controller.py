import requests
import json
import base64
import os
import time
from datetime import datetime


def monta_cabecalho():
    KEY = os.getenv('API_KEY_HEROKU')
    TOKEN = os.getenv('TOKEN_HEROKU')
    api_key = base64.b64encode(KEY)
    headers = {
                "Accept": TOKEN,
                "Authorization": api_key
    }
    return headers

def dia_atual():
    dia = datetime.now().weekday()
    return dia

def hora_atual():
    horario = datetime.now()
    hora = horario.hour
    return hora

def req_heroku(numero):
    url = os.getenv('HEROKU_URL')
    comando = {'quantity': numero}
    payload = json.dumps(comando)
    headers = monta_cabecalho()
    requests.patch(url, headers=headers, data=payload)


def __get_quantidade_dynos_ativos__():
    url = os.getenv('HEROKU_URL')
    headers = monta_cabecalho()
    retorno = requests.patch(url, headers=headers)
    payload = retorno.json()
    if payload['quantity'] == 1:
        return True
    return False


def controle_automacao():
    DESLIGADO = 0
    LIGADO = 1
    SEIS_HORAS = 6 
    DEZESSETE_HORAS = 17
    FIM_HORARIO_FDS = 13
    INICIO_HORARIO_WORKER = 6
    SABADO = 5
    DOMINGO = 6

    ligado = __get_quantidade_dynos_ativos__()
    hora = hora_atual()
    dia = dia_atual()

    if dia in (SABADO, DOMINGO):
        if not ligado and INICIO_HORARIO_WORKER < hora < FIM_HORARIO_FDS:
            req_heroku(LIGADO)
        elif ligado and hora >= FIM_HORARIO_FDS:
            req_heroku(DESLIGADO)
    else:
        if ligado and hora >= DEZESSETE_HORAS or hora < SEIS_HORAS:
            req_heroku(DESLIGADO)
        else:
            req_heroku(LIGADO)    

