# -*- coding: utf-8 -*-
import log_server
from . import extensions
from database import LocalStorage
from produtos import models as prod_model, service as prod_service
from pedidos.models import PedidoTiny
from estoque import opencart, ambientsoft as ams
from estoque import controller as controller_estoque
from pedidos import service as pedido_service
from operacao import gateway as gt
from precificacao import controller as precificacao_controller
from meta import controller as meta_controller
from catalogo_mercado_livre import controller as catalogo_controller
from extensions import uma_vez_no_dia
from job_liberacao_pedido import controller as clp
from heroku import controller as heroku_controller
from produtos import controller as nova_carga_conferencia
from limpar_dados.limpando_base import limpar_bases
import json


MESSAGE_OP = 'Temos {0} operacoes a processar'

#operacoes
BALANCO_ESTOQUE_AMBIENT = 'balanco_estoque_ambient'
VINCULAR_EAN = 'vincular_ean'


@extensions.timer
def job_envio_estoque_site():
    logger = log_server.cerb_logger('job_envio_estoque_site')
    logger.info('Inicando rotina')
    try:
        controller_estoque.envio_estoque(logger)
    except Exception as e:
        logger.error(e)
        extensions.exception_notifier(e)
    finally:
        logger.info('Encerrando rotina')


@extensions.timer
def job_vendas_faturadas(check=False):
    session = LocalStorage.get_session()
    logger = log_server.cerb_logger('job_vendas_faturadas')
    logger.info('Iniciando job_vendas_faturadas')
    pesquisa_pedidos = []

    # consulta
    try:
        pesquisa_pedidos = pedido_service.pesquisa_pedidos(logger=logger)
        params = opencart.req_products_association(logger=logger)
    except Exception as e:
        logger.error(e)
        extensions.exception_notifier(e)

    # processamento
    for pedido in pesquisa_pedidos:
        pedido_processado = len(
            session.query(PedidoTiny)
            .filter_by(id=pedido['id'])
            .all()) > 0
        if not pedido_processado:
            try:
                opencart.valida_separacao(pedido, params, logger=logger)
                pedido_service.salva_pedido(pedido, logger=logger)
            except Exception as e:
                logger.error(e)
                extensions.exception_notifier(e)
    session.close()
    logger.info('Encerrando rotina')


@extensions.timer
def job_operacoes():
    logger = log_server.cerb_logger('job_operacoes')
    logger.info('Iniciando job_vendas_faturadas')
    operacoes_pendentes = gt.get_operacoes_pendentes()
    logger.info(MESSAGE_OP.format(len(operacoes_pendentes)))
    for op in operacoes_pendentes:
        try:
            instrucoes = json.loads(op.json)
            for instrucao in instrucoes:
                if instrucao['nome'] == BALANCO_ESTOQUE_AMBIENT:
                    param = instrucao['parametro']
                    filial = param['filial']
                    product_id = param['product_id']
                    codproduto = param['codproduto']
                    saldo = param['saldo']
                    id_conferencia = param['id_conferencia']
                    revisao = param['revisao']
                    opcao_id = param['opcao_id'] if 'opcao_id' in param\
                        else None
                    ams.balanco_estoque_ambient(filial, product_id, codproduto,
                                                saldo, id_conferencia, opcao_id, revisao, logger=logger)
                if instrucao['nome'] == VINCULAR_EAN:
                    param = instrucao['parametro']
                    codproduto = param['codproduto']
                    ean = param['ean']
                    prod_model.atualizar_ean(codproduto, ean)
            gt.conclui_operacao(op)
        except ValueError as e:
            logger.error(e)
            extensions.exception_notifier(e)
            gt.anula_operacao(op)
        except Exception as e:
            logger.error(e)
            if op.retry > 3:
                gt.anula_operacao(op)
            else:
                extensions.exception_notifier(e)
                gt.rollback_operacao(op)
    logger.info('Encerrando rotina')


@extensions.timer
@uma_vez_no_dia
def job_envio_ab_products():
    logger = log_server.cerb_logger('job_envio_ab_products')
    logger.info('Iniciando job_envio_ab_products')
    try:
        #cadastros
        ab_products = prod_model.get_all_ab_products()
        prod_service.envia_ab_products(ab_products)

        #saldo estoque
        estoques = prod_model.get_all_estoque()
        prod_service.envia_ab_estoque(estoques)
    except Exception as e:
        logger.error(e)
        extensions.exception_notifier(e)
    finally:
        logger.info('Encerrando rotina')


@extensions.timer
@uma_vez_no_dia
def job_precificacao():
    logger = log_server.cerb_logger('job_precificacao')
    logger.info('iniciando job_precificacao')
    try:
        precificacao_controller.execute(logger)
    except Exception as e:
        logger.error(e)
        extensions.exception_notifier(e)
    finally:
        logger.info('Encerrando rotina')


@extensions.timer
@uma_vez_no_dia
def job_meta():
    logger = log_server.cerb_logger('job_meta')
    logger.info('iniciando job_meta')
    try:
        meta_controller.execute()
    except Exception as e:
        extensions.exception_notifier(e)
    finally:
        logger.info('Encerrando rotina')


@extensions.timer
@uma_vez_no_dia
def job_catalogo():
    logger = log_server.cerb_logger('job_catalogo')
    logger.info('iniciando job_catalogo')
    try:
        catalogo_controller.download_catalogo()
    except Exception as e:
        extensions.excpetion_notifier(e)
    finally:
        logger.info('Encerrando rotina')


@extensions.timer
def job_liberar_pedido():
    '''
    Job coleta pedidos Opencart "travados" com status 0
    e atualiza para status 2 liberando para separação no sistema do site
    '''
    logger = log_server.cerb_logger('job_liberar_pedido')
    logger.info('iniciando job_liberar_pedido')
    try:
        clp.liberar_pedidos()
    except Exception as e:
        extensions.excpetion_notifier(e)
    finally:
        logger.info('Encerrando rotina')


@extensions.timer
def job_ciclo_decepticon():
    logger = log_server.cerb_logger('job_ciclo_decepticon')
    logger.info('iniciando job_ciclo_decepticon')
    try:
        heroku_controller.controle_automacao()
    except Exception as e:
        extensions.excpetion_notifier(e)


@uma_vez_no_dia
def job_processa_cmd_vmd():
    '''
    Consulta e grava todos os calculs de VMD e CMD dos produtos vinculados
    e grava em uma tabela para rápida consulta CERBERUS_MD
    '''
    logger = log_server.cerb_logger('job_processa_cmd_vmd')
    logger.info('iniciando job_processa_cmd_vmd')
    try:
        controller_estoque.processa_vmd_cmd(logger=logger)
    except Exception as e:
        extensions.exception_notifier(e)
    finally:
        logger.info('Encerrando rotina')



@uma_vez_no_dia
def job_carga_conferencia():
    logger = log_server.cerb_logger('job_carga_conferencia')
    logger.info('iniciando job_carga_conferencia')
    try:
        nova_carga_conferencia.carga_conferencia()
    except Exception as e:
        logger.error(e)
        extensions.exception_notifier(e)
    finally:
        logger.info('Encerrando rotina')
@uma_vez_no_dia
def job_limpar_base():
    logger = log_server.cerb_logger('job_limpar_base')
    logger.info('iniciando job_limpar_base')
    try:
        limpar_bases()
    except Exception as e:
        extensions.exception_notifier(e)
