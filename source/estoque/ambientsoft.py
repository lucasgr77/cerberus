# encoding: utf-8
from ..sistema import service as sv
from . import models as db
import opencart as oc

AMBIENTE_LOJA_1 = 1
AMBIENTE_LOJA_2 = 7
LOJA_2 = 2


def subtrai_estoque(product_id, loja, codproduto, qtd_subtrair, opcao_id=None):
    if loja == LOJA_2:
        amb_loja = AMBIENTE_LOJA_2
    else:
        amb_loja = loja
    saldo_ant = db.get_saldo_produto(amb_loja, codproduto)
    db.subtrair_saldo_ambiente(amb_loja, codproduto, qtd_subtrair)
    saldo = db.get_saldo_produto(amb_loja, codproduto)
    sv.log_movimentacao_estoque(product_id, loja, 'ambientsoft',
                                saldo_ant, saldo, 'F', opcao_id)


def balanco_estoque_ambient(loja, product_id, codproduto, saldo, id_conferencia,  opcao_id=None, revisao=False, **kwargs):
    amb_loja = loja
    if loja == LOJA_2:
        amb_loja = AMBIENTE_LOJA_2
    saldo_ant = db.get_saldo_produto(amb_loja, codproduto)
    if not revisao and saldo_ant != saldo:
        peneirar_saldo_enviado(codproduto, id_conferencia, saldo_ant, saldo, **kwargs)
    if saldo_ant == saldo:
        return
    db.balanco_saldo_ambient(amb_loja, codproduto, saldo)
    saldo = db.get_saldo_produto(amb_loja, codproduto)
    sv.log_movimentacao_estoque(product_id, loja, 'ambientsoft',
                                saldo_ant, saldo, 'F', opcao_id, codproduto)


def peneirar_saldo_enviado(codproduto, id_conferencia, saldo_ant, saldo_atual, **kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    logger.info('Peneirando: CODPRODUTO: {0}, Saldo Anterior: {1} Saldo Atual: {2}'.format(
        codproduto, saldo_ant, saldo_atual))
    if saldo_ant > 0 and saldo_atual == 0:
        logger.info('Saldo zerado indo para Revisão: ' + str(codproduto))
        oc.update_conference_revisao(id_conferencia, codproduto)
    diferenca = int(saldo_atual) - saldo_ant
    if diferenca > 6 or diferenca < -6:
        logger.info('Saldo excedeu 6 indo para Revisão: ' + str(codproduto))
        oc.update_conference_revisao(id_conferencia, codproduto)
