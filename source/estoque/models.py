# -*- coding:utf-8 -*-
import os
import datetime
from ..database import Firebird
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from ..settings import Base


def get_sku(codproduto):
    cursor = Firebird.get_connection()
    q = 'SELECT p.CODIGO_FABRICA FROM PRODUTO p WHERE CODPRODUTO = {0}'.format(codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    if row:
        return row[0]
    return None


def get_saldo_produto(filial, codproduto):
    cursor = Firebird.get_connection()
    q = 'SELECT SALDOATUAL FROM PRODUTO_ESTOQUE PE, PRODUTO_LOJA PL \
            WHERE PE.CODPRODUTOLOJA  = PL.CODPRODUTOLOJA \
            AND PL.CODFILIAL = {0} \
            AND PE.CODPRODUTO  = {1}'.format(filial, codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    if row:
        return int(row[0])
    return None


def get_saldo_vmd_cmd(filial, codproduto):
    '''
    Retorna SALDO, CMD e VMD de respectivo produto
    '''
    SALDO_ATUAL = 0
    CMD = 1
    VMD = 2
    cursor = Firebird.get_connection()
    q = '''SELECT pe.SALDOATUAL, cm.CMD , cm.VMD
           FROM PRODUTO_ESTOQUE PE, PRODUTO_LOJA PL, CERBERUS_MD cm
           WHERE PE.CODPRODUTOLOJA  = PL.CODPRODUTOLOJA
           AND CM.CODPRODUTO  = PL.CODPRODUTO
           AND PL.CODFILIAL = {0}
           AND PE.CODPRODUTO  = {1}'''.format(filial, codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    if row:
        return int(row[SALDO_ATUAL]), float(row[CMD]), float(row[VMD])
    return None


def subtrair_saldo_ambiente(filial, codproduto, qtd_subtrair):
    cursor = Firebird.get_connection()
    q = '''UPDATE PRODUTO_ESTOQUE SET \
        SALDOATUAL = SALDOATUAL - {2}, \
        DATA_ALTERACAO  = cast('Now' as date), \
        HORA_ALTERACAO  = cast('Now' as time), \
        SALDOANTERIOR = \
            (SELECT SALDOATUAL  FROM PRODUTO_ESTOQUE WHERE CODPRODUTOLOJA IN \
                (SELECT CODPRODUTOLOJA \
                FROM PRODUTO_LOJA pl \
                WHERE CODFILIAL = {0} \
                AND CODPRODUTO = {1}) \
                AND CODPRODUTO  = {1} ) \
        WHERE CODPRODUTOLOJA IN (SELECT CODPRODUTOLOJA  \
        FROM PRODUTO_LOJA pl \
        WHERE CODFILIAL = {0} \
        AND CODPRODUTO = {1}) \
        AND CODPRODUTO  = {1}'''.format(filial, codproduto, qtd_subtrair)
    cursor.execute(q)
    Firebird.commit()
    cursor.close()
    return True


def balanco_saldo_ambient(filial, codproduto, saldo):
    cursor = Firebird.get_connection()
    q = '''UPDATE PRODUTO_ESTOQUE SET \
        SALDOATUAL = {2}, \
        DATA_ALTERACAO  = cast('Now' as date), \
        HORA_ALTERACAO  = cast('Now' as time), \
        SALDOANTERIOR = \
            (SELECT SALDOATUAL  FROM PRODUTO_ESTOQUE WHERE CODPRODUTOLOJA IN \
                (SELECT CODPRODUTOLOJA \
                    FROM PRODUTO_LOJA pl \
                    WHERE CODFILIAL = {0} \
                    AND CODPRODUTO = {1}) \
                    AND CODPRODUTO  = {1} ) \
        WHERE CODPRODUTOLOJA IN (SELECT CODPRODUTOLOJA  \
        FROM PRODUTO_LOJA pl \
        WHERE CODFILIAL = {0} \
        AND CODPRODUTO = {1}) \
        AND CODPRODUTO  = {1}'''.format(filial, codproduto, saldo)
    cursor.execute(q)
    Firebird.commit()
    cursor.close()
    return True

def get_vmd(codproduto):
    VMA = 0
    cursor = Firebird.get_connection()
    data_de_hoje = datetime.datetime.now().date()
    um_ano_atras = (data_de_hoje - datetime.timedelta(days=365)).strftime('%Y-%m-%d')
    hoje = (datetime.datetime.now()).strftime('%Y-%m-%d')
    q = ''' SELECT  sum(vi.QTDE)/ 365 AS VMA, sum(vi.QTDE) AS QTDE, p.CODPRODUTO, p.DESCRICAO
            FROM  VENDA_ITEM vi, VENDA v, PRODUTO p
            WHERE p.CODPRODUTO = {0}
            AND v.CODVENDA = vi.CODVENDA 
            AND v.DATA BETWEEN '{1}' AND '{2}'
            AND vi.CODPRODUTO  = p.CODPRODUTO
            GROUP BY p.CODPRODUTO, p.DESCRICAO
            ORDER BY SUM(vi.QTDE) DESC '''.format(codproduto, um_ano_atras, hoje )
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    vma = 0
    if row is not None:
        vma = max(row[VMA], 0)
    return round(float(vma), 5)


def get_cmd(codproduto):
    COMPRAS = 0
    cursor = Firebird.get_connection()
    data_hoje = datetime.datetime.now().date()
    um_ano_atras = (data_hoje - datetime.timedelta(days=365)).strftime('%Y-%m-%d')
    hoje = (datetime.datetime.now()).strftime('%Y-%m-%d')
    q = ''' SELECT count(c.CODCOMPRA) AS compras, ci.CODPRODUTO FROM COMPRA_ITEM ci, COMPRA c
            WHERE c.DATA_COMPRA BETWEEN '{0}' AND '{1}'
            AND ci.CODPRODUTO = {2}
            AND c.CODCOMPRA = ci.CODCOMPRA 
            GROUP BY CODPRODUTO
            ORDER BY count(c.CODCOMPRA) DESC'''.format(um_ano_atras, hoje, codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    cmd = 0
    if row is not None:
        cmd = max(row[COMPRAS], 0)
    return round(float(cmd)/365, 4)


def atualiza_cmd_vmd(codproduto, cmd, vmd):
    cursor = Firebird.get_connection()
    q = ''' SELECT codproduto FROM CERBERUS_MD WHERE CODPRODUTO = {0}
    '''.format(codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    cursor = Firebird.get_connection()
    if row is None:
        q = '''INSERT INTO CERBERUS_MD (CODPRODUTO, CMD, VMD)
        VALUES ({0}, {1}, {2})'''.format(codproduto, cmd, vmd)
        cursor.execute(q)
        Firebird.commit()
    else:
        q = '''UPDATE CERBERUS_MD SET CMD = {1}, VMD = {2}
        WHERE CODPRODUTO = {0}
        '''.format(codproduto, cmd, vmd)
        cursor.execute(q)
        Firebird.commit()
    cursor.close()
    return True


def __build_origem_estoque__(loja, quantidade):
    ONLINE = 0
    estoque_ambientesoft = True
    if loja == ONLINE:
        estoque_ambientesoft = False
    return {
        'loja': loja,
        'estoque_ambientesoft': estoque_ambientesoft,
        'quantidade': quantidade,
    }


class Loja:
    LOJA_1 = 1
    LOJA_2 = 2
    ONLINE = 0


class MovimentacaoEstoque(Base):
    '''
    Classe representa toda movimentação de estoque realizada pelo Cerberus.
    loja: Loja da movimentação do objeto InformacoesLoja
    sistema: Tiny, AmbienteSoft, Opencart
    job: Qual job do cerberus movimentou (job_envio_estoque_site)
    saldo_anteior: Antes de aplicar a movimentação
    saldo_novo: Saldo atualizado
    data: data e hora da movimentação
    fluxo: 'S' de saída 'E' de Entrada
    tipo: 'F' estoque físico da loja 'V' estoque virtual não físico
    '''
    __tablename__ = 'movimentacao_estoque'
    id = Column(Integer, primary_key=True)
    product_id = Column(Integer)
    opcao_id = Column(Integer)
    codproduto = Column(Integer)
    loja = Column(Integer)
    sistema = Column(String)
    job = Column(String)
    saldo_anterior = Column(Integer)
    saldo_novo = Column(Integer)
    data = Column(DateTime)
    fluxo = Column(String)
    tipo = Column(String)


class InformacoesLoja(Base):
    __tablename__ = 'info_loja'
    id = Column(Integer, primary_key=True)
    nome = Column(String)
