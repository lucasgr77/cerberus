# encoding: utf-8
from source.database import LocalStorage
from source.catalogo_mercado_livre.models import MlCadastro
from . import opencart, models, tiny, mercado_livre, ambientsoft as ams
from source.catalogo_mercado_livre.models import tem_variacao_obrigatoria
from multiprocessing.pool import ThreadPool as Pool
from source import extensions as ext
from source.estoque import models as mdl
from source.extensions import log_server
from source.extensions import cached_file
from source.database import Firebird
import math


LOJA_2 = 7
MSG_MOV_ESTOQUE = '{0} cod_produto: {1}, sku: {5}, product_id: {2} para quantidade: {3} e quantidade_fisica {4}'
MSG_LOG_OPCAO = 'Movimentação opção: {0}, saldo_loja: {1} saldo_f_ant: {2}'
CREATE_TABLE_CERBERUS_MD = '''CREATE TABLE CERBERUS_MD (CODPRODUTO INTEGER, CMD FLOAT,VMD FLOAT)'''
CREATE_INDEX_CERBERUS_MD = '''CREATE INDEX CERBERUS_MD_CODPRODUTO_IDX ON CERBERUS_MD (CODPRODUTO)'''


def envio_estoque(logger):
    produtos_associados = opencart.get_produtos_associados(logger=logger)
    pool = Pool(10)
    async_calls = []
    for prod in produtos_associados:
        saldo, cmd, vmd = models.get_saldo_vmd_cmd(LOJA_2, prod['codproduto'])
        prod['cmd'] = cmd
        prod['vmd'] = vmd
        async_call = pool.apply_async(async_altera_estoque, (logger, prod, saldo, ))
        async_calls.append(async_call)

    pool.close()
    pool.join()

    for async_call in async_calls:
        async_call.get()


def processa_vmd_cmd(**kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    __checa_cerberus_md_existe__(logger)
    associados = opencart.req_products_association(True, logger=logger)
    produtos = set(map(lambda x: x['codproduto'], associados))
    for codproduto in produtos:
        vmd = mdl.get_vmd(codproduto)
        cmd = mdl.get_cmd(codproduto)
        models.atualiza_cmd_vmd(codproduto, cmd, vmd)
        logger.info('Registrando Produto: {0} CMD: {1} VMD: {2}'
                    .format(codproduto, cmd, vmd))


def async_altera_estoque(logger, prod, saldo):
    tem_movimento_estoque = False
    try:
        saldo_ant, saldo_f_ant = prod['quantity'], prod['quantity_fisica']
        blocked = __get_quantity_stock_blocked__(prod['codproduto'],
                                                prod['quantity_blocked'],
                                                prod['cmd'],
                                                prod['vmd'],
                                                logger=logger)
        est_disponivel = saldo - blocked
        saldo_ant = max(saldo_ant, 0)
        est_disponivel = max(est_disponivel, 0)
        if prod['percentual_sell'] is None:
            prod['percentual_sell'] = 0
        saldo_loja = math.floor(
                est_disponivel * float(prod['percentual_sell']))
        if saldo_loja == 0 and saldo >= blocked and saldo > 0:
            saldo_loja = 1
        saldo_online = saldo_ant - saldo_f_ant
        saldo_total = saldo_online + saldo_loja
        tem_movimento_estoque = saldo_loja != saldo_f_ant
        product_id = prod['product_id']
        sku = prod['sku']
        if __tem_variacao_obrigatoria__(product_id, sku):
            atualiza_saldo_ml_variacao(sku, saldo_total, logger=logger)
        if tem_movimento_estoque:
            if prod['product_option_value_id'] is not None:
                logger.info(MSG_LOG_OPCAO.format(prod, saldo_loja, saldo_f_ant))
            movimenta_estoque(logger,
                            prod,
                            saldo_ant,
                            saldo_f_ant,
                            saldo_loja,
                            saldo_total)
    except Exception as e:
        logger.error(e)
        ext.exception_notifier(e)


def movimenta_estoque(logger, prod, saldo_ant, saldo_f_ant, saldo_loja, saldo_total):
    product_id = prod['product_id']
    codproduto = prod['codproduto']
    sku = prod['sku']
    option_id = prod['product_option_value_id']

    logger.info(MSG_MOV_ESTOQUE.format(
        'Movimentando', codproduto, product_id, saldo_total, saldo_loja, sku))

    if option_id is None:
        opencart.atualiza_saldo_oc(product_id, saldo_total, saldo_loja,
                                   logger=logger, saldo_ant=saldo_ant,
                                   saldo_fis_ant=saldo_f_ant)
        tiny.atualiza_saldo_tiny(product_id, sku, saldo_total,
                                 logger=logger, saldo_ant=saldo_ant,
                                 opcao_id=option_id)
    else:
        sku_variante = sku + '-' + str(option_id)
        opencart.atualiza_saldo_opcao_oc(product_id, option_id,
                                         saldo_total, saldo_loja,
                                         logger=logger,
                                         saldo_ant=saldo_ant,
                                         saldo_fis_ant=saldo_f_ant)
        tiny.atualiza_saldo_tiny(product_id, sku_variante, saldo_total,
                                 logger=logger, saldo_ant=saldo_ant,
                                 opcao_id=option_id)


def __tem_variacao_obrigatoria__(product_id, sku):
    '''
    Verifica se determinado produto tem variação obrigatória
    Ex: Peças que o mercado livre nos obriga aplicar variação
    perdendo o vínculo no Tiny
    '''

    response = opencart.req_product_option_value(product_id)
    options = response['oc_product_option_value']['records']

    variacao_obrigatoria = tem_variacao_obrigatoria(sku)
    return len(options) == 0 and variacao_obrigatoria


def atualiza_saldo_ml_variacao(sku, saldo_total, **kwargs):
    '''
    Atualiza saldo no Mercado Livre nas variations
    '''

    session = LocalStorage.get_session()
    produto = session.query(MlCadastro).filter(MlCadastro.sku == sku).first()
    mercado_livre.atualiza_saldo_ml_variacao(produto.mlid,
                                             saldo_total,
                                             produto.variacoes, **kwargs)
    session.close()


def __get_quantity_stock_blocked__(codproduto, quantidade_bloqueada, cmd, vmd, **kwargs):
    '''
    Retorna quantidade bloqueada do estoque.
    CMD > 10 = Sem bloqueios de estoque para vender na online
    CMD < 10 e VMD < 0.27 = Sem bloqueios
    CMD < 10 e VMD > 0.271 = Bloqueia 1 produto
    Se não atender nenhuma regra retorna 2 itens para bloqueio.

    Item bloqueio representa um saldo da loja física que não será
    disponibilizado nos marketplaces para ser vendido.
    '''
    QUANTIDADE_BLOQUEADA_PADRAO = min(quantidade_bloqueada, 2)
    blocked = QUANTIDADE_BLOQUEADA_PADRAO
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    if vmd is None or cmd is None:
        blocked = quantidade_bloqueada
    elif cmd > 10:
        blocked = 0
    elif 0.00 < vmd < 0.27:
        blocked = 0
    elif 0.271 < vmd < 2.70:
        blocked = 1
    logger.info('CODPRODUTO : {0} VMD :{1} CMD: {2}, Bloqueado: {3}'.format(
        codproduto, vmd, cmd, blocked
    ))
    return blocked


def __checa_cerberus_md_existe__(logger):
    if not Firebird.tabela_existe('CERBERUS_MD'):
        cursor = Firebird.get_connection()
        cursor.execute(CREATE_TABLE_CERBERUS_MD)
        Firebird.commit()
        cursor.execute(CREATE_INDEX_CERBERUS_MD)
        Firebird.commit()
        cursor.close()
        logger.info('Tabela CERBERUS_MD criada com sucesso')
