# encoding: utf-8
from source.api import req_mercado_livre
from source.extensions import log_server
import os


def atualiza_saldo_ml_variacao(mlid, saldo, variacoes, **kwargs):
    '''Atualiza saldo do Mercado Livre diretamente via API'''
    if saldo < 0:
        saldo = 0
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    variacoes = list(map(lambda x: {'id': x.id_variacao,
                                    'available_quantity': saldo}, variacoes))
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    url = URL_BASE + 'items/' + mlid
    body = {
        'variations': variacoes
    }
    logger.info('Alterando saldo MLID: {0} saldo: {1} tem variações'
                .format(mlid, saldo))
    req_mercado_livre(url, verb='PUT', data=body)
