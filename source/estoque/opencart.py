# encoding: utf-8
import os
import requests
from .. import log_server, extensions
from ..api import convert_api_response_to_object

from ..extensions import cached_file
from ..notify import service as notify
from ..produtos import service as prod_service
from . import ambientsoft as amb
from multiprocessing.pool import ThreadPool as Pool
from ..sistema import service as sv
from .models import Loja, __build_origem_estoque__
from ..api import req_opencart, convert_api_response_to_object
from ..exceptions import OcException

CODPRODUTO = 0
PRODUCT_ID = 1
ACTIVE = 2
LOJA_2 = 2
ABASTECIMENTO_LOJA_FISICA = 's'
POV_ID = 0
POV_QUANTITY = 5
POV_QUANTITY_FISICA = 13

QUANTITY_STOCK_BLOCKED = 3
PRODUCT_OPTION_VALUE_ID = 4
PERCENTUAL_SELL = 5


def get_oc_product_async(prod, logger, **kwargs):
    PRODUCT_ID = 1
    try:
        logger.info('get_oc_product_async: {0}'.format(prod[PRODUCT_ID]))
        oc_product = req_oc_product(prod[PRODUCT_ID], logger=logger)
        if prod[PRODUCT_OPTION_VALUE_ID] is None:
            dados_produto = {
                'product_id' : prod[PRODUCT_ID],
                'quantity' : oc_product['quantity'],
                'quantity_fisica': oc_product['quantity_fisica'],
                'codproduto': prod[CODPRODUTO],
                'sku': oc_product['sku'],
                'quantity_blocked': prod[QUANTITY_STOCK_BLOCKED],
                'product_option_value_id' : prod[PRODUCT_OPTION_VALUE_ID],
                'percentual_sell': prod[PERCENTUAL_SELL]
            }
            logger.info('dados produto normal: ' + str(dados_produto))
            return dados_produto
        else:
            option = get_product_option_value_id_by_id(prod[PRODUCT_OPTION_VALUE_ID])
            oc_product = req_oc_product(prod[PRODUCT_ID])
            dados_produto = {
                'product_id': prod[PRODUCT_ID],
                'quantity': option['quantity'],
                'quantity_fisica': option['quantity_fisica'],
                'codproduto': prod[CODPRODUTO],
                'sku': oc_product['sku'],
                'quantity_blocked': prod[QUANTITY_STOCK_BLOCKED],
                'product_option_value_id': prod[PRODUCT_OPTION_VALUE_ID],
                'percentual_sell': prod[PERCENTUAL_SELL]
            }
            logger.info('dados produto com opção: ' + str(dados_produto))
            return dados_produto
    except Exception as e:
        logger.error(e)
        extensions.exception_notifier(e)


def req_products_association(serialized = False, **kwargs):
    BASE_URL = os.getenv('OC_URL_PRODUCTS_ASSOCIATION')
    url = '{0}'.format(BASE_URL)
    response_payload = req_opencart(url, **kwargs)
    if not serialized:
        return response_payload['products_association']['records']
    records = response_payload['products_association']['records']
    columns = response_payload['products_association']['columns']
    return convert_api_response_to_object(columns, records)


def get_product_option_value_id_by_id(product_option_value_id):
    URL_BASE = os.getenv('OC_URL_OC_PRODUCT_OPTION_VALUE')
    url = '{0}/{1}'.format(URL_BASE, product_option_value_id)
    payload = req_opencart(url)
    return payload


def get_produtos_associados(**kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs \
        else log_server.cerb_logger(__name__)

    PRODUTO_ATIVO_VENDER_SITE = 's'
    produtos = []
    pool = Pool(10)
    products_queries = []

    response_payload = req_products_association(**kwargs)
    logger.info('Avaliando estoque de {0} produtos'.format(str(len(response_payload))))
    for prod in response_payload:
        if prod[ACTIVE].lower() == PRODUTO_ATIVO_VENDER_SITE:
            async_call = pool.apply_async(get_oc_product_async, (prod, logger, ))
            products_queries.append(async_call)
    pool.close()
    pool.join()

    for product_query in products_queries:
        produtos.append(product_query.get())

    return produtos


def req_product_option_value(product_id, **kwargs):
    URL_BASE = os.getenv('OC_URL_OC_PRODUCT_OPTION_VALUE')
    url = '{0}?filter=product_id,eq,{1}'.format(URL_BASE,product_id)
    payload = req_opencart(url)
    return payload


def req_oc_product(product_id, **kwargs):
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT')
    url = '{0}/{1}'.format(BASE_URL, product_id)
    response_payload = req_opencart(url, **kwargs)
    return response_payload


def req_oc_product_description(product_id, **kwargs):
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT_DESCRIPTION')
    url = '{0}/{1}'.format(BASE_URL, product_id)
    response_payload = req_opencart(url, **kwargs)
    return response_payload


def atualiza_saldo_oc(product_id, saldo, saldo_fis, **kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    saldo_ant = kwargs['saldo_ant'] if 'saldo_ant' in kwargs\
        else None
    saldo_fis_ant = kwargs['saldo_fis_ant'] if 'saldo_fis_ant' in kwargs\
        else None
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT')
    url = '{0}/{1}'.format(BASE_URL, product_id)
    data = {}
    if saldo is not None:
        data['quantity'] = saldo
    if saldo_fis is not None:
        data['quantity_fisica'] = saldo_fis
    logger.info('request: POST '+url + ' ' + str(data))
    req_opencart(url, 'PUT', data=data, **kwargs)
    if saldo is not None and saldo_ant is not None:
        sv.log_movimentacao_estoque(product_id, Loja.ONLINE,
                                    'opencart', saldo_ant, saldo, 'F')
    if saldo_fis is not None and saldo_fis_ant is not None:
        sv.log_movimentacao_estoque(product_id, Loja.ONLINE,
                                    'opencart', saldo_fis_ant, saldo_fis, 'V')


def atualiza_saldo_opcao_oc(product_id, option_id, saldo, saldo_fis, **kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    saldo_ant = kwargs['saldo_ant'] if 'saldo_ant' in kwargs\
        else None
    saldo_fis_ant = kwargs['saldo_fis_ant'] if 'saldo_fis_ant' in kwargs\
        else None
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT_OPTION_VALUE')
    url = '{0}/{1}'.format(BASE_URL, option_id)
    data = {}
    if saldo is not None:
        data['quantity'] = saldo
    if saldo_fis is not None:
        data['quantity_fisica'] = saldo_fis
    logger.info('request: POST '+url + ' ' + str(data))
    req_opencart(url, 'PUT', data=data, **kwargs)
    if saldo is not None and saldo_ant is not None:
        sv.log_movimentacao_estoque(product_id, Loja.ONLINE, 'opencart',
                                    saldo_ant, saldo, 'F', option_id)
    if saldo_fis is not None and saldo_fis_ant is not None:
        sv.log_movimentacao_estoque(product_id, Loja.ONLINE, 'opencart',
                                    saldo_fis_ant, saldo_fis, 'V', option_id)


def processar_saldo_venda(product_id, qtd_fis, qtd_inteirar, item, opcao_id=None, **kwargs):
    qtd_fis_atual = qtd_fis - qtd_inteirar
    if 'origens' not in item:
        raise Exception('Item vendido sem origens: CODPRODUTO: {0} PRODUCT_ID: {1}'
                        .format(item['codproduto'], item['product_id']))
    for origem in item['origens']:
        if origem['estoque_ambientesoft']:
            amb.subtrai_estoque(product_id, origem['loja'], item['codproduto'], qtd_inteirar, opcao_id)
    if opcao_id is None:
        atualiza_saldo_oc(product_id, None, qtd_fis_atual, saldo_fis_ant=qtd_fis, **kwargs)
    else:
        atualiza_saldo_opcao_oc(product_id, opcao_id, qtd_fis_atual,
                                qtd_fis_atual, saldo_fis_ant=qtd_fis, **kwargs)


def valida_separacao(pedido, params, **kwargs):
    itens = pedido['itens']
    for item in itens:
        item['product_id'] = prod_service.get_oc_id_from_sku(item['sku'], **kwargs)
        item['origens'] = [__build_origem_estoque__(0, item['quantidade'])]
        if not item['product_id']:
            separar_item_opcao(item, params)
        else:
            separar_item_normal(params, item, **kwargs)
    if __get_versao_notificar__() == '2':
        envio = ':ml:'
        if pedido['nome_vendedor'] is None or \
           'mercado livre' not in pedido['nome_vendedor'].lower():
            envio = ':correios:'
        notify.notificar_separar_estoques_v2(pedido['id'], itens,
                                            envio, **kwargs)


def separar_item_opcao(item, params, **kwargs):
    if '-' in item['sku']:
        sku = item['sku']
        sku = sku[0:sku.rindex('-')]
        sku_valido = sku
        item['product_id'] = prod_service.get_oc_id_from_sku(sku_valido, **kwargs)
        oc_product = req_oc_product(item['product_id'], **kwargs)
        __processa_separacao_opcao__(item, oc_product, params)


def separar_item_normal(params, item, **kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs \
        else log_server.cerb_logger(__name__)
    for param in params:
        if item['product_id'] == param[PRODUCT_ID]:
            # request
            oc_product = req_oc_product(item['product_id'], **kwargs)

            # params
            item['codproduto'] = param[CODPRODUTO]
            estoque_disponivel = max(oc_product['quantity'], 0)
            qtd_vendida = float(item['quantidade'])
            estoque_fisico = float(oc_product['quantity_fisica'])
            total_estoque = float(estoque_disponivel) + qtd_vendida
            saldo_online = max(total_estoque - estoque_fisico, 0)
            __log_mov_estoque__(item, logger, estoque_disponivel,
                                qtd_vendida, estoque_fisico, total_estoque, saldo_online)
            qtd_inteirar = qtd_vendida - saldo_online
            qtd_inteirar = max(qtd_inteirar, 0)

            # logistica
            item['origens'] = __get_origem_estoques__(qtd_vendida, saldo_online,
                                                        qtd_inteirar)
            logger.info('Movimentando produto: ' + str(oc_product))
            if saldo_online < qtd_vendida:
                processar_saldo_venda(oc_product['product_id'], estoque_fisico,
                                      qtd_inteirar, item, **kwargs)
                if __get_versao_notificar__() == '1':
                    notify.notificar_separar_estoques(param[CODPRODUTO], qtd_inteirar, **kwargs)
                return


def __log_mov_estoque__(item, logger, estoque_disponivel, qtd_vendida, estoque_fisico, total_estoque, saldo_online):
    logger.info('''Dados separar item normal: codproduto: {5}\n
                estoque_disponivel: {0}\nqtd_vendida: {1}\n
                estoque_fisico: {2}\ntotal_estoque: {3}\n
                saldo_online: {4}'''.format(estoque_disponivel,
                                            qtd_vendida,
                                            estoque_fisico,
                                            total_estoque,
                                            saldo_online,
                                            item['codproduto']))
    logger.info('Estoque online: {0}, vendido: {1}'
                .format(saldo_online, qtd_vendida))


def __processa_separacao_opcao__(item, oc_product, params, **kwargs):
    '''
    Processa a baixa de uma venda no sistema
    ATENÇÂO: Estoque de opções vem da tabela oc_product_option_value e ela não é subtraído o quantity
    (diferente da oc_product que o tiny subtrai o quantity automático)
    '''
    ERRO_OPCAO_NAO_ENCONTRADA = 'Parametrização POVI não encontrada para o product_id: {0}, povi: {1}'
    ERRO_MULTIPLAS_OPCOES_ENCONTRADA = 'Mais de um produto parametrizado com opcao encontrado: {0}'
    LOG_MOVIMENTACAO = 'Movimentando produto com opcao product_id: {0}, povi: {1}'
    ERRO_NAO_ENCONTRADO = 'Nâo foi possível processar o produto sku: {0}'

    logger = kwargs['logger'] if 'logger' in kwargs \
        else log_server.cerb_logger(__name__)
    data = req_product_option_value(oc_product['product_id'])['oc_product_option_value']
    options = convert_api_response_to_object(data['columns'],
                                             data['records'])
    qtd_vendida = float(item['quantidade'])
    # pesquisa oc_product_option_value para pegar o saldo
    povi = item['sku'].split('-')[-1]
    option = list(filter(lambda x: str(x['product_option_value_id']) == povi,
                         options))
    if not option:
        raise OcException(ERRO_OPCAO_NAO_ENCONTRADA.format(oc_product['product_id'],
                                                        povi))
    option = option[0]

    # pega a parametrização no products_association
    parametros = list(filter(lambda param:
        oc_product['product_id'] == param[PRODUCT_ID] and
        option['product_option_value_id'] == param[PRODUCT_OPTION_VALUE_ID],
        params))
    if len(parametros) > 1:
        raise OcException(ERRO_MULTIPLAS_OPCOES_ENCONTRADA.format(oc_product))
    param = parametros[0]

    # valida
    sku_op = str(oc_product['sku']) + '-'\
        + str(option['product_option_value_id'])
    if sku_op == item['sku']:
        item['codproduto'] = param[CODPRODUTO]
        estoque_fisico = option['quantity_fisica']
        opcao_id = option['product_option_value_id']
        saldo_online = 0  #  online não estoca produtos com opção tipo capacetes
        logger.info(LOG_MOVIMENTACAO.format(oc_product, opcao_id))
        qtd_inteirar = qtd_vendida - saldo_online

        item['origens'] = __get_origem_estoques__(qtd_vendida,
                                                    saldo_online,
                                                    qtd_inteirar)
        processar_saldo_venda(oc_product['product_id'], estoque_fisico,
                                qtd_inteirar,item, opcao_id, **kwargs)
        if __get_versao_notificar__() == '1':
            notify.notificar_separar_estoques(param[CODPRODUTO], qtd_inteirar, **kwargs)
        return
    raise OcException(ERRO_NAO_ENCONTRADO.format(item['sku']))


def __get_origem_estoques__(quantidade_vendida, estoque_online, qtd_inteirar):
    '''
    Processa de onde serão as origens do estoque para cumprir com a venda.
    Preenche um array 'origens' com objetos identificando 'loja' e 'quantidade'
    '''
    origens = []
    # item completo loja online
    if qtd_inteirar == 0:
        origens = [__build_origem_estoque__(0, quantidade_vendida)]
    # item completo estoque loja 2
    elif qtd_inteirar == quantidade_vendida:
        origens = [__build_origem_estoque__(2, quantidade_vendida)]
    # item parcial estoque loja 2
    else:
        origens = [__build_origem_estoque__(2, qtd_inteirar),
                   __build_origem_estoque__(0, estoque_online)]
    return origens


def update_conference_revisao(id_conferencia, codproduto):
    #GET sn_conference
    URL_BASE = os.getenv('OC_URL_SN_CONFERENCE')
    url = '{0}?filter=id_conferencia,eq,{1}&filter=codproduto,eq,{2}'.format(URL_BASE, id_conferencia, codproduto)
    response = req_opencart(url)
    data = response['sn_conference']
    sn_conference = convert_api_response_to_object(data['columns'], data['records'])[0]
    sn_conference['done'] = 'R'

    #PUT sn_conference
    url = '{0}/{1},{2}'.format(URL_BASE, codproduto, id_conferencia)
    req_opencart(url , 'PUT', data=sn_conference)


def __get_versao_notificar__():
    return os.getenv('VERSAO_NOTIFICAR_VENDAS')