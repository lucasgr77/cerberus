# -*- coding:utf-8 -*-
import requests
import time
import os
import json
import datetime
from bs4 import BeautifulSoup

from ..exceptions import TiException
from ..extensions import cached_file
from .. import extensions
from .. import log_server
from ..produtos.service import get_tiny_id_from_sku
from ..produtos import service as produto_service
from ..produtos import models as produto_model
from ..produtos.models import ProdutoTiny
from ..sistema import service as sv
import opencart
from ..database import LocalStorage
from models import Loja
from HTMLParser import HTMLParser
from ..api import req_tiny


def atualizar_estoque_tiny(product_id, id_tiny, saldo, **kwargs):
    saldo_ant = kwargs['saldo_ant'] if 'saldo_ant' in kwargs else None
    opcao_id = kwargs['opcao_id'] if 'opcao_id' in kwargs else None

    if not id_tiny:
        raise TiException("Id Nulo")
    date = time.strftime('%Y-%m-%d %H:%M:00', time.localtime())

    url_base = os.getenv('TINY_URL_ATUALIZAR_ESTOQUE')
    token = os.getenv('TINY_TOKEN')
    payload  = '{"estoque":{"idProduto": "'+str(id_tiny)+'","tipo": "B","data":"'+str(date)+'" ,"quantidade":"'+str(saldo)+'","observacoes": "Lancamento via Cerberus"}}'
    url = '{0}?token={1}&formato=json&estoque={2}'.format(url_base, token, payload)
    req_tiny(url, "POST")
    if saldo_ant is not None:
        sv.log_movimentacao_estoque(product_id, Loja.ONLINE, 'tiny', saldo_ant, saldo, 'F', opcao_id)
    return True


def atualiza_saldo_tiny(product_id, sku, saldo, **kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs else log_server.cerb_logger(__name__)
    try:
        id_tiny = get_tiny_id_from_sku(sku, **kwargs)
        atualizar_estoque_tiny(product_id, id_tiny, saldo, **kwargs)
    except Exception as ex:
        logger.error(ex)
        extensions.exception_notifier(ex)


def obter_produto(product_id, **kwargs):
    url_base = os.getenv('TINY_URL_OBTER_PRODUTO')
    token = os.getenv('TINY_TOKEN')
    url = '{0}?token={1}&formato=json&id={2}'.format(
        url_base, token, product_id
    )
    payload = req_tiny(url, **kwargs)
    return payload['retorno']['produto']


def alterar_produto(produto, **kwargs):
    LM_ENVIAR_PRODUTO = 'Alterando produto no Tiny: {0}'
    h = HTMLParser()

    ambiente = os.getenv('AMBIENTE')

    logger = kwargs['logger'] if 'logger' in kwargs \
        else log_server.cerb_logger(__name__)
    logger.info(LM_ENVIAR_PRODUTO.format(produto['id']))

    token = os.getenv('TINY_TOKEN')
    produto['sequencia'] = 1
    descricao = BeautifulSoup(produto['descricao_complementar'], 'html.parser')
    try:
        if bool(BeautifulSoup(descricao.text, "html.parser").find()):
            produto['descricao_complementar'] = BeautifulSoup(descricao.text, "html.parser").text
    except:
        logger.error('Erro ao parsear descricao produto: ' + produto['nome'])
        produto['descricao_complementar'] = ''
    produto['altura_embalagem']= produto['alturaEmbalagem']
    produto['largura_embalagem']= produto['larguraEmbalagem']
    produto['comprimento_embalagem'] = produto['comprimentoEmbalagem']
    url_base = os.getenv('TINY_URL_ALTERAR_PRODUTO')
    url = '''{0}?token={1}&formato=json&produto={{"produtos":[{{"produto": {2}}}]}}'''.format(url_base, token, json.dumps(produto))
    logger.info('request POST: ' + url)
    req_tiny(url, "POST", logger=logger)
    return True


def confere_atualiza_cadastro(produtos_associados, **kwargs):
    logger = kwargs['logger'] if 'logger' in kwargs else log_server.cerb_logger(__name__)

    session = LocalStorage.get_session()
    logger.info('Iniciando subrotina: confere_atualiza_cadastro')
    produtos_incompletos = produto_service.get_produtos_tiny_incompletos(produtos_associados, **kwargs)
    logger.info('Temos {0} produtos para atualizar'.format(len(produtos_incompletos)))
    for produto_associado in produtos_incompletos:
        try:
            product_id = produto_service.get_tiny_id_from_sku(produto_associado['sku'], **kwargs)
            cadastro = obter_produto(product_id, **kwargs)
            logger.info('avaliando o produto: ' + cadastro['nome'])
            if cadastro['peso_bruto'] > 1000:
                cadastro['peso_bruto'] = float(cadastro['peso_bruto']) / float(1000)
            if cadastro['peso_liquido'] > 1000:
                cadastro['peso_liquido'] = float(cadastro['peso_liquido']) / float(1000)
            cadastro_copia = dict(cadastro)
            if not cadastro['descricao_complementar'] or not cadastro['alturaEmbalagem'] or not cadastro['comprimentoEmbalagem'] \
                    or not cadastro['larguraEmbalagem'] or not cadastro['unidade'] or not cadastro['origem'] or not cadastro['ncm']:
                oc_product = opencart.req_oc_product(produto_associado['product_id'], **kwargs)
                oc_product_description = opencart.req_oc_product_description(produto_associado['product_id'], **kwargs)
                cadastro = __parse_parameters_cadastro__(cadastro, produto_associado, oc_product_description, oc_product)
            if cadastro_copia != cadastro:
                alterar_produto(cadastro, **kwargs)
            prod_tiny = ProdutoTiny(product_id=produto_associado['product_id'], codproduto=produto_associado['codproduto'])
            session.add(prod_tiny)
            session.commit()
        except Exception as ex:
            logger.error(ex)
            extensions.exception_notifier(ex)


def __parse_parameters_cadastro__(cadastro, produto_associado, oc_product_description, oc_product):
    cadastro['ncm'] = cadastro['ncm'] if  cadastro['ncm'] else produto_model.get_ncm(produto_associado['codproduto'])
    cadastro['descricao_complementar'] = cadastro['descricao_complementar'] if cadastro['descricao_complementar'] else oc_product_description['description']
    cadastro['alturaEmbalagem'] = __float_parameter__('alturaEmbalagem', oc_product['height'], cadastro)
    cadastro['larguraEmbalagem'] = __float_parameter__('larguraEmbalagem', oc_product['length'], cadastro)
    cadastro['comprimentoEmbalagem'] = __float_parameter__('comprimentoEmbalagem', oc_product['width'], cadastro)
    cadastro['unidade'] = cadastro['unidade'] if cadastro['unidade'] else 'UN'
    cadastro['origem'] = cadastro['origem'] if cadastro['origem'] else '0'
    return cadastro


def __float_parameter__(key, expected_value, cadastro):
    if not cadastro[key] or float(cadastro[key]) <= 0:
        return expected_value
    return cadastro[key]


@cached_file
def pedido_obter(numero_pedido, **kwargs):
    url_base = os.getenv('TINY_URL_PEDIDO_OBTER')
    token = os.getenv('TINY_TOKEN')
    itens = []
    url = '{0}/?token={1}&formato=json&id={2}'\
        .format(url_base, token, numero_pedido)

    payload = req_tiny(url, **kwargs)
    for i in payload["retorno"]["pedido"]['itens']:
        retorno = {
            'id_produto_tiny': i['item']['id_produto'],
            'quantidade': i['item']['quantidade'],
            'sku':     i['item']['codigo'],
            'valor_unitario': i['item']['valor_unitario'],
            'loja': 0,
            'separacao_loja': 'n'
        }
        itens.append(retorno)
    frete = float(payload['retorno']['pedido']['valor_frete'])
    total_produtos = float(payload['retorno']['pedido']['total_produtos'])
    return itens, frete, total_produtos
