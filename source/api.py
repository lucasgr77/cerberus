# encoding: utf-8
import requests
import os
from .exceptions import OcException, TiException, MlException, SkyHubException
from .extensions import log_server
from .database import LocalStorage
from .sistema.models import MercadoLivreToken
from source.models import ControleRequest
import time
from datetime import datetime, date

API_BLOQUEADA = '6'


def req_opencart(url, verb='GET', **kwargs):
    TOKEN = os.getenv('OPENCART_TOKEN')
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    retry = kwargs['retry'] if 'retry' in kwargs else 0
    tempo_espera = kwargs['tempo_espera'] if 'tempo_espera' in kwargs\
        else 15
    data = kwargs['data'] if 'data' in kwargs else None
    env = os.getenv('AMBIENTE')
    headers = {'key': TOKEN, 'content-type': 'application/json'}
    response = requests.request(verb, url, json=data, headers=headers)
    if response.status_code not in (200, 404) and retry < 3:
        logger.info('Erro chamada opencart: ' + response.content)
        if env == 'prod':
            time.sleep(tempo_espera)
        retry = retry + 15
        tempo_espera = tempo_espera * 2
        return req_opencart(url, verb=verb,
                            data=data, retry=retry, tempo_espera=tempo_espera)
    try:
        return response.json()
    except ValueError as e:
        logger.error(e)
        raise OcException(response.text)


def req_tiny(url, verb='GET', **kwargs):
    '''
    Funcao que faz requests para Tiny com tratativa de
    espera em caso da api bloquear.
    Aguarda 15 segundos e tenta novamente por 3x.
    Caso retorne api bloqueada na 3a tentativa, retorna o erro.
    Caso retorne outro erro sem ser API BLOQUEADA, ele Nao tenta de novo

    Parametro: URL do tiny pronta
    '''
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    retry = kwargs['retry'] if 'retry' in kwargs else 0
    tempo_espera = kwargs['tempo_espera'] if 'tempo_espera' in kwargs\
        else 15
    env = os.getenv('AMBIENTE')

    logger.info('request Tiny ' + verb + ': ' + url)
    response = requests.request(verb, url)
    logger.info('response Tiny: ' + response.content)
    try:
        payload = response.json()
        log_request('req_tiny', verb, url, response.request.body, response.text)
    except ValueError:
        raise TiException(response.text)
    if response.status_code == 200 and 'erros' not in payload['retorno']:
        return response.json()
    elif 'erros' in payload['retorno'] and payload['retorno']['codigo_erro'] == API_BLOQUEADA and retry < 3:
        logger.info('(Tiny) Api bloqueada retry em: ' + str(tempo_espera))
        if env == 'prod':
            time.sleep(tempo_espera)
        retry = retry + 1
        tempo_espera = tempo_espera * 2
        return req_tiny(url, retry=retry, tempo_espera=tempo_espera)
    mensagem_erro = ', '.join(map(lambda x: x['erro'],
                                  payload['retorno']['erros']))
    raise TiException(mensagem_erro)


def req_skyhub(url, verb='GET', **kwargs):
    '''
    Funcao que faz requests para Skyhub com tratativa

    Parametro: URL do tiny pronta
    '''
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    retry = kwargs['retry'] if 'retry' in kwargs else 0
    tempo_espera = kwargs['tempo_espera'] if 'tempo_espera' in kwargs\
        else 15
    data = kwargs['data'] if 'data' in kwargs else None
    env = os.getenv('AMBIENTE')
    TOKEN = os.getenv('SKYHUB_TOKEN')
    EMAIL = os.getenv('SKYHUB_EMAIL')

    logger.info('request Skyhub' + verb + ': ' + url)
    headers = {'X-Api-Key': TOKEN, 'X-User-Email': EMAIL,
               'content-type': 'application/json'}
    response = requests.request(verb, url, json=data, headers=headers)
    logger.info('response Skyhub: ' + response.content)
    if response.status_code == 204:
        return True
    elif response.status_code in [500, 400] and retry < 3:
        logger.info('(Skyhub) Api bloqueada retry em: ' + str(tempo_espera))
        if env == 'prod':
            time.sleep(tempo_espera)
        retry = retry + 1
        tempo_espera = tempo_espera * 2
        return req_skyhub(url, verb=verb, retry=retry, tempo_espera=tempo_espera)
    raise SkyHubException(response.text)


def get_token():
    session = LocalStorage.get_session()
    MlToken = session.query(MercadoLivreToken).first()
    session.close()
    if MlToken is None:
        raise Exception('Favor configurar token do Mercado Livre no Banco!')
    return MlToken


def refresh_token_mercado_livre():
    token = get_token()
    url_refresh = os.getenv('MERCADOLIVRE_URL_REFRESH_TOKEN')
    client_id = os.getenv('MERCADOLIVRE_CLIENT_ID')
    secret = os.getenv('MERCADOLIVRE_SECRET')
    url = url_refresh + '&client_id={0}&client_secret={1}&refresh_token={2}'.format(
        client_id, secret, token.refresh_token
    )
    response = requests.post(url)
    if response.status_code == 200:
        session = LocalStorage.get_session()
        body = response.json()
        token.access_token = 'Bearer ' + body['access_token']
        token.refresh_token = body['refresh_token']
        session.add(token)
        session.commit()
        session.close()
    else:
        raise Exception('Nao foi possÃ­vel dar refresh no token do Mercado Livre')


def req_mercado_livre(url, verb='GET', **kwargs):
    TOKEN = get_token().access_token
    logger = kwargs['logger'] if 'logger' in kwargs\
        else log_server.cerb_logger(__name__)
    retry = kwargs['retry'] if 'retry' in kwargs else 0
    tempo_espera = kwargs['tempo_espera'] if 'tempo_espera' in kwargs\
        else 15
    data = kwargs['data'] if 'data' in kwargs else None
    env = os.getenv('AMBIENTE')
    headers = {'Authorization': TOKEN, 'content-type': 'application/json'}
    logger.info('request Mercado Livre ' + verb + ': ' + url)
    response = requests.request(verb, url, json=data, headers=headers)
    logger.info('response Mercado Livre: ' + response.content)
    if response.status_code == 401:
        refresh_token_mercado_livre()
        return req_mercado_livre(url, verb=verb,
                            data=data, retry=retry, tempo_espera=tempo_espera)
    if response.status_code not in (200, 404, 400) and retry < 3:
        logger.info('Erro chamada Mercado Livre: ' + response.content)
        if env == 'prod':
            time.sleep(tempo_espera)
        retry = retry + 15
        tempo_espera = tempo_espera * 2
        return req_mercado_livre(url, verb=verb,
                            data=data, retry=retry, tempo_espera=tempo_espera)
    try:
        payload = response.text
        if verb == 'GET':
            payload = response.json()
            if 'error' in payload and len(payload['cause']) > 0:
                raise MlException(payload['cause'][0]['message'])
            return payload
        return payload
    except ValueError as e:
        logger.error(e)
        raise MlException(response.text)


def convert_api_response_to_object(columns, records):
    response = []
    for register in records:
        object = {}
        i = 0
        for column in columns:
            object[column] = register[i]
            i += 1
        response.append(object)
    return response


def log_request(metodo_origem, tipo, url, payload_enviado, payload_recebido):
    data = datetime.now()
    session = LocalStorage.get_session()
    controle_req = ControleRequest(metodo_origem=metodo_origem,
                                  tipo=tipo,
                                  url=url,
                                  payload_enviado=payload_enviado,
                                  payload_recebido=payload_recebido,
                                  data_hora=data)
    session.add(controle_req)
    session.commit()
    session.close()
