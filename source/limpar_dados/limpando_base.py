import time
import datetime
from source.database import LocalStorage
from source.sistema.models import JobExecucao
from source.estoque.models import MovimentacaoEstoque



def limpar_dados_job_execusao():
    hoje = (datetime.datetime.now())
    seis_meses_atras = (hoje - datetime.timedelta(days=180))
    session = LocalStorage.get_session()
    tabela_job_exec = session.query(JobExecucao).filter(JobExecucao.inicio < seis_meses_atras).all()
    for registro in tabela_job_exec:
        session.query(JobExecucao).filter(JobExecucao.id == registro.id).delete()
    session.commit()
    session.close()


def limpar_dados_movimenta_estoque():
    hoje = (datetime.datetime.now())
    seis_meses_atras = (hoje - datetime.timedelta(days=180))
    session = LocalStorage.get_session()
    mov_estoque = session.query(MovimentacaoEstoque).filter(MovimentacaoEstoque.data < seis_meses_atras).all()
    for registro in mov_estoque:
        session.query(MovimentacaoEstoque).filter(MovimentacaoEstoque.id < registro.id).delete()
    session.commit()
    session.close()


def limpar_bases():
    limpar_dados_job_execusao()
    limpar_dados_movimenta_estoque()