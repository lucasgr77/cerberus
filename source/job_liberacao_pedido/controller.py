from source.job_liberacao_pedido import opencart as op
from source.notify.slack import enviar_mensagem
import json
import os
import requests

def liberar_pedidos():
    pedidos = op.get_pedidos_travados()
    url_canal = os.getenv('URL_SLACK_IMPORTACAO_VENDAS')
    for pedido in pedidos:
        dados_do_pedido ='Temos um novo pedido, numero do pedido: {0}, nome do cliente: {1} {2}, {3}'.format(pedido['store_id'], pedido['firstname'], pedido['lastname'], '<@U020A1F4G7M>') 
        op.libera_pedido(pedido['order_id'])
        enviar_mensagem(dados_do_pedido, url_canal)
