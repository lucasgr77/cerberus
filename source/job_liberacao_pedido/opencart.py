from source.api import req_opencart, convert_api_response_to_object
import os


def get_pedidos_travados():
    url_base = os.getenv('OC_URL_BASE')
    url = '{0}/oc_order?filter=order_status_id,eq,0'.format(url_base)
    response = req_opencart(url)
    columns = response['oc_order']['columns']
    records = response['oc_order']['records']
    return convert_api_response_to_object(columns, records)


def libera_pedido(id_pedido_oc):
    STATUS_DESBLOQUEADO = 2
    url_base = os.getenv('OC_URL_BASE')
    url = '{0}/oc_order/{1}'.format(url_base, id_pedido_oc)
    body = {'order_status_id': STATUS_DESBLOQUEADO}
    req_opencart(url, verb='PUT', data=body)
