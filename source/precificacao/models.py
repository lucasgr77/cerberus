from source.database import LocalStorage
from ..produtos.models import ProdutoTiny
from sqlalchemy import Column, Integer, String, Float, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from ..settings import Base
import datetime

class  PrecificacaoItem(Base):
    '''
    Classe de controle de Precificacao de cada Item
    
    Status: 
    INSERIDO = 0
    CALCULADO_PRECO = 1
    ATUALIZADO_ML = 2
    ATUALIZADO_B2W_MAGALU = 3
    FINALIZADO = 4
    '''
    __tablename__ = 'precificacao_item'

    id = Column(Integer, primary_key=True)
    erro = Column(String)
    codproduto = Column(Integer)
    preco_custo = Column(Float)
    preco_compra = Column(Float)
    preco_venda = Column(Float)
    preco_ml = Column(Float)
    preco_b2w = Column(Float)
    preco_magalu = Column(Float)
    preco_oc = Column(Float)
    data_alteracao = Column(String)
    conc_ml_media = Column(Float)
    conc_ml_minima = Column(Float)
    status = Column(Integer)

    def __repr__(self):
        return '<PrecificacaoItem {0} codproduto={1}>'.format(self.id, self.codproduto)



def get_precificacao_item(codproduto):
    session = LocalStorage.get_session()
    hoje = datetime.datetime.now().date()
    retorno = session.query(PrecificacaoItem).filter(
        PrecificacaoItem.codproduto == codproduto,
        PrecificacaoItem.data_alteracao == hoje).first()
    FINALIZADO = 4
    session.close()
    if retorno is None:
        return __get_precificacao_by_data__(codproduto)
    elif retorno.status < FINALIZADO:
        return retorno
    else:
        return None


def __get_precificacao_by_data__(codproduto):
    hoje = datetime.datetime.now().date()
    session = LocalStorage.get_session()
    precificacaoItem = PrecificacaoItem(
        codproduto=codproduto,
        data_alteracao=hoje,
        status=0
    )
    session.add(precificacaoItem)
    session.commit()
    precificacao = session.query(PrecificacaoItem).filter(
        PrecificacaoItem.codproduto == codproduto,
        PrecificacaoItem.data_alteracao == hoje).first()
    session.close()
    return precificacao


def save_precificacao(precificacao):
    session = LocalStorage.get_session()
    session.add(precificacao)
    session.commit()
    session.close()


class ControleSku(Base):
    __tablename__ = 'controle_sku'

    sku = Column(String, primary_key=True)

    def __init__(self, sku):
        self.sku = sku


def desabilita_sku(sku):
    session = LocalStorage.get_session()
    controle_sku = ControleSku(sku)
    session.add(controle_sku)
    session.commit()
    session.close()

def sku_esta_habilitado(sku):
    session = LocalStorage.get_session()
    desabilitados = session.query(ControleSku).filter(ControleSku.sku == sku).all()
    session.close()
    if len(desabilitados) == 0:
        return True
    else:
        return False