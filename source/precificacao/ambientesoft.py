import datetime
from ..database import Firebird
import os


def get_precos_atualizados():
    CODPRODUTO = 0
    PRECO_CUSTO = 1
    PRECO_COMPRA = 2
    PRECO_VENDA = 3
    DATA_ALT_PRECO = 4
    dias_precificacao = float(os.getenv('DIAS_PRECIFICACAO'))

    produtos_atualizar_preco =[]
    data_de_hoje = datetime.datetime.now().date()
    tres_dias_atras = (data_de_hoje - datetime.timedelta(days=dias_precificacao)).strftime('%Y-%m-%d')
    cursor = Firebird.get_connection()
    q = '''SELECT CODPRODUTO, PRECO_CUSTO, PRECO_COMPRA, PRECO_VENDA, DATA_ALT_PRECO FROM PRODUTO_PRECO 
        WHERE DATA_ALT_PRECO >= '{0}'
        GROUP BY CODPRODUTO, PRECO_CUSTO, PRECO_COMPRA, PRECO_VENDA, DATA_ALT_PRECO '''.format(tres_dias_atras)
    cursor.execute(q)
    rows = cursor.fetchall()
    cursor.close()
    for row in rows:
        produto = {
            "codproduto":row[CODPRODUTO],
            "preco_custo":row[PRECO_CUSTO],
            "preco_compra":row[PRECO_COMPRA],
            "preco_venda":row[PRECO_VENDA],
            "data_alt_preco":row[DATA_ALT_PRECO]
        }
        produtos_atualizar_preco.append(produto)
    return produtos_atualizar_preco

def get_precos_by_codproduto(codproduto):
    CODPRODUTO = 0
    PRECO_CUSTO = 1
    PRECO_COMPRA = 2
    PRECO_VENDA = 3
    DATA_ALT_PRECO = 4
    cursor = Firebird.get_connection()
    q = '''SELECT pp.CODPRODUTO, pp.PRECO_CUSTO, pp.PRECO_COMPRA, pp.PRECO_VENDA, pp.DATA_ALT_PRECO 
        FROM PRODUTO_PRECO pp, PRODUTO_LOJA pl 
        WHERE pp.CODPRODUTO = {0}
        AND pp.CODPRODUTOLOJA  = pl.CODPRODUTOLOJA 
        AND pl.CODFILIAL  = 7 '''.format(codproduto)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    return {
            "codproduto": row[CODPRODUTO],
            "preco_custo": row[PRECO_CUSTO],
            "preco_compra": row[PRECO_COMPRA],
            "preco_venda": row[PRECO_VENDA],
            "data_alt_preco": row[DATA_ALT_PRECO]
        }