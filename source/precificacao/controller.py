# -*- coding:utf-8 -*-
import os
import ambientesoft as amb
import datetime
from models import get_precificacao_item, save_precificacao, desabilita_sku, sku_esta_habilitado
from mercado_livre import get_preco_concorrencia
from source.precificacao.opencart import (
    ProdutoOcNotFound,
    get_product_description
)
from source.precificacao import opencart as op
from source.precificacao.mercado_livre import (
    get_mlid_por_sku,
    get_mlid_por_descricao,
    MlException,
    consulta_taxa_comissao,
    consulta_valor_frete,
    altera_preco_ml
)
from source.precificacao import mercado_livre as ml
from .. import extensions
from ..api import req_opencart, req_skyhub


class RemoveProdutoException(Exception):
    pass


INSERIDO = 0
CALCULADO_PRECO = 1
FINALIZADO = 2
ERRO = 9
LOG_INICIO = 'Tem(os) {0} produto(s) AMBIENTE para precificar'
LOG_PRODUTO = 'Precificando codproduto: {0}'

LOG_MSG_ATUALIZACAO = 'Atualizando sku: {0} {1}, preço: {2}'


def execute(logger):
    # Ambientesoft
    produtos = amb.get_precos_atualizados()
    logger.info(LOG_INICIO.format(len(produtos)))
    for produto in produtos:
        precificacao = get_precificacao_item(produto['codproduto'])
        try:
            if precificacao is None:
                continue
            logger.info(LOG_PRODUTO.format(produto['codproduto']))
            calcula_preco(produto, precificacao)
            atualiza_preco(produto, precificacao, logger)
            precificacao.status = FINALIZADO
        except ProdutoOcNotFound as ex:
            logger.error(ex)
        except Exception as ex:
            logger.error(ex)
            extensions.exception_notifier(ex)
            precificacao.erro = ex.message
            precificacao.status = ERRO
        finally:
            if precificacao is not None:
                save_precificacao(precificacao)

    # Opencart
    produtos = op.ultimos_produtos_atualizados(logger)
    logger.info('Tem(os) {0} produto(s) OPENCART para precificar'.format(len(produtos)))
    for produto in produtos:
        precificacao = get_precificacao_item(produto['codproduto'])
        try:
            if precificacao is None:
                continue
            if precificacao.status < FINALIZADO:
                pp = amb.get_precos_by_codproduto(produto['codproduto'])
                produto['preco_custo'] = pp['preco_custo']
                produto['preco_compra'] = pp['preco_compra']
                produto['preco_venda'] = pp['preco_venda']
                logger.info('Precificando codproduto: ' + str(produto['codproduto']))
                calcula_preco(produto, precificacao)
                atualiza_preco(produto, precificacao, logger)
        except Exception as ex:
            logger.error(ex)
            extensions.exception_notifier(ex)
            precificacao.erro = ex.message
            precificacao.status = ERRO
        finally:
            if precificacao is not None:
                save_precificacao(precificacao)


def calcula_preco(produto, precificacao):
    precificacao.preco_custo = produto['preco_custo']
    precificacao.preco_compra = produto['preco_compra']
    precificacao.preco_venda = produto['preco_venda']
    oc_product = op.get_oc_product_sku(op.get_product_id_by_codproduto(produto['codproduto']), logger)
    produto['oc_product'] = oc_product
    produto['product_id'] = oc_product['product_id']
    produto['sku'] = oc_product['sku']
    produto['id_ml'] = get_id_ml(oc_product)
    produto['cadastro_ml'] = ml.get_cadastro_ml(produto['id_ml'])
    tem_precificacao = tem_precificacao_fixa(oc_product)
    if (oc_product['price_ml'] is not None and float(oc_product['price_ml']) > 0) \
        and tem_precificacao:
        precificacao.preco_ml = round(float(oc_product['price_ml']), 2)
    else:
        precificacao.preco_ml = calcula_preco_ml(produto, precificacao)

    if (oc_product['price_b2w'] is not None and float(oc_product['price_b2w']) > 0) \
        and tem_precificacao:
        precificacao.preco_b2w = round(float(oc_product['price_b2w']), 2)
    else:
        precificacao.preco_b2w = calcula_preco_b2w(produto)
    if oc_product['price_magalu'] is not None and float(oc_product['price_magalu']) > 0 \
        and tem_precificacao:
        precificacao.preco_magalu = round(float(oc_product['price_magalu']), 2)
    else:
        precificacao.preco_magalu = calcula_preco_magalu(produto)
    precificacao.preco_oc = calcula_preco_oc(produto, precificacao)
    precificacao.status = CALCULADO_PRECO


def calcula_preco_ml(produto, precificacao):
    cadastro_ml = produto['cadastro_ml']

    # Taxa
    taxa_comissao = consulta_taxa_comissao(cadastro_ml['category_id'], produto['preco_venda'])
    preco_venda_final = taxa_comissao + float(produto['preco_venda'])

    # Margem Lucro
    preco_venda_final = preco_venda_final * 1.04

    # Frete
    frete = consulta_valor_frete(cadastro_ml['id'])
    preco_venda_final = preco_venda_final + frete
    if preco_venda_final < 7:
        preco_venda_final = 7 + float(produto['preco_venda'])
    produto['frete'] = frete

    # concorrencia
    concorrencia = get_preco_concorrencia(cadastro_ml['title'], cadastro_ml['category_id'])
    if concorrencia['minimo'] > 0:
        diferenca_minima = preco_venda_final / concorrencia['minimo']
        diferenca_media = preco_venda_final / concorrencia['media']
        precificacao.conc_ml_media = concorrencia['media']
        precificacao.conc_ml_minima = concorrencia['minimo']
        #Ajuste preço com concorrencia
        if diferenca_minima > 1 and diferenca_minima < 1.05:
            preco_venda_final = calcula_ajuste_preco_ml(diferenca_minima, preco_venda_final)
        elif diferenca_minima < 1:
            preco_venda_final = calcula_ajuste_preco_ml(diferenca_minima, preco_venda_final)
        elif diferenca_media > 1 and diferenca_media < 1.05:
            preco_venda_final = calcula_ajuste_preco_ml(diferenca_media, preco_venda_final)
        elif diferenca_media < 1:
            preco_venda_final = calcula_ajuste_preco_ml(diferenca_media, preco_venda_final)
        if (preco_venda_final - float(produto['preco_custo']) - float(taxa_comissao)) < 1:
            preco_venda_final = float(produto['preco_venda'])  + frete + taxa_comissao
    return '{:.2f}'.format(preco_venda_final)


def get_id_ml(oc_product):
    id_ml = get_mlid_por_sku(oc_product['sku'])
    if id_ml is None:
        descricao = get_product_description(oc_product['product_id'])
        id_ml = get_mlid_por_descricao(descricao)
    if id_ml is None:
        raise MlException('Erro chamada get_mlid_por_descricao')
    return id_ml


def calcula_ajuste_preco_ml(diferenca, preco_venda):
    percentual_ajuste = 2 - diferenca
    arredondamento = 0.01 if percentual_ajuste < 1 else 1
    return preco_venda * percentual_ajuste - arredondamento


def calcula_preco_b2w(produto):
    oc_product = produto['oc_product']
    habilitado = sku_esta_habilitado(oc_product['sku'])
    if habilitado:
        peso = peso_embalagem_b2w(oc_product['length'], oc_product['width'],
            oc_product['height'], oc_product['weight'], oc_product['sku'])
        preco_venda = float(produto['preco_venda'])
        preco_final_b2w = preco_venda * 1.175 + 5
        preco_frete = calculo_preco_frete_b2w(preco_final_b2w, peso)
        return '{:.2f}'.format(preco_final_b2w + preco_frete)
    else:
        return None


def calcula_preco_magalu(produto):
    preco_venda = float(produto['preco_venda'])
    if preco_venda <= 79.00:
        preco_venda = (preco_venda + 4.90) / 0.90
    elif preco_venda > 79.00:
        preco_venda = (preco_venda + 39.90) / 0.88
    return round(preco_venda, 2)



def peso_embalagem_b2w(comprimento, largura, altura, peso_fisico, sku):
    PESO_BASE = 5.0
    LIMITE_MAXIMO_CM = 90
    SOMA_TODOS_LADOS_PRODUTO = 200 / 100
    LIMITE_PESO_CUBICO = 30
    comprimento = float(comprimento) / 100
    largura = float(largura) / 100
    altura = float(altura) / 100
    peso_fisico = float(peso_fisico)
    soma_produto = float(comprimento + largura + altura)
    peso_cubico = ((comprimento * altura * largura) / 6000) * 1000000
    if soma_produto > SOMA_TODOS_LADOS_PRODUTO or \
        peso_cubico > LIMITE_PESO_CUBICO:
        desabilitar_produto_b2w(sku)
    if comprimento > LIMITE_MAXIMO_CM or\
       largura > LIMITE_MAXIMO_CM or\
       altura > LIMITE_MAXIMO_CM:
        desabilitar_produto_b2w(sku)
    if peso_cubico < PESO_BASE:
        peso_gramas = peso_fisico
    if peso_cubico > PESO_BASE:
        peso_gramas = max(peso_cubico, peso_fisico)
    return peso_gramas


def calculo_preco_frete_b2w(preco_final_b2w, peso_gramas):
    CEM_REAIS = 100
    if preco_final_b2w >= CEM_REAIS:
        return custo_frete_gratis(peso_gramas)
    else:
        return 12.90


def custo_frete_gratis(peso_gramas):
    lista_precos_frete = {
        "peso1_ate_499g": 30.99,
        "peso2_de_500g_a_999g": 33.99,
        "peso3_de_1kg_a_1999kg": 34.99,
        "peso4_de_2kg_a_4999kg": 43.99,
        "peso5_de_5kg_a_8999kg": 63.99,
        "peso6_de_9kg_a_12999kg": 99.99,
        "peso7_de_13kg_a_16999kg": 110.99,
        "peso8_de_17kg_a_22999kg": 129.99,
        "peso9_de_23kg_a_28999kg": 149.99,
        "peso10_29000kg": 169.99
        }
    peso_gramas = peso_gramas * 1000
    taxa_frete = 0
    if peso_gramas <= 499 :
        taxa_frete = lista_precos_frete["peso1_ate_499g"]
    elif peso_gramas >= 500 and peso_gramas <= 999:
        taxa_frete = lista_precos_frete["peso2_de_500g_a_999g"]
    elif peso_gramas >= 1000 and peso_gramas <= 1999:
        taxa_frete = lista_precos_frete["peso3_de_1kg_a_1999kg"]
    elif peso_gramas >= 2000 and peso_gramas <= 4999:
        taxa_frete = lista_precos_frete["peso4_de_2kg_a_4999kg"]
    elif peso_gramas >= 5000 and peso_gramas <= 8999:
        taxa_frete = lista_precos_frete["peso5_de_5kg_a_8999kg"]
    elif peso_gramas >= 9000 and peso_gramas <= 12999:
        taxa_frete = lista_precos_frete["peso6_de_9kg_a_12999kg"]
    elif peso_gramas >= 13000 and peso_gramas <= 16999:
        taxa_frete = lista_precos_frete["peso7_de_13kg_a_16999kg"]
    elif peso_gramas >= 17000 and peso_gramas <= 22999:
        taxa_frete = lista_precos_frete["peso8_de_17kg_a_22999kg"]
    elif peso_gramas >= 23000 and peso_gramas <= 28000:
        taxa_frete = lista_precos_frete["peso9_de_23kg_a_28999kg"]
    elif peso_gramas > 29000:
        taxa_frete = lista_precos_frete["peso10_29000kg"]
    return taxa_frete




def calcula_preco_oc(produto, precificacao):
    preco_venda_oc = produto['preco_venda']
    return float(preco_venda_oc) * 1.05

def atualiza_preco_oc_product(product_id, preco_venda_oc):
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT')
    url = '{0}/{1}'.format(BASE_URL, product_id)
    data = {
        "price": float(preco_venda_oc)
    }
    req_opencart(url, 'PUT', data=data)

def atualiza_preco(produto, precificacao, logger):
    CD_STATUS_INICIO = 0
    SEM_RETRY = 0
    # Atualiza preço do produto no Mercado Livre, no Opencart e coloca na fila do Decepticon
    sku = produto['sku']
    logger.info(LOG_MSG_ATUALIZACAO.format(sku, 'Mercado Livre', precificacao.preco_ml))
    altera_preco_ml(produto, precificacao.preco_ml)
    # altera_preco_b2w(sku, precificacao.preco_b2w)
    # todo atualizar preco magalu
    op.post_dc_fila_produtos(produto['sku'], precificacao.preco_magalu, CD_STATUS_INICIO, SEM_RETRY)
    logger.info(LOG_MSG_ATUALIZACAO.format(sku, 'Open Cart', precificacao.preco_oc))
    atualiza_preco_oc_product(produto['product_id'], precificacao.preco_oc)


def altera_preco_b2w(sku, preco):
    if preco is None:
        return 0
    url_base = os.getenv('SKYHUB_URL_BASE')
    url = url_base + '/products/' + sku
    body = {
        'product': {
            'price': preco
        }
    }
    req_skyhub(url, 'PUT', data=body)


def tem_precificacao_fixa(oc_product):
    # Returna True se o produto tem um preco fixo com data dentro do prazo
    hoje = datetime.datetime.now().date()
    prazo_valido = True
    if oc_product['date_target_price'] is not None and oc_product['date_target_price'] != '0000-00-00':
        data_target_price = datetime.datetime.strptime(oc_product['date_target_price'], '%Y-%m-%d').date()
        prazo_valido = data_target_price > hoje
    return prazo_valido


def desabilitar_produto_b2w(sku):
    desabilita_sku(sku)
    URL_BASE = os.getenv('SKYHUB_URL_BASE')
    url = '{0}/products/{1}'.format(URL_BASE, sku)
    body = {
        'product':{
            'status': 'disabled'
        }
    }
    req_skyhub(url, 'PUT', data=body)
