# -*- coding:utf-8 -*-
import os
from ..api import req_mercado_livre
from ..extensions import cached_file
from ..database import LocalStorage
from difflib import SequenceMatcher
import string
from datetime import date


class MlException(Exception):
    pass


@cached_file
def get_mlid_por_sku(sku):
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    USER_ID = os.getenv('MERCADOLIVRE_USER_ID')
    url = URL_BASE + 'users/{0}/items/search?seller_sku={1}'.format(USER_ID, sku)
    retorno = req_mercado_livre(url)
    if 'results' in retorno and len(retorno['results']) > 0:
        return retorno['results'][0]
    else:
        None


@cached_file
def get_mlid_por_descricao(descricao):
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    USER_ID = os.getenv('MERCADOLIVRE_USER_ID')
    url = URL_BASE + 'users/{0}/items/search?q={1}'.format(USER_ID, descricao)
    retorno = req_mercado_livre(url)
    if 'results' in retorno and len(retorno["results"]) > 0:
        return retorno["results"][0]
    mensagem_ml = 'Nenhum produto encontrado descricao ' + retorno['query']
    if 'message' in retorno:
        mensagem_ml = retorno['message']
    raise MlException('get_mlid_por_descricao: ' + mensagem_ml)


def consulta_valor_frete(id_produto_ml):
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    url = URL_BASE + 'items/{0}/shipping_options/free'.format(id_produto_ml)
    response = req_mercado_livre(url)
    if "coverage" in response:
        return response['coverage']['all_country']['list_cost']
    elif 'message' in response and 'item without free shipping' in response['message'] \
        or '''invalid item's mode''' in response['message']:
        return 0
    elif 'message' in response:
        raise MlException("Erro na consulta do frete Mercado Livre, Message: " + response['message'])
    else:
        raise MlException('Erro na consulta do valor de frete, id ml: ' + id_produto_ml)


def get_preco_concorrencia(descricao, category_ml):
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    url = URL_BASE + 'sites/MLB/search?q=' + descricao
    retorno = req_mercado_livre(url)
    total = 0
    contagem = 1
    minimo = 0
    total_vendidos = 0
    for produto in retorno['results']:
        if similaridade(produto['title'], descricao) > 0.70 \
            and produto['category_id'] == category_ml\
                and produto['seller']['id'] != 307700862:
            total_vendidos += produto['sold_quantity']
            total += produto['price']
            contagem += 1
            if minimo == 0 or produto['price'] < minimo:
                minimo = produto['price']
    return {
        'media': total/contagem,
        'minimo': minimo,
        'total_vendidos': total_vendidos,
        'total_sku_concorrentes': contagem
    }


def similaridade(word_a, word_b):
    if not word_a or not word_b:
        return 0
    print(word_a)
    print(word_b)
    word_a = sorted(word_a.lower().split(' '))
    word_b = sorted(word_b.lower().split(' '))

    word_a = ''.join(word_a)
    word_b = ''.join(word_b)

    word_a = ''.join([char for char in list(word_a) if char not in string.punctuation])
    word_b = ''.join([char for char in list(word_b) if char not in string.punctuation])
    print('1: ' +word_a, 'descricao: ' + word_b)

    return SequenceMatcher(None, word_a, word_b).ratio()


def consulta_taxa_comissao(category_id_ml, preco_venda):
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    url = URL_BASE + 'sites/MLB/listing_prices?price={0}&category_id={1}'.format(preco_venda, category_id_ml)
    retorno = req_mercado_livre(url)
    if 'message' in retorno:
        raise MlException('Erro no retorno consulta_taxa_comissao: ' + retorno['message'])
    else:
        for categoria in retorno:
            if categoria['listing_type_id'] == 'gold_special':
                return categoria['sale_fee_amount']
        raise MlException('Erro na consulta do frete Mercado Livre, ID Mercado Livre: ' + category_id_ml)


def get_cadastro_ml(id_ml):
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    url = URL_BASE + 'items/{0}'.format(id_ml)
    retorno = req_mercado_livre(url)
    mensagem_ml = 'Nenhum produto encontrado'
    if 'message' in retorno:
        mensagem_ml = retorno['message']
        raise MlException('Erro chamada get_cadastro_ml, mensagem: ' + mensagem_ml)
    return retorno


def altera_preco_ml(produto, preco):
    cadastro_ml = produto['cadastro_ml']
    id_produto_ml = produto['id_ml']
    URL_BASE = os.getenv('MERCADOLIVRE_URL_BASE')
    url = URL_BASE + 'items/' + id_produto_ml
    # não tem variacao
    if 'variations' not in cadastro_ml or len(cadastro_ml['variations']) == 0:
        body = {
            'price': preco
        }
    else:
        variacoes = list(map(lambda x: {'id': x['id'], 'price': preco},
                             cadastro_ml['variations']))
        body = {
            'variations': variacoes
        }

    req_mercado_livre(url, verb='PUT', data=body)
