from source.database import LocalStorage
from sqlalchemy import Column, Integer, String, Float, String
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from ..settings import Base


class ControleSku(Base):
    __tablename__ = 'controle_sku'

    sku = Column(String, primary_key=True)

    def __init__(self, sku):
        self.sku = sku


def desabilita_sku(sku):
    session = LocalStorage.get_session()
    controle_sku = ControleSku(sku)
    session.add(controle_sku)
    session.commit()
    session.close()

def sku_esta_habilitado(sku):
    session = LocalStorage.get_session()
    desabilitados = session.query(ControleSku).filter(ControleSku.sku == sku).all()
    session.close()
    if len(desabilitados) == 0:
        return True
    else:
        return False