# -*- coding:utf-8 -*-
import os
from ..api import req_opencart
from source.precificacao.mercado_livre import MlException
from datetime import date
from ..extensions import cached_file
from .. import extensions
import datetime


class ProdutoOcNotFound(Exception):

    def __init__(self, codproduto):
        self.message = 'Erro get_product_id_by_codproduto, \
produto nao encontrado associacao, \
codproduto: {0}'.format(codproduto)

    def __str__(self):
        return self.message


@cached_file
def get_product_id_by_codproduto(codproduto):
    PRODUCT_ID = 1
    BASE_URL = os.getenv('OC_URL_PRODUCTS_ASSOCIATION_SIMPLES')
    url = '{0}?filter=codproduto,eq,{1}'.format(BASE_URL, codproduto)
    response_payload = req_opencart(url)
    if len(response_payload['products_association']['records']) == 0:
        raise ProdutoOcNotFound(codproduto)
    for produto in response_payload['products_association']['records']:
        return produto[PRODUCT_ID]


@cached_file
def get_codproduto_by_product_id(product_id):
    CODPRODUTO = 0
    BASE_URL = os.getenv('OC_URL_PRODUCTS_ASSOCIATION_SIMPLES')
    url = '{0}?filter=product_id,eq,{1}'.format(BASE_URL, product_id)
    response_payload = req_opencart(url)
    if len(response_payload['products_association']['records']) == 0:
        raise MlException('Erro get_association_by_product_id, produto nao encontrado associacao, product_id: ' + str(product_id))
    for produto in response_payload['products_association']['records']:
        return produto[CODPRODUTO]


def get_oc_product_sku(product_id, logger):
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT')
    url = '{0}/{1}'.format(BASE_URL, product_id)
    try:
        response_payload = req_opencart(url)
    except Exception as e:
        logger.error(e)
    if 'sku' in response_payload:
        return response_payload
    raise MlException('Erro get_oc_product_sku, chave sku nao existe')


def get_product_description(product_id):
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT_DESCRIPTION')
    url = '{0}/{1}'.format(BASE_URL, product_id)
    response_payload = req_opencart(url)
    return response_payload['name']


def post_dc_fila_produtos(sku, preco, status, retry):
    data_hoje = datetime.datetime.now()
    data_inserido = data_hoje.strftime('%y-%m-%d %H:%M')
    BASE_URL = os.getenv('OC_URL_DC_FILA_PRODUTOS')
    url = '{0}'.format(BASE_URL)
    data = {
        "sku": sku,
        "preco": preco,
        "status": status,
        "retry": retry,
        "data_inserido": data_inserido
    }
    req_opencart(url, 'POST', data=data)


def ultimos_produtos_atualizados(logger):
    PRICE_ML = 32
    PRICE_B2W = 33
    PRICE_MAGALU = 34
    DATE_TARGET_PRICE = 35
    PRODUCT_ID = 0

    dias_precificacao = get_dias_precificacao()
    lista_precos = []
    BASE_URL = os.getenv('OC_URL_OC_PRODUCT')
    url = '{0}?filter=date_modified,gt,{1}'.format(BASE_URL, dias_precificacao)
    response_payload = req_opencart(url)
    for oc_product in response_payload['oc_product']['records']:
        if oc_product[PRICE_ML] is not None or oc_product[PRICE_B2W] is not None or oc_product[PRICE_MAGALU] is not None \
        and (oc_product[DATE_TARGET_PRICE] is None or oc_product[DATE_TARGET_PRICE] > data_comparacao):
            try:
                codproduto = get_codproduto_by_product_id(oc_product[PRODUCT_ID])
            except MlException as e:
                logger.info('PRODUCT_ID: {0} Nao tem vinculo products_association'
                            .format(str(oc_product[PRODUCT_ID])))
                continue
            except Exception as e:
                logger.error(e)
                extensions.exception_notifier(e)
                continue
            retorno = {
                'codproduto': codproduto,
                'product_id': oc_product[PRODUCT_ID],
                'price_ml': oc_product[PRICE_ML],
                'price_b2w': oc_product[PRICE_B2W],
                'price_magalu': oc_product[PRICE_MAGALU]
            }
            lista_precos.append(retorno)
    return lista_precos


def get_dias_precificacao():
    dias_precificacao = float(os.getenv('DIAS_PRECIFICACAO'))
    hoje = datetime.datetime.now().date()
    return (hoje - datetime.timedelta(days=dias_precificacao)).strftime('%Y-%m-%d')
    