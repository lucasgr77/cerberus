import os
from ..database import Firebird
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from ..settings import Base
import datetime

def get_produtos_mais_vendidos():
    cursor = Firebird.get_connection()
    hoje = datetime.datetime.now().date()
    domingo_anterior = (hoje - datetime.timedelta(days=3)).strftime('%Y-%m-%d')
    proximo_domingo = (hoje + datetime.timedelta(days=4)).strftime('%Y-%m-%d')
    q = "SELECT FIRST 100 VI.CODPRODUTO, SUM(VI.QTDE) AS QTDE FROM VENDA_ITEM VI  JOIN venda V  ON V.CODVENDA = VI.CODVENDA \
         WHERE V.DATA BETWEEN '{0}' AND '{1}'  GROUP BY VI.CODPRODUTO \
         ORDER BY QTDE DESC".format(domingo_anterior, proximo_domingo)
    cursor.execute(q)
    row = cursor.fetchone()
    cursor.close()
    if row:
         return row[0]
    return None