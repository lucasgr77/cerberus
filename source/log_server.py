import logging
import os
from logging.handlers import RotatingFileHandler
import sys
import os

def cerb_logger(fname=None):
    path_log_file = os.getenv('CAMINHO_LOG')
    if(path_log_file is None):
        path_log_file = 'logs'
    if not os.path.isdir(path_log_file):
        os.mkdir(path_log_file)
    log_file = '{0}/{1}.log'.format(path_log_file, fname)
    LOGGER = logging.getLogger(fname)
    if len(LOGGER.handlers) == 0:
        LOGGER.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s [%(threadName)s] - %(message)s')

        file_handler = RotatingFileHandler(log_file, maxBytes=5000000, backupCount=1)
        file_handler.setFormatter(formatter)

        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        LOGGER.addHandler(console_handler)

        LOGGER.addHandler(file_handler)
    return LOGGER 