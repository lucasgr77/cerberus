from ..api import req_opencart
from .modes import CbOperacao
import time
import os

STATUS_CONCLUIDO = 1
STATUS_PENDENTE = 0
STATUS_ERRO = 9


def get_operacoes_pendentes():
    BASE_URL = os.getenv('OC_URL_CB_OPERATIONS')
    url = '{0}?filter=status,eq,0'.format(BASE_URL)
    response = req_opencart(url)
    operacoes = list(map(lambda x:
        CbOperacao(x[0], x[1], x[2], x[3], x[4], x[5]),
        response['cb_operations']['records']))
    return operacoes


def conclui_operacao(operacao):
    BASE_URL = os.getenv('OC_URL_CB_OPERATIONS')
    url = '{0}/{1}'.format(BASE_URL, operacao.id)
    operacao.status = STATUS_CONCLUIDO
    operacao.atualizacao = time.strftime('%Y-%m-%d %H:%M:00', time.localtime())
    req_opencart(url, 'PUT', data=dict(operacao))


def rollback_operacao(operacao):
    altera_operacao(operacao, STATUS_PENDENTE)


def anula_operacao(operacao):
    altera_operacao(operacao, STATUS_ERRO)


def altera_operacao(operacao, status):
    BASE_URL = os.getenv('OC_URL_CB_OPERATIONS')
    url = '{0}/{1}'.format(BASE_URL, operacao.id)
    operacao.status = status
    operacao.atualizacao = time.strftime('%Y-%m-%d %H:%M:00', time.localtime())
    operacao.retry = int(operacao.retry) + 1
    req_opencart(url, 'PUT', data=dict(operacao))
