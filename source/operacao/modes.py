

class CbOperacao:
    def __init__(self, id, inclusao, atualizacao, json, retry, status):
        self.id = id
        self.inclusao = inclusao
        self.atualizacao = atualizacao
        self.json = json
        self.retry = retry
        self.status = status

    def __iter__(self):
        yield 'id', self.id
        yield 'inclusao', self.inclusao
        yield 'atualizacao', self.atualizacao
        yield 'json', self.json
        yield 'retry', self.retry
        yield 'status', self.status
