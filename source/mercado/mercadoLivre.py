#-*- coding:utf-8 -*-
import requests
import json
import os
from dotenv import load_dotenv, find_dotenv


class MercadoException(Exception):
    pass


def preco_mercado_livre(produto):
    todos = []
    offset = 0
    limit = 50
    response = requests.get('https://api.mercadolibre.com/sites/MLB/search?q={0}&offset={1}&limit={2}'.format(produto, offset,limit))
    payload = response.json()
    total = payload['paging']['total']
    print(total)
    chamadas = total/limit
    for chamada in range(chamadas):
        offset = limit * chamada
        consulta_mercado_livre(offset,produto,limit)
    print(total)
    for pesquisa in payload['results']:
        retorno ={
            'Preco': pesquisa['price'],
            'Quantidade_vendida': pesquisa['sold_quantity'],
            'Saldo': pesquisa['available_quantity']
        }
        todos.append(retorno)
        print(retorno)
    print(len(todos))


def consulta_mercado_livre(offset, produto, limit):
    todos = []
    response = requests.get('https://api.mercadolibre.com/sites/MLB/search?q={0}&offset={1}&limit={2}'.format(produto, offset,limit))
    payload = response.json()
    for pesquisa in payload['results']:
        retorno ={
            'Preco': pesquisa['price'],
            'Quantidade_vendida': pesquisa['sold_quantity'],
            'Saldo': pesquisa['available_quantity']
        }
        todos.append(retorno)
    return todos
