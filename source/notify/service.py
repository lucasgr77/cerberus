# encoding: utf-8
import slack
import logging
from ..produtos import models as produto_model
from .models import dto_informativo_produto
import os
from source.ext.deprecated import deprecated
from source.produtos import models as op


@deprecated(reason='Versão substituida utilize notificar_separar_estoques_v2')
def notificar_separar_estoques(codproduto, quantidade_vendida, **kwargs):
    '''
    Método notifica produção no slack para como empacotar os pedidos
    '''
    LOJA_2 = 7
    logger = kwargs['logger'] if 'logger' in kwargs else logging.getLogger(__name__)
    logger.info('Notificando  v1 venda : ' + str(codproduto))
    dados_produto = produto_model.get_produto(codproduto, LOJA_2)

    descricao = '{0} - {1}'.format(dados_produto['descricao'], dados_produto['codigo_fabrica'])
    quantidade = str(quantidade_vendida).replace('.0','')
    localizacao = dados_produto['localizacao']
    loja_separacao = '<@ULNKUCBAB>'
    slack.mensagens_separacao_slack(descricao, quantidade, localizacao, loja_separacao)


def notificar_separar_estoques_v2(numpedven, produtos, envio, **kwargs):
    '''
    Método notifica produção no slack para como empacotar os pedidos
    Envia para canal de cada loja itens a serem separados com o pedido completo
    Versão 2
    '''
    logger = kwargs['logger'] if 'logger' in kwargs\
        else logging.getLogger(__name__)
    LOJA_1 = 1
    LOJA_2 = 2
    for prod in produtos:
        __assegura_objeto_notificacao__(prod)
    lojas_notificar = list(map(lambda x:
                           map(lambda y: y['loja'], x['origens'])[0],
                           produtos))
    if LOJA_1 in lojas_notificar or LOJA_2 in lojas_notificar:
        logger.info('Notificando v2 venda : ' + str(numpedven))
        mensagem = __mensagem_estoque__(numpedven, produtos, envio, **kwargs)
        if LOJA_2 in lojas_notificar:
            url = os.getenv('URL_SLACK_VENDA_LOJA_2')
            slack.enviar_mensagem(mensagem, url)
        if LOJA_1 in lojas_notificar:
            url = os.getenv('URL_SLACK_VENDA_LOJA_1')
            slack.enviar_mensagem(mensagem, url)
    else:
        logger.info('Sem notificação estoque online : ' + str(numpedven))

def __mensagem_estoque__(numero_venda, produtos, envio, **kwargs):
    cabecalho = '''*Venda {0}*'''.format(numero_venda)
    corpo = ''
    for produto in produtos:
        for origem in produto['origens']:
            info_prod = __get_informativo__(origem['loja'], produto)
            corpo = corpo + __corpo_mensagem_venda__(info_prod,
                                                     origem['quantidade'])
    rodape = '''*Envio:* {0}
---*estoque atualizado*---'''.format(envio)
    mensagem = '''{0}
{1}
{2}'''.format(cabecalho, corpo, rodape)
    return mensagem


def __corpo_mensagem_venda__(info_prod, quantidade):
    quantidade = slack.text_quantity(quantidade)
    mensagem = '''\n{5} *Peça:* {0} *Sku:* {1}
*Quantidade:* {2} | *Localização:* {3} | *Estoque:* {4}'''
    return mensagem.format(info_prod.descricao,
                           info_prod.codigo_fabrica,
                           quantidade,
                           info_prod.localizacao,
                           info_prod.usuario,
                           info_prod.get_user_emoji())


def __get_informativo__(origem, produto):
    LOJA_2_AMBIENTSOFT = 7
    ONLINE = 0
    SEM_LOCALIZACAO = ''

    if origem == ONLINE:
        model = op.get_oc_product_description(produto['product_id'])
        return dto_informativo_produto(model['name'],
                                       produto['sku'],
                                       SEM_LOCALIZACAO,
                                       origem)

    if 'codproduto' in produto and produto['codproduto'] is not None:
        if origem == 2:
            origem = LOJA_2_AMBIENTSOFT
        model = produto_model.get_produto(produto['codproduto'], origem)
        return dto_informativo_produto(model['descricao'],
                                       model['codigo_fabrica'],
                                       model['localizacao'],
                                       origem)

def __assegura_objeto_notificacao__(produto):
    if 'origens' not in produto:
        raise Exception('Produto sem chave "origens", SKU: {0}'
                        .format(produto['sku']))
    if 'quantidade' not in produto:
        raise Exception('Produto sem chave "quantidade"')

    if 'codproduto' not in produto and 'product_id' not in produto:
        raise Exception('Produto sem chave "codproduto" e "product_id", SKU: {0}'
                        .format(produto['sku']))
