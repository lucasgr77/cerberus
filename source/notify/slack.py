#-*- coding:utf-8 -*-
import requests
import json
import os
from source.ext.deprecated import deprecated


class SlackException(Exception):
    pass


@deprecated(reason='Versão substituida utilize enviar_mensagem')
def mensagem_slack(mensagem):
    url_base = os.getenv('SLACK_URL_NOTIFICACAO_VENDA')
    url = '{0}'.format(url_base)
    corpo_mensagem =json.dumps({"type": "app_mention", 'text': mensagem, 'link_names': 1})
    response = requests.post(url, data=corpo_mensagem)
    if response.status_code != 200:
        raise SlackException(response.text)
    return True


@deprecated(reason='Versão substituida utilize eviar_mensagem')
def mensagens_separacao_slack(nome_peca, quantidade, localizacao,
                              loja_separacao):
    mensagem_slack(corpo_mensagem_venda(nome_peca,
                                        quantidade,
                                        localizacao,
                                        loja_separacao))


@deprecated(reason='Versão substituida utilize enviar_mensagem')
def corpo_mensagem_venda(nome_peca, quantidade, localizacao, loja_separacao):
    STR_VAZIA = 'String Vazia'
    if nome_peca is None or nome_peca == '':
        raise SlackException(STR_VAZIA)
    if quantidade is None or quantidade == '':
        raise SlackException(STR_VAZIA)
    if localizacao is None or localizacao == '':
        raise SlackException(STR_VAZIA)
    if loja_separacao is None or loja_separacao == '':
        raise SlackException(STR_VAZIA)
    quantidade = text_quantity(quantidade)
    mensagem = '''*Peça:* {0}
*Quantidade:* {1} | *Localização:* {2} | *Estoque:* {3}\n'''
    return mensagem.format(nome_peca, quantidade, localizacao, loja_separacao)


def text_quantity(quantidade):
    quantidade = int(float(quantidade))
    retorno = str(quantidade)
    array_emoji = [
        ' :u5272:',
        ' :u6307:',
        ' :orthodox_cross:',
        ' :leo:',
        ' :koko:',
        ' :zap:',
        ' :fire:',
        ' :bomb:',
        ' :earth_asia:'
    ]
    if quantidade > 1 and quantidade < 10:
        retorno += str(array_emoji[quantidade - 2])
    elif quantidade >= 10:
        retorno += str(array_emoji[8])
    return retorno


def enviar_mensagem(mensagem, url_canal):
    corpo_mensagem = json.dumps({'text': mensagem})
    response = requests.post(url_canal, data=corpo_mensagem)
    if response.status_code != 200:
        raise SlackException(response.text)
