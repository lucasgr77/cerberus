from source.extensions import cached_file
import os


class dto_informativo_produto:

    def __init__(self, descricao, codigo_fabrica, localizacao, loja_separacao):
        self.descricao = descricao
        self.codigo_fabrica = codigo_fabrica
        self.localizacao = localizacao
        self.loja_separacao = loja_separacao
        if loja_separacao == 0:
            self.usuario = self.get_user_loja_onlne()
        if loja_separacao == 1:
            self.usuario = self.get_user_loja_1()
        if loja_separacao in (2, 7):
            self.usuario = self.get_user_loja_2()

    def get_user_emoji(self):
        if self.loja_separacao == 1:
            return ':large_green_circle:'
        elif self.loja_separacao in (2, 7):
            return ':large_blue_circle:'
        return ':large_yellow_circle:'

    @staticmethod
    @cached_file
    def get_user_loja_1():
        return os.getenv('USER_SLACK_LOJA_1')


    @staticmethod
    @cached_file
    def get_user_loja_2():
        return os.getenv('USER_SLACK_LOJA_2')

    @staticmethod
    @cached_file
    def get_user_loja_onlne():
        return os.getenv('USER_SLACK_LOJA_ONLINE')
