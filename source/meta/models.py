from sqlalchemy import Column, Integer, String, Float, DateTime
from ..settings import Base


class ControleJobs(Base):
    __tablename__ = 'controle_jobs'

    id = Column(Integer, primary_key=True)
    data = Column(DateTime)
    nome = Column(String)

    def __init__(self, data, nome):
            self.data = data
            self.nome = nome
 