#-*- coding:utf-8 -*-
import requests
from ..database import LocalStorage, Firebird
from .models import ControleJobs
from source.notify.slack import SlackException
from source.pedidos.service import pedidos_recentes
from sqlalchemy import and_, func
from .meta_cadastro import meta as meta_cadastro
import datetime
import json
import os


class TotalVendasLojas:
    def __init__(self, total, custo, lucro, data):
        self.total = total
        self.custo = custo
        self.lucro = lucro
        self.data = data

    total = 0
    custo = 0
    lucro = 0
    data = ''


def execute():
    meta_vendas_diarias()
    meta_cadastro()


def meta_vendas_diarias():
    total_vendas_lojas_fisica = __total_vendas_lojas_fisica__()
    total_vendas_site = __total_vendas_site__()
    notificar_vendas(total_vendas_lojas_fisica, total_vendas_site)


def __imprimiu_meta_hoje__(job_meta):
    session = LocalStorage.get_session()
    hoje = (datetime.datetime.now()).strftime('%Y-%m-%d')
    execucoes_job = session.query(ControleJobs).filter(
        func.date(ControleJobs.data) >= hoje,
        func.date(ControleJobs.data) <= hoje).first()
    execucoes_job = session.query(ControleJobs).filter(
        ControleJobs.nome == job_meta).first()
    session.close()
    return execucoes_job is not None


def __total_vendas_lojas_fisica__():
    ontem = (datetime.datetime.now() - datetime.timedelta(days=1)
             ).strftime('%Y-%m-%d')
    q = "SELECT SUM(v.VALOR_TOTAL) AS total, \
            sum(v.PRECO_CUSTO)  AS custo, \
            sum(v.VALOR_TOTAL) - sum(v.PRECO_CUSTO) AS lucro \
            FROM VENDA v  \
            WHERE v.LIBERADO = 'S' \
            AND v.CONCLUIDO  = 'S' \
            AND v.DATA = '{0}' \
            ".format(ontem)
    cursor = Firebird.get_connection()
    cursor.execute(q)
    rows = cursor.fetchall()
    retorno = None
    for row in rows:
        total = 0 if row[0] is None else row[0]
        custo = 0 if row[1] is None else row[1]
        lucro = 0 if row[2] is None else row[2]
        retorno = TotalVendasLojas(
            float(total),
            float(custo),
            float(lucro),
            ontem
        )
    cursor.close()
    return retorno


def __total_vendas_site__(**kwargs):
    pedidos_ontem = pedidos_recentes(dias_pesquisa=1, data_final=True)
    total_site = 0
    for pedido in pedidos_ontem:
        total_site += pedido['total_produtos']
    return total_site


def notificar_vendas(vendas_lojas, vendas_site):
    url_base = os.getenv('SLACK_URL_META')
    url = '{0}'.format(url_base)
    hoje = datetime.datetime.now().date()
    ontem = (hoje - datetime.timedelta(days=1)).strftime('%d/%m/%Y')
    mensagem = '''
*Fechamento Diário*
*Data do fechamento*: {0}

*Vendas*

* Lojas Fisicas
*Total*: {1}
*Custo*: {2}
*Lucro*: {3}

* Site
*Total*: {4}
    '''.format(ontem, vendas_lojas.total, vendas_lojas.custo,
               vendas_lojas.lucro, vendas_site)
    corpo_mensagem = json.dumps({"type": "app_mention", 'text': mensagem})
    response = requests.post(url, data=corpo_mensagem)
    if response.status_code != 200:
        raise SlackException(response.text)
