#-*- coding:utf-8 -*-
import datetime
from ..api import req_opencart, convert_api_response_to_object
from source.notify.slack import SlackException
import os
import json
import requests


def meta():
    data = __get_meta_data__()


def __get_meta_data__():
    ontem = (datetime.datetime.now() - datetime.timedelta(days=1)
             ).strftime('%Y-%m-%d')
    url_base = os.getenv('OC_URL_DC_FILA_CAD_RPOD')
    response = req_opencart(url_base)
    dc_fila_cad = response['dc_fila_cad_prod']
    cadastros = convert_api_response_to_object(dc_fila_cad['columns'],
                                               dc_fila_cad['records'])
    finalizados = list(filter(lambda x: x['status'] > 0 and x['status'] < 9, cadastros))
    finalizados_ontem = list(filter(lambda x: x['data_atualizacao'][0:10] == ontem, finalizados))
    return {
        'total': len(cadastros),
        'total_finalizados': len(finalizados),
        'finalizados_ontem': len(finalizados_ontem)
    }
