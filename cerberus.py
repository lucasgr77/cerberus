# -*- coding:utf-8 -*-
import os
import sys
from apscheduler.triggers import interval
from source.jobs import (
    job_envio_estoque_site,
    job_vendas_faturadas,
    job_operacoes,
    job_envio_ab_products,
    job_precificacao,
    job_meta,
    job_catalogo,
    job_liberar_pedido,
    job_ciclo_decepticon,
    job_processa_cmd_vmd,
    job_carga_conferencia,
    job_limpar_base
    )
from source.database import LocalStorage
from source.sistema.service import check_connections
from apscheduler.schedulers.blocking import BlockingScheduler



LocalStorage.connect(False)
check_connections()
tempo_exec_estoque = os.getenv('JOB_ENVIO_ESTOQUE_SITE_EXEC_MINUTOS')
tempo_exec_vendas = os.getenv('JOB_VENDAS_FATURADAS_EXEC_MINUTOS')
tempo_exec_operacoes = os.getenv('JOB_OPERACOES_EXEC_MINUTOS')
tempo_exec_envio_ab_products = os.getenv('JOB_ENVIO_AB_PRODUCTS_EXEC_MINUTOS')
tempo_exec_precificacao = os.getenv('JOB_PRECIFICACAO_EXEC_MINUTOS')
tempo_exec_ciclo_heroku = os.getenv('JOB_CICLO_DECEPTICON_EXEC_MINUTOS')



envio_estoque_site_tempo = int(tempo_exec_estoque) \
    if tempo_exec_estoque is not None else 15
vendas_faturadas_exec_tempo = int(tempo_exec_vendas) \
    if tempo_exec_vendas is not None else 120
tempo_exec_operacoes = int(tempo_exec_operacoes) \
    if tempo_exec_operacoes is not None else 5
tempo_exec_envio_ab_products = int(tempo_exec_envio_ab_products) \
    if tempo_exec_envio_ab_products is not None else 480
tempo_exec_precificacao = int(tempo_exec_precificacao) \
    if tempo_exec_precificacao is not None else 500




scheduler = BlockingScheduler(timezone='utc')
trigger_estoque = interval.IntervalTrigger(minutes=envio_estoque_site_tempo, timezone='utc')
trigger_vendas = interval.IntervalTrigger(minutes=vendas_faturadas_exec_tempo, timezone='utc')
trigger_operacoes = interval.IntervalTrigger(minutes=tempo_exec_operacoes, timezone='utc')
trigger_envio_ab_products = interval.IntervalTrigger(minutes=tempo_exec_envio_ab_products, timezone='utc')
trigger_precificacao = interval.IntervalTrigger(minutes=tempo_exec_precificacao, timezone='utc')
trigger_meta = interval.IntervalTrigger(minutes=60, timezone='utc')
trigger_catalogo = interval.IntervalTrigger(minutes=60, timezone='utc')
meia_hora = interval.IntervalTrigger(minutes=30, timezone='utc')
duas_horas = interval.IntervalTrigger(minutes=120, timezone='utc')
scheduler.add_job(job_envio_estoque_site,
                  name='job_envio_estoque_site',
                  trigger=trigger_estoque)
scheduler.add_job(job_vendas_faturadas,
                  name='job_vendas_faturadas',
                  trigger=trigger_vendas)
scheduler.add_job(job_operacoes,
                  name='job_operacoes',
                  trigger=trigger_operacoes)
scheduler.add_job(job_envio_ab_products,
                  name='job_envio_ab_products',
                  trigger=trigger_envio_ab_products)
scheduler.add_job(job_precificacao,
                  name='job_precificacao',
                  trigger=trigger_precificacao)
scheduler.add_job(job_meta,
                  name='job_meta',
                  trigger=trigger_meta)
scheduler.add_job(job_catalogo,
                  name='job_catalogo',
                  trigger=trigger_catalogo)
scheduler.add_job(job_catalogo,
                  name='job_catalogo',
                  trigger=trigger_catalogo)
scheduler.add_job(job_processa_cmd_vmd,
                  name='job_processa_cmd_vmd',
                  trigger=meia_hora)
scheduler.add_job(job_ciclo_decepticon,
                  name='job_ciclo_decepticon',
                  trigger=meia_hora)
scheduler.add_job(job_liberar_pedido,
                  name='job_liberar_pedido',
                  trigger=meia_hora)
scheduler.add_job(job_carga_conferencia,
                  name='job_carga_conferencia',
                  trigger=duas_horas)
scheduler.add_job(job_limpar_base,
                name='job_limpar_base',
                trigger=duas_horas)
print('Registrado Serviços')



start = sys.argv[1:]
if len(start) == 0:
    print('Sem argumentos, rodando todos os jobs!')
    scheduler.start()
elif start[0] == 'job_vendas_faturadas':
    job_vendas_faturadas()
elif start[0] == 'job_envio_estoque_site':
    job_envio_estoque_site()
elif start[0] == 'job_operacoes':
    job_operacoes()
elif start[0] == 'job_envio_ab_products':
    job_envio_ab_products()
elif start[0] == 'job_precificacao':
    job_precificacao()
elif start[0] == 'job_meta':
    job_meta()
elif start[0] == 'job_catalogo':
    job_catalogo()
elif start[0] == 'job_liberar_pedido':
    job_liberar_pedido()
elif start[0] == 'job_ciclo_decepticon':
    job_ciclo_decepticon()
elif start[0] == 'job_processa_cmd_vmd':
    job_processa_cmd_vmd()
elif start[0] == 'job_carga_conferencia':
    job_carga_conferencia()
elif start[0] == 'job_limpar_base':
    job_limpar_base()
