Para criar o ambiente do cerberus execute:

Instalando python 2.7
```
cd ~
mkdir tmp
cd tmp
wget https://www.python.org/ftp/python/2.7.18/Python-2.7.18.tgz
sudo tar xzf Python-2.7.18.tgz
cd Python-2.7.18
sudo ./configure --enable-optimizations
sudo make altinstall
sudo apt install libsqlite3-dev
```

Feche seu terminal abra um novo na pasta do projeto

Instalando ambiente virtual
```
virtualenv env --python=python2.7
```

Ativando ambiente virtual
```
source env/bin/activate
```

Seu terminal deverá aparecer atrás `(env)` demonstrando que está funcionando

Instalando dependencias:
```
pip install -U setuptools
pip install -r requirements.txt
```
![Screenshot](execute_calcula_preco.png)
![Screenshot](precificacao_diagrama.png)
