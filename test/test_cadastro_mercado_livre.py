import pytest
import os
import requests_mock
import mock

from .base import BaseTest
from sqlalchemy.orm import sessionmaker
from source.database import LocalStorage
from source.catalogo_mercado_livre.models import (
    MlCadastro, MlVariacao, tem_variacao_obrigatoria, salvar
)


class TestMlCadastro(BaseTest):

    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()


    def test_variacao_true(self, session):
        session.add(MlCadastro(
            mlid='MLB-1621471476',
            sku= 'ab10',
        ))
        session.add(MlVariacao(
            mlid='MLB-1621471476',
            id_variacao= 123456,
        ))
        session.commit()
        session.close()

        variacao = tem_variacao_obrigatoria('ab10')

        assert variacao is True


    def test_sem_variacao(self, session):
        session.add(MlCadastro(
            mlid='MLB-1621471476',
            sku= 'ab10',
        ))
        session.commit()
        session.close()

        variacao = tem_variacao_obrigatoria('ab10')

        assert variacao is False


    def test_produto_nao_esta_salvo(self, session, mocker):
        session.add(MlCadastro(
            mlid='MLB-1621471476',
            sku= '',
        ))
        session.commit()
        session.close()
        with pytest.raises(Exception) as ex:
            variacao = tem_variacao_obrigatoria('')
        assert 'Produto invalido'


    def test_salvar_novo_mlid(self, mocker):
        salvar('MLB-123', 'eu01', [12, 34, 56])
        session = LocalStorage.get_session()
        retorno = session.query(MlCadastro).first()
        assert retorno.mlid == 'MLB-123'
        assert retorno.sku == 'eu01'
        assert len(retorno.variacoes) == 3
        for variacao in retorno.variacoes:
            assert variacao.id_variacao in [12, 34, 56]

    def test_tem_variacao_false(self):
        salvar('MLB-123', 'eu01', [12, 34,56])
        tem_variacao = tem_variacao_obrigatoria('eu01')
        assert tem_variacao is True

        salvar('MLB-123', 'eu01', [])
        tem_variacao = tem_variacao_obrigatoria('eu01')
        assert tem_variacao is False


    def teste_atualiza_sku(self):
        salvar('MLB-123', 'eu01', [12, 34, 56])
        session = LocalStorage.get_session()
        retorno = session.query(MlCadastro).first()
        assert retorno.sku == 'eu01'
        session.commit()
        session.close()

        salvar('MLB-123', 'eu02', [12, 34, 56])
        session = LocalStorage.get_session()
        retorno = session.query(MlCadastro).first()
        assert retorno.mlid == 'MLB-123'
        assert retorno.sku == 'eu02'
        assert len(retorno.variacoes) == 3


    def test_atualiza_variacos_para_vazio(self):
        salvar('MLB-123', 'eu01', [12, 34, 56])
        session = LocalStorage.get_session()
        retorno = session.query(MlCadastro).first()
        assert retorno.sku == 'eu01'
        session.commit()
        session.close()

        salvar('MLB-123', 'eu02', [])
        session = LocalStorage.get_session()
        retorno = session.query(MlCadastro).first()
        assert retorno.mlid == 'MLB-123'
        assert retorno.sku == 'eu02'
        assert len(retorno.variacoes) == 0


    def test_mlid_nulo(self, mocker):
        with pytest.raises(Exception) as ex:
            variacao = salvar(None, 'eu01', [12, 34, 56])
        assert 'Sem mlid'