import mock
import requests_mock
import pytest
from ..base import BaseTest
from source.heroku.controller import controle_automacao, req_heroku



class TestHeroku(BaseTest):

    def test_ligar_heroku(self, mocker):
        SEGUNDA = 1
        mocker.patch('source.heroku.controller.dia_atual', mock.MagicMock(return_value=SEGUNDA))
        mocker.patch('source.heroku.controller.hora_atual', mock.MagicMock(return_value=7))
        mocker.patch('source.heroku.controller.__get_quantidade_dynos_ativos__', mock.MagicMock(return_value=False))
        mk_heroku = mocker.patch('source.heroku.controller.req_heroku')
        controle_automacao()
        quantidade_dyno = mk_heroku.call_args[0][0]
        assert quantidade_dyno == 1

    def test_desligar_heroku(self, mocker):
        SEGUNDA = 1
        mocker.patch('source.heroku.controller.dia_atual', mock.MagicMock(return_value=SEGUNDA))
        mocker.patch('source.heroku.controller.hora_atual', mock.MagicMock(return_value=18))
        mocker.patch('source.heroku.controller.__get_quantidade_dynos_ativos__', mock.MagicMock(return_value=True))
        mk_heroku = mocker.patch('source.heroku.controller.req_heroku')
        controle_automacao()
        quantidade_dyno = mk_heroku.call_args[0][0]
        assert quantidade_dyno == 0

    def test_desligar_heroku_sabado(self, mocker):
        SABADO = 5
        mocker.patch('source.heroku.controller.dia_atual', mock.MagicMock(return_value=SABADO))
        mocker.patch('source.heroku.controller.hora_atual', mock.MagicMock(return_value=14))
        mocker.patch('source.heroku.controller.__get_quantidade_dynos_ativos__', mock.MagicMock(return_value=True))
        mk_heroku = mocker.patch('source.heroku.controller.req_heroku')
        controle_automacao()
        quantidade_dyno = mk_heroku.call_args[0][0]
        assert quantidade_dyno == 0

    def test_desligar_heroku_domingo(self, mocker):
        DOMINGO = 6
        mocker.patch('source.heroku.controller.dia_atual', mock.MagicMock(return_value=DOMINGO))
        mocker.patch('source.heroku.controller.hora_atual', mock.MagicMock(return_value=14))
        mocker.patch('source.heroku.controller.__get_quantidade_dynos_ativos__', mock.MagicMock(return_value=True))
        mk_heroku = mocker.patch('source.heroku.controller.req_heroku')
        controle_automacao()
        quantidade_dyno = mk_heroku.call_args[0][0]
        assert quantidade_dyno == 0

    def test_ligar_heroku_domingo(self, mocker):
        DOMINGO = 6
        mocker.patch('source.heroku.controller.dia_atual', mock.MagicMock(return_value=DOMINGO))
        mocker.patch('source.heroku.controller.hora_atual', mock.MagicMock(return_value=7))
        mocker.patch('source.heroku.controller.__get_quantidade_dynos_ativos__', mock.MagicMock(return_value=False))
        mk_heroku = mocker.patch('source.heroku.controller.req_heroku')
        controle_automacao()
        quantidade_dyno = mk_heroku.call_args[0][0]
        assert quantidade_dyno == 1