import pytest
from source.notify.service import notificar_separar_estoques_v2
from pytest_mock import mocker

USER_SLACK_LOJA_ONLINE = 'USER_SLACK_LOJA_ONLINE'
USER_SLACK_LOJA_1 = 'USER_SLACK_LOJA_1'
USER_SLACK_LOJA_2 = 'USER_SLACK_LOJA_2'
URL_CANAL_SLACK_LOJA_1 = 'http://mock-slack.com/loja1'
URL_CANAL_SLACK_LOJA_2 = 'http://mock-slack.com/loja2'

class TestNotificacaoV2:

    def test_produto_invalido(self):
        NUMPEDVEN = 1
        produtos = [{
            'id_produto_tiny': '11',
            'quantidade': '1',
            'sku': 'a',
            'valor_unitario': 10.5,
            'loja': 0,
            'separacao_loja': 'n'
        }]
        envio = ':ml:'
        with pytest.raises(Exception) as ex:
            notificar_separar_estoques_v2(NUMPEDVEN, produtos, envio)
        assert str(ex.value[0]) == 'Produto sem chave "origens", SKU: a'

    def test_produto_ultimo_invalido(self):
        NUMPEDVEN = 1
        produtos = [{
            'id_produto_tiny': '11',
            'quantidade': '1',
            'sku': 'a',
            'valor_unitario': 10.5,
            'loja': 0,
            'separacao_loja': 'n',
            'codproduto': 11,
            'product_id': 22,
            'origens': [
                {
                    'loja': 0,
                    'quantidade': 1
                }
            ]
        },
        {
            'id_produto_tiny': '11',
            'quantidade': '1',
            'sku': 'b',
            'valor_unitario': 10.5,
            'loja': 0,
            'separacao_loja': 'n'
        }]
        envio = ':ml:'
        with pytest.raises(Exception) as ex:
            notificar_separar_estoques_v2(NUMPEDVEN, produtos, envio)
        assert str(ex.value[0]) == 'Produto sem chave "origens", SKU: b'

    def test_produtos_total_loja_2(self, mocker):
        mk_slack = mocker.patch('source.notify.slack.enviar_mensagem')
        DESCRICAO_PECA = 'PARAL TRAS T 150 PTO'
        CODIGO_FABRICA = '1021/012-01'
        LOCALIZACAO = '10/A/2 - PRA'
        mocker.patch('source.produtos.models.get_produto', return_value={
            'descricao': DESCRICAO_PECA,
            'codigo_fabrica': CODIGO_FABRICA,
            'localizacao': LOCALIZACAO
        })
        NUMPEDVEN = 1
        produtos = [{
            'id_produto_tiny': '11',
            'quantidade': 1,
            'sku': 'a',
            'valor_unitario': 10.5,
            'loja': 0,
            'separacao_loja': 'n',
            'codproduto': 11,
            'product_id': 5101,
            'origens': [
                {
                    'loja': 2,
                    'quantidade': 1
                }
            ]
        }]
        envio = ':ml:'
        notificar_separar_estoques_v2(NUMPEDVEN, produtos, envio)
        mensagem, url = mk_slack.call_args[0]
        assert DESCRICAO_PECA in mensagem
        assert CODIGO_FABRICA in mensagem
        assert LOCALIZACAO in mensagem
        assert USER_SLACK_LOJA_2 in mensagem
        assert USER_SLACK_LOJA_ONLINE not in mensagem
        assert url == URL_CANAL_SLACK_LOJA_2

    def test_produtos_parcial_loja_2_e_online(self, mocker):
        mk_slack = mocker.patch('source.notify.slack.enviar_mensagem')
        DESCRICAO_PECA = 'PARAL TRAS T 150 PTO'
        CODIGO_FABRICA = '1021/012-01'
        LOCALIZACAO = '10/A/2 - PRA'
        mocker.patch('source.produtos.models.get_produto', return_value={
            'descricao': DESCRICAO_PECA,
            'codigo_fabrica': CODIGO_FABRICA,
            'localizacao': LOCALIZACAO
        })
        mocker.patch('source.produtos.models.get_oc_product_description',
                     return_value={
                        'name': 'Paralama Traseiro Titan 150 Preto 2008',
        })
        NUMPEDVEN = 1
        produtos = [{
            'id_produto_tiny': '11',
            'quantidade': 7,
            'sku': 'a',
            'valor_unitario': 10.5,
            'loja': 0,
            'separacao_loja': 'n',
            'codproduto': 11,
            'product_id': 5101,
            'origens': [
                {
                    'loja': 2,
                    'quantidade': 2
                },
                {
                    'loja': 0,
                    'quantidade': 5
                }
            ]
        }]
        envio = ':ml:'
        notificar_separar_estoques_v2(NUMPEDVEN, produtos, envio)
        mensagem, url = mk_slack.call_args[0]
        assert DESCRICAO_PECA in mensagem
        assert CODIGO_FABRICA in mensagem
        assert LOCALIZACAO in mensagem
        assert USER_SLACK_LOJA_2 in mensagem
        assert USER_SLACK_LOJA_ONLINE in mensagem

        # quantidades
        assert '*Quantidade:* 5' in mensagem
        assert '*Quantidade:* 2' in mensagem

        assert url == URL_CANAL_SLACK_LOJA_2

    def test_produtos_total_online(self, mocker):
        mk_slack = mocker.patch('source.notify.slack.enviar_mensagem')
        DESCRICAO_PECA = 'Paralama Traseiro Titan 150 Preto 2008'
        mocker.patch('source.produtos.models.get_oc_product_description',
                     return_value={
                        'name': DESCRICAO_PECA,
        })
        NUMPEDVEN = 1
        produtos = [{
            'id_produto_tiny': '11',
            'quantidade': 7,
            'sku': 'a',
            'valor_unitario': 10.5,
            'loja': 0,
            'separacao_loja': 'n',
            'codproduto': 11,
            'product_id': 5101,
            'origens': [
                {
                    'loja': 0,
                    'quantidade': 1
                }
            ]
        }]
        envio = ':ml:'
        notificar_separar_estoques_v2(NUMPEDVEN, produtos, envio)
        assert mk_slack.call_count == 0

