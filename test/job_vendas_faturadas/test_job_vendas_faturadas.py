# -*- coding:utf-8 -*-
import pytest
import mock
import json
import logging
import requests_mock
import datetime

from source.jobs import job_vendas_faturadas
from source.database import LocalStorage
from source.pedidos.models import PedidoTiny, ItemPedidoTiny
from source.exceptions import TiException, OcException
from source.estoque import opencart, tiny
from source.estoque.models import MovimentacaoEstoque
from source.estoque.opencart import __processa_separacao_opcao__

@mock.patch('source.database.Firebird.connect', mock.MagicMock(return_value=1))
@mock.patch('source.database.Firebird.close', mock.MagicMock(return_value=1))
@mock.patch('source.estoque.tiny.confere_atualiza_cadastro', 
            mock.MagicMock(return_value=True))
class TestJobVendasFaturadas:
    PREFIX = 'test/jsons/job_vendas_faturadas/'
    count = 0
    SUBTRAI_SALDO_AMBIENTE = 'source.estoque.models.subtrair_saldo_ambiente'
    GET_SALDO_AMBIENT = 'source.estoque.models.get_saldo_produto'
    NOTIFICAR_ESTOQUE = 'source.notify.service.notificar_separar_estoques'
    PESQUISA_PEDIDOS = 'source.pedidos.service.pesquisa_pedidos'
    PRODUCTS_ASSOC = 'source.estoque.opencart.req_products_association'
    REQ_OC_PRODUCT = 'source.estoque.opencart.req_oc_product'
    REQ_OPENCART = 'source.estoque.opencart.req_opencart'
    GET_OC_ID = 'source.produtos.service.get_oc_id_from_sku'
    __processa_separacao_opcao__ ='source.estoque.opencart.__processa_separacao_opcao__'
    REQ_OC_POV = 'source.estoque.opencart.req_product_option_value'
    @pytest.fixture(autouse=True)
    def setup(self):
        self.__before_test__()
        yield
        self.__after_test__()

    def __before_test__(self):
        LocalStorage.connect(False)

    def __after_test__(self):
        LocalStorage.drop()

    @pytest.fixture
    def log(self):
        return logging.getLogger('mock')

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
            open(PREFIX + 'pesquisa_itens_envio_1.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's']]))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={
            'sku': '101sku', 'product_id': 5000, 'model': 'Titan 150',
            'quantity_fisica': 1, 'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[1, 0]))
    def test_venda_com_estoque_fisica(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)

        job_vendas_faturadas()
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 0

        #assert opencart
        assert mk_opencart.call_count == 1
        url = mk_opencart.call_args[0][0]
        assert '/oc_product_option_value/' not in url
        assert '/oc_product/' in url

        filial, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]
        assert filial == 7
        assert codproduto == 42
        assert qtd_subtrair == 1
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) >= 1
        itens_tiny = session.query(ItemPedidoTiny).all()
        for i in itens_tiny:
            assert i.separacao_loja == '1'
            assert i.loja == 2

        assert len(session.query(MovimentacaoEstoque).all()) >= 1

        #movimentacao estoque ambient soft
        mov_estq = session.query(MovimentacaoEstoque).all()[0]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 1
        assert mov_estq.saldo_novo == 0
        assert mov_estq.sistema == 'ambientsoft'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'F'

        #movimentacao estoque req_opencart
        mov_estq = session.query(MovimentacaoEstoque).all()[1]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 1
        assert mov_estq.saldo_novo == 0
        assert mov_estq.sistema == 'opencart'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'V'

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                open(PREFIX + 'pesquisa_itens_2_pedidos.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1, 5000, 's']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(side_effect=[{
                    'sku': '123', 'product_id': 5000, 'model': 'Titan 150',
                    'quantity_fisica': 3, 'quantity': 2}, {
                    'sku': '123', 'product_id': 5000, 'model': 'Titan 150',
                    'quantity_fisica': 2, 'quantity': 0}]))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[3, 2, 2, 0]))
    def test_duas_vendas_mesmo_item(self, mocker):
        session = LocalStorage.get_session()
        assert len(session.query(PedidoTiny).all()) == 0
        mocker.patch.object(opencart, 'req_opencart')
        mk_notify = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        job_vendas_faturadas()

        filial, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]
        assert filial == 7
        assert codproduto == 1
        assert qtd_subtrair == 2
        assert len(session.query(PedidoTiny).all()) == 2
        assert len(session.query(ItemPedidoTiny).all()) == 2
        assert len(session.query(MovimentacaoEstoque).all()) == 4
        assert mk_notify.call_count == 2

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_2.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1,5000,'s']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={
                    'sku': '101sku',
                    'product_id': 5000,
                    'model': 'Titan 150',
                    'quantity_fisica':1,
                    'quantity': 0}))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[2, 0]))
    def test_venda_dois_itens_um_estoque_fisica(self, mocker):
        session = LocalStorage.get_session()
        assert len(session.query(PedidoTiny).all()) == 0

        mock_get_id_sku = mocker.patch(self.GET_OC_ID)
        mock_get_id_sku.side_effect = [5000, None]
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)

        job_vendas_faturadas()

        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 0
        filial, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]

        assert filial == 7
        assert codproduto == 1
        assert qtd_subtrair == 1
        assert mk_opencart.call_count == 1
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_2.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1,5000,'n']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '123',
        'product_id': 5000, 
        'model': 'Titan 150', 
        'quantity_fisica':3,
        'quantity': 2}))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[2, 0]))
    def test_venda_nao_tem_item_parametrizado(self, mocker):
        session = LocalStorage.get_session()
        assert len(session.query(PedidoTiny).all()) == 0

        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)

        job_vendas_faturadas()

        assert mk_opencart.call_count == 0
        assert mock_notificar.call_count == 0
        assert 0 == len(session.query(PedidoTiny).all())
        session.close()


    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_1.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1,5000,'s']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '123',
        'product_id': 5000, 
        'model': 'Titan 150', 
        'quantity_fisica':1,
        'quantity': 0}))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[2, 0]))
    def test_venda_ja_salva_nao_deve_notificar_duas_vezes(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)

        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 0
        assert mk_opencart.call_count == 1
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1

        job_vendas_faturadas()
        assert mk_opencart.call_count == 1
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1

        job_vendas_faturadas()
        assert mk_opencart.call_count == 1
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1

    @pytest.mark.skip(reason="Teste quebrando rodando todos, mas particular funciona")
    def test_venda_erro_api_bloqueada_deve_tentar_denovo(self, mocker):
        mocker.stopall()
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        data_de_hoje = datetime.datetime.now().date()
        dois_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
        token = '123'
        url_base = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php'
        url_pesquisa = '{0}/?token={1}&formato=json&dataInicial={2}&pagina=1'.format(
            url_base, token, dois_dias_atras)
        url_pesquisa_pg_2 = '{0}/?token={1}&formato=json&dataInicial={2}&pagina=2'.format(
            url_base, token, dois_dias_atras)
        url_obter = 'http://mock-tiny.com.br/api2/pedido.obter.php/?token=123&formato=json&id=709822530'

        with requests_mock.Mocker() as m:
            obter = open(self.PREFIX + 'api_bloqueada.json').read()
            pesquisa = open(self.PREFIX + 'pesquisa_pedidos_tiny.json').read()
            m.get(url_pesquisa, text=pesquisa)
            m.get(url_pesquisa_pg_2, text='A página tentando obter não existe')
            m.get(url_obter, text=obter)

            job_vendas_faturadas()
            assert exception_notifier.call_count == 1
            assert 'API Bloqueada' in str(exception_notifier.call_args[0][0])

    def call_back_api_bloqueada(self, request, context):
        if self.count == 0:
            self.count = 1
            context.status_code = 200
            bloqueado = open(self.PREFIX + 'api_bloqueada.json').read()
            return bloqueado
        acessivel = open(self.PREFIX + 'obter_pedido.json').read()
        return acessivel

    @pytest.mark.skip(reason="Teste quebrando rodando todos, mas particular funciona")
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1, 5000, 's']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '123',
                                     'product_id': 5000, 'model': 'Titan 150',
                                     'quantity_fisica': 1, 'quantity': 0}))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[2, 0]))
    def test_venda_erro_api_bloqueada_uma_vez(self, mocker):
        mocker.stopall()
        exception_notifier = mocker.patch(self.NOTIFICAR_ESTOQUE)
        data_de_hoje = datetime.datetime.now().date()
        dois_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
        dias_31 = (data_de_hoje - datetime.timedelta(days=31)).strftime('%d/%m/%Y')
        token = '123'
        url_base = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php'
        url_pesquisa = '{0}/?token={1}&formato=json&dataInicial={2}&pagina=1'.format(url_base,
                                                                   token,
                                                                   dois_dias_atras)

        url_pesquisa_pg_2 = '{0}/?token={1}&formato=json&dataInicial={2}&pagina=2'.format(url_base,
                                                                   token,
                                                                   dois_dias_atras)
        url_atrasados = '{0}/?token={1}&formato=json&dataInicial={2}&situacao=faturado'.format(
            url_base,
            token,
            dias_31)
        url_obter = 'http://mock-tiny.com.br/api2/pedido.obter.php/?token=123&formato=json&id=709822530'
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        assert len(session.query(PedidoTiny).all()) == 0
        with requests_mock.Mocker() as m:
            obter = open(self.PREFIX + 'api_bloqueada.json').read()
            pesquisa = open(self.PREFIX + 'pesquisa_pedidos_tiny.json').read()
            nenhum = open(self.PREFIX + 'nenhum.pedido.json').read()
            m.get(url_pesquisa, text=pesquisa)
            m.get(url_pesquisa_pg_2, text='A página tentando obter não existe')
            m.get(url_obter, text=self.call_back_api_bloqueada)
            m.get(url_atrasados, text=nenhum)

            job_vendas_faturadas()
            assert exception_notifier.call_count == 0
            payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
            assert payload_oc['quantity_fisica'] == 0
            loja, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]

            assert loja == 7
            assert codproduto == 1
            assert qtd_subtrair == 1
            assert mk_opencart.call_count == 1
            assert mock_notificar.call_count == 1
            assert len(session.query(PedidoTiny).all()) == 1


    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_1.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1,5000,'s']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value=
                                               {'sku': '123','product_id': 5000, 'model': 'Titan 150',
                                                'quantity_fisica':2,'quantity': 5}))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[2, 0]))
    def test_venda_com_estoque_online(self, mocker):
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mocker.patch('source.produtos.models.get_produto', return_value={
            'descricao': 'Produto A',
            'codigo_fabrica': '101sku',
            'localizacao' : '1/A/1/PRA'
        })
        mocker.patch('source.notify.slack.mensagem_slack')

        job_vendas_faturadas()
        session = LocalStorage.get_session()
        assert mk_opencart.call_count == 0
        assert mk_subtrai_ambiente.call_count == 0
        assert len(session.query(PedidoTiny).all()) == 1
        assert len(session.query(ItemPedidoTiny).all()) >= 1



    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_qtd5.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1,5000,'s']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '101sku','product_id': 5000, 'model': 'Titan 150',
    'quantity_fisica':2,'quantity': 0}))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[2, 0]))
    def test_venda_com_estoque_quantidade_5(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente.return_value = True
        assert len(session.query(PedidoTiny).all()) == 0


        job_vendas_faturadas()
        codproduto, interar_item_pedido = mock_notificar.call_args[0]

        #atualiza saldo online
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 0
        assert mk_opencart.call_count == 1

        #atualiza saldo AMBIENTE
        filial, codproduto, saldoproduto = mk_subtrai_ambiente.call_args[0]
        assert filial == 7
        assert codproduto == 1
        assert saldoproduto == 2

        #notificacao
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1
        assert interar_item_pedido == 2

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_1.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[1,5000,'s']]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '101sku','product_id': 5000, 'model': 'Titan 150',
    'quantity_fisica':0,'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[2, 0]))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    def test_venda_com_estoque_quantidade_fisica_0(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_saldo_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()
        assert mk_opencart.call_count == 0
        assert mk_saldo_ambiente.call_count == 0
        assert mock_notificar.call_count == 0
        assert len(session.query(PedidoTiny).all()) == 1



    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_sku_op.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's', 5, 10]]))
    @mock.patch(REQ_OC_POV, mock.MagicMock(return_value={
    "oc_product_option_value": {
        "columns": [
                "product_option_value_id",
                "product_id",
                "option_id",
                "option_value_id",
                "quantity",
                "subtract",
                "price",
                "price_prefix",
                "points",
                "points_prefix",
                "weight",
                "weight_prefix",
                "quantity_fisica"
            ],
        "records": [
            [
                10, #product_option_value_id
                5000, #product_id
                76, #option_id
                363, #option_value_id
                1, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ]]}}))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '101sku','product_id': 5000, 'model': 'Titan 150',
    'quantity_fisica':0,'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[4, 3]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[None, 5000]))
    def test_vendas_separacao_opcao_sucesso(self, mocker):
        product_option_value_id = 10
        codproduto_vendido = 42
        qtd_vendida = 1
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_saldo_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()

        #assert opencart
        assert mk_opencart.call_count == 1
        url = mk_opencart.call_args[0][0]
        assert '/oc_product_option_value/' + str(product_option_value_id) in url # comparando as urls
        assert '/oc_product/' not in url
        data = mk_opencart._mock_call_args_list[0][1]['data']
        assert data['quantity_fisica'] == 0
        assert data['quantity'] == 0

        #assert ambient
        assert mk_saldo_ambiente.call_count == 1
        amb_loja, codproduto, qtd_subtrair = mk_saldo_ambiente.call_args[0]
        assert amb_loja == 7
        assert codproduto == codproduto_vendido
        assert qtd_subtrair == qtd_vendida

        # assert notificação
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1
        codproduto, quantidade = mock_notificar.call_args[0]
        assert quantidade == qtd_vendida
        assert codproduto == codproduto_vendido

        #assert movimentacao estqoue
        assert len(session.query(MovimentacaoEstoque).all()) == 2
        mov_estq = session.query(MovimentacaoEstoque).all()[0]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 4
        assert mov_estq.saldo_novo == 3
        assert mov_estq.sistema == 'ambientsoft'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'F'
        assert mov_estq.opcao_id == 10

        #movimentacao estoque req_opencart
        mov_estq_oc = session.query(MovimentacaoEstoque).all()[1]
        assert mov_estq_oc.fluxo == 'S'
        assert mov_estq_oc.saldo_anterior == 1
        assert mov_estq_oc.saldo_novo == 0
        assert mov_estq_oc.sistema == 'opencart'
        assert mov_estq_oc.job == 'job_vendas_faturadas'
        assert mov_estq_oc.tipo == 'V'
        assert mov_estq_oc.opcao_id == 10


    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_sku_op_2hifen.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's', 5, 10]]))
    @mock.patch(REQ_OC_POV, mock.MagicMock(return_value={
    "oc_product_option_value": {
       "columns": [
            "product_option_value_id",
            "product_option_id",
            "product_id",
            "option_id",
            "option_value_id",
            "quantity",
            "subtract",
            "price",
            "price_prefix",
            "points",
            "points_prefix",
            "weight",
            "weight_prefix",
            "quantity_fisica"
        ],
        "records": [
            [
                10, #product_option_value_id
                99, #product_option_id
                5000, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ]]}}))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '101-sku','product_id': 5000, 'model': 'Titan 150',
    'quantity_fisica':0,'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[4, 3]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[None, 5000]))
    def test_vendas_separacao_opcao_sku(self, mocker):
        product_option_value_id = 10
        codproduto_vendido = 42
        qtd_vendida = 1
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_saldo_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()

        #assert opencart
        assert mk_opencart.call_count == 1
        url = mk_opencart.call_args[0][0]
        assert '/oc_product_option_value/' + str(product_option_value_id) in url
        assert '/oc_product/' not in url
        data = mk_opencart._mock_call_args_list[0][1]['data']
        assert data['quantity_fisica'] == 0

        #assert ambient
        assert mk_saldo_ambiente.call_count == 1
        amb_loja, codproduto, qtd_subtrair = mk_saldo_ambiente.call_args[0]
        assert amb_loja == 7
        assert codproduto == codproduto_vendido
        assert qtd_subtrair == qtd_vendida

        # assert notificação
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1
        codproduto, quantidade = mock_notificar.call_args[0]
        assert codproduto == codproduto_vendido
        assert quantidade == qtd_vendida

        #assert movimentacao estqoue
        assert len(session.query(MovimentacaoEstoque).all()) == 2
        mov_estq = session.query(MovimentacaoEstoque).all()[0]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 4
        assert mov_estq.saldo_novo == 3
        assert mov_estq.sistema == 'ambientsoft'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'F'
        assert mov_estq.opcao_id == 10

        #movimentacao estoque req_opencart
        mov_estq_oc = session.query(MovimentacaoEstoque).all()[1]
        assert mov_estq_oc.fluxo == 'S'
        assert mov_estq_oc.saldo_anterior == 1
        assert mov_estq_oc.saldo_novo == 0
        assert mov_estq_oc.sistema == 'opencart'
        assert mov_estq_oc.job == 'job_vendas_faturadas'
        assert mov_estq_oc.tipo == 'V'
        assert mov_estq_oc.opcao_id == 10



    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_sku_op.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's', 5, None]]))
    @mock.patch(REQ_OC_POV, mock.MagicMock(return_value={
    "oc_product_option_value": {
       "columns": [
            "product_option_value_id",
            "product_option_id",
            "product_id",
            "option_id",
            "option_value_id",
            "quantity",
            "subtract",
            "price",
            "price_prefix",
            "points",
            "points_prefix",
            "weight",
            "weight_prefix",
            "quantity_fisica"
        ],
        "records": [
            [
                None, #product_option_value_id
                99, #product_option_id
                5000, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ]
        ]}})
    )
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '101sku','product_id': 5000, 'model': 'Titan 150',
    'quantity_fisica':0,'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[4, 3]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[None, 5000]))
    def test_vendas_separacao_opcao_nao_parametrizado(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_saldo_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()

        # assert opencart
        assert mk_opencart.call_count == 0

        # assert ambient
        assert mk_saldo_ambiente.call_count == 0

        # assert notify
        assert mock_notificar.call_count == 0



    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_2_com_opcao.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's', 5, 10],[43, 5001, 's', 5, None]]))
    @mock.patch(REQ_OC_POV, mock.MagicMock(return_value={
    "oc_product_option_value": {
       "columns": [
            "product_option_value_id",
            "product_option_id",
            "product_id",
            "option_id",
            "option_value_id",
            "quantity",
            "subtract",
            "price",
            "price_prefix",
            "points",
            "points_prefix",
            "weight",
            "weight_prefix",
            "quantity_fisica"
        ],
         "records": [
            [
                10, #product_option_value_id
                99, #product_option_id
                5000, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ],
            [
                None, #product_option_value_id
                99, #product_option_value_id
                5001, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ]]}}))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(side_effect=[{'sku': '101sku','product_id': 5000, 'model': 'Titan 150',
    'quantity_fisica':0,'quantity': 0},{'sku': '102sku','product_id': 5001, 'model': 'Fan 150',
    'quantity_fisica':1,'quantity': 0}]))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[5, 4, 4,3]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[None, 5000, 5001]))
    def test_vendas_separacao_opcao_2_itens(self, mocker):
        product_option_value_id = 10
        codproduto_vendido = 42
        codproduto_vendido_2 = 43
        product_id = 5001
        qtd_vendida = 1
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_saldo_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()

        #assert opencart
        assert mk_opencart.call_count == 2
        url = mk_opencart._mock_call_args_list[0][0][0]
        assert '/oc_product_option_value/' + str(product_option_value_id) in url
        assert '/oc_product/' not in url
        data = mk_opencart._mock_call_args_list[0][1]['data']
        assert data['quantity_fisica'] == 0

        #assert opencart sem opcao
        url = mk_opencart._mock_call_args_list[1][0][0]
        assert '/oc_product/' + str(product_id) in url
        assert '/oc_product_option_value/'  not in url
        data = mk_opencart._mock_call_args_list[1][1]['data']
        assert data['quantity_fisica'] == 0

        #assert ambient
        assert mk_saldo_ambiente.call_count == 2
        amb_loja, codproduto, qtd_subtrair = mk_saldo_ambiente._mock_call_args_list[0][0]
        assert amb_loja == 7
        assert codproduto == codproduto_vendido
        assert qtd_subtrair == qtd_vendida


        #assert ambient produdo sem opcao
        amb_loja, codproduto, qtd_subtrair = mk_saldo_ambiente._mock_call_args_list[1][0]
        assert amb_loja == 7
        assert codproduto == codproduto_vendido_2
        assert qtd_subtrair == qtd_vendida

        #assert notify
        assert mock_notificar.call_count == 2
        assert len(session.query(PedidoTiny).all()) == 1
        codproduto, quantidade = mock_notificar._mock_call_args_list[0][0]
        assert codproduto == 42
        assert quantidade == 1
        codproduto, quantidade = mock_notificar._mock_call_args_list[1][0]
        assert codproduto == 43
        assert quantidade == 1

        #assert movimentacao estqoue
        assert len(session.query(MovimentacaoEstoque).all()) == 4


    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_2_itens_opcao.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's', 5, 10],[43, 5001, 's', 5, None]]))
    @mock.patch(REQ_OC_POV, mock.MagicMock(return_value={
    "oc_product_option_value": { 
        "columns": [
            "product_option_value_id",
            "product_option_id",
            "product_id",
            "option_id",
            "option_value_id",
            "quantity",
            "subtract",
            "price",
            "price_prefix",
            "points",
            "points_prefix",
            "weight",
            "weight_prefix",
            "quantity_fisica"
        ],
        "records": [
            [
                10, #product_option_value_id
                99, #product_option_id
                5000, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ],
            [
                None, #product_option_value_id
                99, #product_option_value_id
                5001, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ]
        ]
    }}))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(side_effect=[
        {'sku': '101sku','product_id': 5000, 'model': 'Titan 150', 'quantity_fisica':0,'quantity': 0},
        {'sku': '102sku','product_id': 5001, 'model': 'Fan 150', 'quantity_fisica':1,'quantity': 0}]))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[5, 4, 4,3]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[None, 5000, 5001]))
    def test_vendas_separacao_opcao_2_pedidos(self, mocker):
        product_option_value_id = 10
        product_id = 5001
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_saldo_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()

        # assert opencart
        assert mk_opencart.call_count == 2
        url = mk_opencart._mock_call_args_list[0][0][0]
        assert '/oc_product_option_value/' + str(product_option_value_id) in url
        assert '/oc_product/' not in url
        data = mk_opencart._mock_call_args_list[0][1]['data']
        assert data['quantity_fisica'] == 0

        # assert opencart sem opcao
        url = mk_opencart._mock_call_args_list[1][0][0]
        assert '/oc_product/' + str(product_id) in url
        assert '/oc_poroduct_option_value/'  not in url
        data = mk_opencart._mock_call_args_list[1][1]['data']
        assert data['quantity_fisica'] == 0

        # assert ambient
        assert mk_saldo_ambiente.call_count == 2
        codproduto, quantidade = mock_notificar._mock_call_args_list[0][0]
        assert codproduto == 42
        assert quantidade == 1


        # assert ambient produdo sem opcao
        codproduto, quantidade = mock_notificar._mock_call_args_list[1][0]
        assert codproduto == 43
        assert quantidade == 1

        # assert movimentacao estqoue
        assert len(session.query(MovimentacaoEstoque).all()) == 4



    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_2_com_opcao.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's', 5, 10],[42, 5001, 's', 5, None]]))
    @mock.patch(REQ_OC_POV, mock.MagicMock(return_value={
    "oc_product_option_value": {
       "columns": [
            "product_option_value_id",
            "product_option_id",
            "product_id",
            "option_id",
            "option_value_id",
            "quantity",
            "subtract",
            "price",
            "price_prefix",
            "points",
            "points_prefix",
            "weight",
            "weight_prefix",
            "quantity_fisica"
        ],
         "records": [
            [
                10, #product_option_value_id
                99, #product_option_id
                5000, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ],
            [
                None, #product_option_value_id
                99, #product_
                5001, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ]
        ]
    }}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[5, 4, 4,3]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[None, 5000, 5000]))
    def test_vendas_separacao_exception_2_produtos(self, mocker):
        item_vendido = {
            'sku': '101sku',
            'quantidade': 1
        }
        oc_product = {'sku': '101sku','product_id': 5000, 'model': 'Titan 150',
                    'quantity_fisica':0,'quantity': 0}
        params = [[42, 5000,  's',  5,  10], 
        [43, 5000,  's',  5,  10]]
        session = LocalStorage.get_session()
        mocker.patch.object(opencart, 'req_opencart')
        mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        with pytest.raises(OcException) as ex:
            __processa_separacao_opcao__(item_vendido, oc_product, params)
        assert 'encontrada para o product_id: 5000' in ex.value[0]




    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
            open(PREFIX + 'pesquisa_dois_produtos.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's']]))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={
            'sku': '101sku', 'product_id': 5000, 'model': 'Titan 150',
            'quantity_fisica': 3, 'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[3, 0]))
    def test_venda_com_estoque_dois_produtos(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 0

        #assert opencart
        assert mk_opencart.call_count == 1
        url = mk_opencart.call_args[0][0]
        assert '/oc_product_option_value/' not in url
        assert '/oc_product/' in url

        filial, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]
        assert filial == 7
        assert codproduto == 42
        assert qtd_subtrair == 3
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1
        itens_tiny = session.query(ItemPedidoTiny).all()
        for i in itens_tiny:
           assert i.separacao_loja == '1'
           assert i.loja == 2

        assert len(session.query(MovimentacaoEstoque).all()) == 2

        #movimentacao estoque ambient soft
        mov_estq = session.query(MovimentacaoEstoque).all()[0]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 3
        assert mov_estq.saldo_novo == 0
        assert mov_estq.sistema == 'ambientsoft'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'F'

        #movimentacao estoque req_opencart
        mov_estq = session.query(MovimentacaoEstoque).all()[1]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 3
        assert mov_estq.saldo_novo == 0
        assert mov_estq.sistema == 'opencart'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'V'




    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
            open(PREFIX + 'pesquisa_venda_4_produtos.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's']]))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={
            'sku': '101sku', 'product_id': 5000, 'model': 'Titan 150',
            'quantity_fisica': 3, 'quantity': 1}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[5, 3]))
    def test_venda_quatro_produtos(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        assert len(session.query(PedidoTiny).all()) == 0
        job_vendas_faturadas()
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 1

        #assert opencart
        assert mk_opencart.call_count == 1
        url = mk_opencart.call_args[0][0]
        assert '/oc_product_option_value/' not in url
        assert '/oc_product/' in url

        filial, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]
        assert filial == 7
        assert codproduto == 42
        assert qtd_subtrair == 2
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1
        itens_tiny = session.query(ItemPedidoTiny).all()
        for i in itens_tiny:
           assert i.separacao_loja == '1'
           assert i.loja == 2

        assert len(session.query(MovimentacaoEstoque).all()) == 2

        #movimentacao estoque ambient soft
        mov_estq = session.query(MovimentacaoEstoque).all()[0]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 5
        assert mov_estq.saldo_novo == 3
        assert mov_estq.sistema == 'ambientsoft'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'F'

        #movimentacao estoque req_opencart
        mov_estq = session.query(MovimentacaoEstoque).all()[1]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 3
        assert mov_estq.saldo_novo == 1
        assert mov_estq.sistema == 'opencart'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'V'


    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
            open(PREFIX + 'pesquisa_venda_3_produtos.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's']]))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={
            'sku': '101sku', 'product_id': 5000, 'model': 'Titan 150',
            'quantity_fisica': 4, 'quantity': 1}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[5, 2]))
    def test_venda_qtd_tres_produtos(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        assert len(session.query(PedidoTiny).all()) == 0
        job_vendas_faturadas()
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 1

        #assert opencart
        assert mk_opencart.call_count == 1
        url = mk_opencart.call_args[0][0]
        assert '/oc_product_option_value/' not in url
        assert '/oc_product/' in url

        filial, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]
        assert filial == 7
        assert codproduto == 42
        assert qtd_subtrair == 3
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) == 1
        itens_tiny = session.query(ItemPedidoTiny).all()
        for i in itens_tiny:
           assert i.separacao_loja == '1'
           assert i.loja == 2

        assert len(session.query(MovimentacaoEstoque).all()) == 2

        #movimentacao estoque ambient soft
        mov_estq = session.query(MovimentacaoEstoque).all()[0]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 5
        assert mov_estq.saldo_novo == 2
        assert mov_estq.sistema == 'ambientsoft'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'F'

        #movimentacao estoque req_opencart
        mov_estq = session.query(MovimentacaoEstoque).all()[1]
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 4
        assert mov_estq.saldo_novo == 1
        assert mov_estq.sistema == 'opencart'
        assert mov_estq.job == 'job_vendas_faturadas'
        assert mov_estq.tipo == 'V'

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
            open(PREFIX + 'pesquisa_venda_3_skus.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[
        [42, 5000, 's'],
        [43, 5001, 's'],
        [44, 5002, 's']
                                                             ]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[5000,5001,5002]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(side_effect=[
        {
            'sku': '101sku',
            'product_id': 5000,
            'model': 'Titan 150',
            'quantity': 1,
            'quantity_fisica': 0
        },
        {
            'sku': '102sku',
            'product_id': 5001,
            'model': 'Titan 150',
            'quantity': 0,
            'quantity_fisica': 2
        },
        {
            'sku': '103heasku',
            'product_id': 5002,
            'model': 'Titan 150',
            'quantity': 0,
            'quantity_fisica': 1
        }
    ]))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[5, 2, 3]))
    def test_mensagem_pedido_agrupado(self, mocker, monkeypatch):
        monkeypatch.setenv('VERSAO_NOTIFICAR_VENDAS', '2')
        LOJA_2 = 0
        LOJA_ONLINE = 1
        session = LocalStorage.get_session()
        mocker.patch.object(opencart, 'req_opencart')
        mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mocker.patch('source.estoque.opencart.processar_saldo_venda')
        mk_notifica_separacao = mocker.patch('source.notify.service.notificar_separar_estoques_v2')
        mocker.patch('source.produtos.models.get_produto', return_value={
            'descricao': 'Teste A',
            'codigo_fabrica': '123',
            'localizacao' : 'a/b/1'
        })
        job_vendas_faturadas()
        assert mk_notifica_separacao.call_count == 1
        numero_venda, itens, envio = mk_notifica_separacao.call_args[0]
        assert numero_venda == 1
        assert len(itens) == 3
        for item in itens:
            if item['product_id'] == 5000:
                assert item['origens'][0]['loja'] == 0
                assert item['origens'][0]['quantidade'] == 3
            elif item['product_id'] == 5001:
                assert item['origens'][LOJA_2]['loja'] == 2
                assert item['origens'][LOJA_2]['quantidade'] == 2
                assert item['origens'][LOJA_ONLINE]['loja'] == 0
                assert item['origens'][LOJA_ONLINE]['quantidade'] == 1
            elif item['product_id'] == 5002:
                assert item['origens'][LOJA_2]['loja'] == 2
                assert item['origens'][LOJA_2]['quantidade'] == 1
                assert item['origens'][LOJA_ONLINE]['loja'] == 0
                assert item['origens'][LOJA_ONLINE]['quantidade'] == 2

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
            open(PREFIX + 'pesquisa_venda_3_skus.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[
        [42, 5000, 's'],
        [42, 5001, 's']
                                                             ]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[5000,5001,5002]))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(side_effect=[
                                               {
                                                   'sku': '101sku',
                                                   'product_id': 5000,
                                                   'model': 'Titan 150',
                                                   'quantity_fisica': 4, 'quantity': 1
                                                },
                                               {
                                                   'sku': '102sku',
                                                   'product_id': 5001,
                                                   'model': 'Titan 150',
                                                   'quantity_fisica': 4, 'quantity': 1
                                                },
                                               {
                                                   'sku': '103heasku',
                                                   'product_id': 5002,
                                                   'model': 'Titan 150',
                                                   'quantity_fisica': 4, 'quantity': 1
                                                }
                                            ]))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[5, 2]))
    def test_mensagem_pedido_agrupado_estoque_online(self, mocker, monkeypatch):
        monkeypatch.setenv('VERSAO_NOTIFICAR_VENDAS', '2')
        session = LocalStorage.get_session()
        mocker.patch.object(opencart, 'req_opencart')
        mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mocker.patch('source.estoque.opencart.processar_saldo_venda')
        mk_notifica_separacao = mocker.patch('source.notify.service.notificar_separar_estoques_v2')
        mocker.patch('source.produtos.models.get_produto', return_value={
            'descricao': 'Teste A',
            'codigo_fabrica': '123',
            'localizacao' : 'a/b/1'
        })
        assert len(session.query(PedidoTiny).all()) == 0
        job_vendas_faturadas()
        assert mk_notifica_separacao.call_count == 1
        numero_venda, itens, envio = mk_notifica_separacao.call_args[0]
        assert numero_venda == 1
        assert len(itens) == 3
        skus = list(map(lambda x : x['sku'], itens))
        assert len(skus) == 3
        assert '101sku' in skus
        assert '102sku' in skus
        assert '103sku' in skus
        assert envio == ':ml:'

    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
                    open(PREFIX + 'pesquisa_itens_envio_sku_op.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's', 5, 200]]))
    @mock.patch(REQ_OC_POV, mock.MagicMock(return_value={
    "oc_product_option_value": {
       "columns": [
            "product_option_value_id",
            "product_option_id",
            "product_id",
            "option_id",
            "option_value_id",
            "quantity",
            "subtract",
            "price",
            "price_prefix",
            "points",
            "points_prefix",
            "weight",
            "weight_prefix",
            "quantity_fisica"
        ],
        "records": [
            [
                404, #product_option_value_id
                99, #product_option_id
                5000, #product_id
                76, #option_id
                363, #option_value_id
                0, #quantity
                1, #subtract
                "0.0000", #price
                "+", #price_prefix
                0, #potins
                "+", #points_prefix
                "0.00000000", #weight
                "+", #weight_prefix
                1 #quantity_fisica
            ]
        ]}})
    )
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={'sku': '101sku','product_id': 5000, 'model': 'Titan 150',
    'quantity_fisica':0,'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[4, 3]))
    @mock.patch(GET_OC_ID, mock.MagicMock(side_effect=[None, 5000]))
    def test_opcao_especifica_nao_encontrada(self, mocker):
        session = LocalStorage.get_session()
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mk_saldo_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        assert len(session.query(PedidoTiny).all()) == 0

        job_vendas_faturadas()

        # assert opencart
        assert mk_opencart.call_count == 0

        # assert ambient
        assert mk_saldo_ambiente.call_count == 0

        # exception
        assert exception_notifier.call_count == 1
        message = str(exception_notifier.call_args_list[0][0][0])
        assert message == 'Parametrização POVI não encontrada para o product_id: 5000, povi: 10'

        # assert notify
        assert mock_notificar.call_count == 0
        assert len(session.query(PedidoTiny).all()) == 0


    @mock.patch(PESQUISA_PEDIDOS, mock.MagicMock(return_value=json.loads(
            open(PREFIX + 'pesquisa_itens_envio_1.json').read())))
    @mock.patch(PRODUCTS_ASSOC, mock.MagicMock(return_value=[[42, 5000, 's']]))
    @mock.patch(GET_OC_ID, mock.MagicMock(return_value=5000))
    @mock.patch(REQ_OC_PRODUCT, mock.MagicMock(return_value={
            'sku': '101sku', 'product_id': 5000, 'model': 'Titan 150',
            'quantity_fisica': 10, 'quantity': 0}))
    @mock.patch(GET_SALDO_AMBIENT, mock.MagicMock(side_effect=[1, 0]))
    def test_venda_estoque_fisico_defazado(self, mocker):
        session = LocalStorage.get_session()
        mk_opencart = mocker.patch.object(opencart, 'req_opencart')
        mock_notificar = mocker.patch(self.NOTIFICAR_ESTOQUE)
        mk_subtrai_ambiente = mocker.patch(self.SUBTRAI_SALDO_AMBIENTE)

        job_vendas_faturadas()
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        assert payload_oc['quantity_fisica'] == 9

        #assert opencart
        assert mk_opencart.call_count == 1
        url = mk_opencart.call_args[0][0]
        assert '/oc_product_option_value/' not in url
        assert '/oc_product/' in url

        filial, codproduto, qtd_subtrair = mk_subtrai_ambiente.call_args[0]
        assert filial == 7
        assert codproduto == 42
        assert qtd_subtrair == 1
        assert mock_notificar.call_count == 1
        assert len(session.query(PedidoTiny).all()) >= 1
        itens_tiny = session.query(ItemPedidoTiny).all()
        for i in itens_tiny:
           assert i.separacao_loja == '1'
           assert i.loja == 2
