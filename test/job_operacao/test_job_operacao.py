# encoding: utf-8
import mock
import requests_mock
import pytest
import json
from pytest_mock import mocker
from source.jobs import job_operacoes
from ..base import BaseTest
from source.estoque.models import MovimentacaoEstoque
from source.database import LocalStorage

PFX_TINY = 'http://mock-tiny.com.br/api2/'
PFX_OC = 'http://mock-onlinemotopecas.com.br/api.php/'
PFX_JSON = 'test/job_operacao/jsons/'
GET_SALDO = 'source.estoque.models.get_saldo_produto'
BALANCO_AMBIENT = 'source.estoque.models.balanco_saldo_ambient'
MODEL_ATUALIZA_EAN = 'source.produtos.models.atualizar_ean'


class TestJobOperacao(BaseTest):

    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[1, 0]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_operacao_zerando_estoque_sucesso(self, mocker, session):
        with requests_mock.Mocker() as m:
            #get operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_tripla.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/1', text='1')

            job_operacoes()
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

            #movimentacao estoque ambient soft
            mov_estq = session.query(MovimentacaoEstoque).all()[0]
            assert mov_estq.fluxo == 'S'
            assert mov_estq.loja == 2
            assert mov_estq.saldo_anterior == 1
            assert mov_estq.saldo_novo == 0
            assert mov_estq.sistema == 'ambientsoft'
            assert mov_estq.job == 'job_operacoes'
            assert mov_estq.tipo == 'F'
            assert mov_estq.product_id == 155
            assert mov_estq.opcao_id is None

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[0, 3]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_operacao_entrada_sucesso(self, mocker, session):
        with requests_mock.Mocker() as m:
            #get operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_simples.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/2', text='1')

            job_operacoes()
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

            #movimentacao estoque ambient soft
            mov_estq = session.query(MovimentacaoEstoque).all()[0]
            assert mov_estq.fluxo == 'E'
            assert mov_estq.loja == 2
            assert mov_estq.saldo_anterior == 0
            assert mov_estq.saldo_novo == 3
            assert mov_estq.sistema == 'ambientsoft'
            assert mov_estq.job == 'job_operacoes'
            assert mov_estq.tipo == 'F'
            assert mov_estq.product_id == 5001
            assert mov_estq.opcao_id is None

    def test_operacao_falha_atualizacao_prod(self, mocker):
        with requests_mock.Mocker() as m:
            #get operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_tripla.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/1', text='1')

            mk_atualiza = mocker.patch('source.estoque.ambientsoft.balanco_estoque_ambient')
            mk_atualiza.side_effect = Exception('Erro esperado')
            job_operacoes()
            assert mk_atualiza.call_count == 1
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 0' in operacao_enviada
            assert '"retry": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

    def test_operacao_json_invalido(self, mocker):
        with requests_mock.Mocker() as m:
            #get operacoes invalido
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_invalido.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/2', text='1')

            mk_atualiza = mocker.patch('source.estoque.ambientsoft.balanco_estoque_ambient')
            job_operacoes()
            assert mk_atualiza.call_count == 0
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 9' in operacao_enviada
            assert '"retry": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

    def test_operacao_falha_maximo_retry(self, mocker):
        with requests_mock.Mocker() as m:
            #get operacoes invalido
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_retry_3.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/2', text='1')

            mk_atualiza = mocker.patch('source.estoque.ambientsoft.balanco_estoque_ambient')
            job_operacoes()
            assert mk_atualiza.call_count == 0
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 9' in operacao_enviada
            assert '"retry": 4' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[1, 5]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_operacao_entrada_estoque_opcao(self, mocker, session):
        with requests_mock.Mocker() as m:
            #get operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_opcao_loja_1.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/3', text='1')

            job_operacoes()
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

            #movimentacao estoque ambient soft
            mov_estq = session.query(MovimentacaoEstoque).all()[0]
            assert mov_estq.fluxo == 'E'
            assert mov_estq.loja == 1
            assert mov_estq.saldo_anterior == 1
            assert mov_estq.saldo_novo == 5
            assert mov_estq.sistema == 'ambientsoft'
            assert mov_estq.job == 'job_operacoes'
            assert mov_estq.tipo == 'F'
            assert mov_estq.product_id == 212
            assert mov_estq.opcao_id == 101

    def test_operacao_vincular_ean_sucesso(self, mocker, session):
        with requests_mock.Mocker() as m:
            mk_atualiza_ean = mocker.patch(MODEL_ATUALIZA_EAN, mock.MagicMock(return_value=True))
            #get operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_vincular_ean.json').read())
            #put
            m.put(PFX_OC + 'cb_operations/5', text='1')

            job_operacoes()
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada
            assert mk_atualiza_ean.call_count == 1
            assert len(session.query(MovimentacaoEstoque).all()) == 0

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[0, 5]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_vincula_ean_e_ajusta_estoque(self, mocker, session):
        with requests_mock.Mocker() as m:
            mk_atualiza_ean = mocker.patch(MODEL_ATUALIZA_EAN, mock.MagicMock(return_value=True))
            #get operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacoes_ean_saldo.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/6', text='1')

            job_operacoes()
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

            #op ean
            assert mk_atualiza_ean.call_count == 1

            #movimentacao estoque ambient soft
            mov_estq = session.query(MovimentacaoEstoque).all()[0]
            assert mov_estq.fluxo == 'E'
            assert mov_estq.loja == 2
            assert mov_estq.saldo_anterior == 0
            assert mov_estq.saldo_novo == 5
            assert mov_estq.sistema == 'ambientsoft'
            assert mov_estq.job == 'job_operacoes'
            assert mov_estq.tipo == 'F'
            assert mov_estq.product_id == 5001
            assert mov_estq.opcao_id is None

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[5, 5]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_saldo_identico_nao_movimenta(self, mocker, session):
        with requests_mock.Mocker() as m:
            mk_atualiza_ean = mocker.patch(MODEL_ATUALIZA_EAN, mock.MagicMock(return_value=True))
            #get operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_mantem_saldo.json').read())

            #put
            m.put(PFX_OC + 'cb_operations/6', text='1')

            job_operacoes()
            assert m.call_count == 2
            operacao_enviada = m.request_history[1]._request.body
            assert '"status": 1' in operacao_enviada
            assert '"atualizacao": "0000-00-00+00"' not in operacao_enviada

            #movimentacao estoque ambient soft
            assert len(session.query(MovimentacaoEstoque).all()) == 0

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[5, 50]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_saldo_conferencia_alto_entra_conferencia(self, mocker, session):
        with requests_mock.Mocker() as m:
            #GET operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_saldo_estrapola.json').read())

            #PUT
            m.put(PFX_OC + 'cb_operations/6', text='1')

            #GET sn_conferencia
            m.get(PFX_OC + 'sn_conference?filter=id_conferencia,eq,99&&filter=codproduto,eq,1',
                  text=open(PFX_JSON + 'sn_conferencia_comum.json').read())

            #PUT sn_conferencia
            m.put(PFX_OC + 'sn_conference/1,99', text='1')
            job_operacoes()

            assert m.call_count == 4
            operacao_enviada = json.loads(m.request_history[3]._request.body)
            assert operacao_enviada['status'] == 1
            assert operacao_enviada['atualizacao'] != '"0000-00-00+00"'
            req_put_conferencia = json.loads(m.request_history[2]._request.body)
            assert req_put_conferencia['done'] == 'R'

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[40, 4]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_saldo_baixo_entra_conferencia(self, mocker, session):
        with requests_mock.Mocker() as m:
            #GET operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_saldo_reduzido.json').read())

            #PUT
            m.put(PFX_OC + 'cb_operations/6', text='1')

            #GET sn_conferencia
            m.get(PFX_OC + 'sn_conference?filter=id_conferencia,eq,99&&filter=codproduto,eq,1',
                  text=open(PFX_JSON + 'sn_conferencia_comum.json').read())

            #PUT sn_conferencia
            m.put(PFX_OC + 'sn_conference/1,99', text='1')
            job_operacoes()

            assert m.call_count == 4
            operacao_enviada = json.loads(m.request_history[3]._request.body)
            assert operacao_enviada['status'] == 1
            assert operacao_enviada['atualizacao'] != '"0000-00-00+00"'
            req_put_conferencia = json.loads(m.request_history[2]._request.body)
            assert req_put_conferencia['done'] == 'R'


    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[90, 4]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_saldo_alto_nao_entra_peneira(self, mocker, session):
        #testa um item estoque antes 90 e novo 89 não entra peneira
        with requests_mock.Mocker() as m:
            #GET operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_saldo_alto.json').read())

            #PUT
            m.put(PFX_OC + 'cb_operations/6', text='1')

            #GET sn_conferencia
            m.get(PFX_OC + 'sn_conference?filter=id_conferencia,eq,99&&filter=codproduto,eq,1',
                  text=open(PFX_JSON + 'sn_conferencia_comum.json').read())

            #PUT sn_conferencia
            m.put(PFX_OC + 'sn_conference/1,99', text='1')
            job_operacoes()

            assert m.call_count == 2
            operacao_enviada = json.loads(m.request_history[1]._request.body)
            assert operacao_enviada['status'] == 1
            assert operacao_enviada['atualizacao'] != '"0000-00-00+00"'

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[5, 3]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_saldo_baixo_nao_entra_peneira(self, mocker, session):
        #testa um item estoque antes 90 e novo 89 não entra peneira
        with requests_mock.Mocker() as m:
            #GET operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_simples.json').read())

            #PUT
            m.put(PFX_OC + 'cb_operations/2', text='1')

            #GET sn_conferencia
            m.get(PFX_OC + 'sn_conference?filter=id_conferencia,eq,99&&filter=codproduto,eq,1',
                  text=open(PFX_JSON + 'sn_conferencia_comum.json').read())

            #PUT sn_conferencia
            m.put(PFX_OC + 'sn_conference/1,99', text='1')
            job_operacoes()

            assert m.call_count == 2
            operacao_enviada = json.loads(m.request_history[1]._request.body)
            assert operacao_enviada['status'] == 1

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[1, 0]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_saldo_zerado(self, mocker, session):
        with requests_mock.Mocker() as m:
            #GET operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_saldo_zerado.json').read())

            #PUT
            m.put(PFX_OC + 'cb_operations/6', text='1')

            #GET sn_conferencia
            m.get(PFX_OC + 'sn_conference?filter=id_conferencia,eq,99&&filter=codproduto,eq,1',
                  text=open(PFX_JSON + 'sn_conferencia_comum.json').read())

            #PUT sn_conferencia
            m.put(PFX_OC + 'sn_conference/1,99', text='1')
            job_operacoes()

            assert m.call_count == 4
            operacao_enviada = json.loads(m.request_history[3]._request.body)
            assert operacao_enviada['status'] == 1
            assert operacao_enviada['atualizacao'] != '"0000-00-00+00"'
            req_put_conferencia = json.loads(m.request_history[2]._request.body)
            assert req_put_conferencia['done'] == 'R'

    @mock.patch(GET_SALDO, mock.MagicMock(side_effect=[1, 0]))
    @mock.patch(BALANCO_AMBIENT, mock.MagicMock(return_value=True))
    def test_saldo_revisado(Self, mocker, session):
        with requests_mock.Mocker() as m:
            #GET operacoes
            m.get(PFX_OC + 'cb_operations?filter=status,eq,0',
                  text=open(PFX_JSON + 'operacao_revisado.json').read())

            #PUT
            m.put(PFX_OC + 'cb_operations/6', text='1')

            #GET sn_conferencia
            m.get(PFX_OC + 'sn_conference?filter=id_conferencia,eq,99&&filter=codproduto,eq,1',
                  text=open(PFX_JSON + 'sn_conferencia_comum.json').read())

            #PUT sn_conferencia
            m.put(PFX_OC + 'sn_conference/1,99', text='1')
            job_operacoes()

            assert m.call_count == 2
            operacao_enviada = json.loads(m.request_history[1]._request.body)
            assert operacao_enviada['status'] == 1
            assert operacao_enviada['atualizacao'] != '"0000-00-00+00"'