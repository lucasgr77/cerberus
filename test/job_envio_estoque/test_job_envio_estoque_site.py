# encoding: utf-8
import pytest
import mock
import logging
import json
from ..base import BaseTest
import requests_mock

from source.jobs import job_envio_estoque_site
from source.estoque.opencart import get_produtos_associados
from source.database import LocalStorage
from source.estoque.models import MovimentacaoEstoque
from source.sistema.models import MercadoLivreToken


PARAM_SALDO = 1
PARAM_SKU = 0
PFX_JSON='test/job_envio_estoque/json/'
PFX_ML='https://mock-api.mercadolibre.com/'
REQ_OC_OPTION = 'source.estoque.opencart.req_product_option_value'


@mock.patch('source.database.Firebird.connect', mock.MagicMock(return_value=1))
@mock.patch('source.database.Firebird.close', mock.MagicMock(return_value=1))
@mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={
    'oc_product_option_value': {'records': []}
}))
@mock.patch('source.estoque.tiny.confere_atualiza_cadastro', mock.MagicMock(return_value=True))
class TestJobEnvioEstoqueSite(BaseTest):

    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()

    @pytest.fixture
    def insert_data(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=5))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
    def test_habilita_produto_vender(self, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        url_oc = mk_opencart._mock_call_args_list[0][0][0]
        payload = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        #api opencart
        assert '/oc_product/' in url_oc
        assert payload_oc['quantity'] == 1
        assert payload_oc['quantity_fisica'] == 1
        assert mk_tiny.call_count == 1
        assert payload['estoque']['idProduto'] == '789'
        assert payload['estoque']['quantidade'] == '1'
        assert mk_opencart.call_count == 1

        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 3

        #movimentacao estoque tiny
        mov_estq = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'tiny').first()
        assert mov_estq.product_id == 10
        assert mov_estq.fluxo == 'E'
        assert mov_estq.saldo_anterior == 0
        assert mov_estq.saldo_novo == 1
        assert mov_estq.sistema == 'tiny'
        assert mov_estq.job == 'async_altera_estoque'
        assert mov_estq.tipo == 'F'

        #movimentação estoque fisico oc
        mov_estq_oc_fis = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'opencart').all()[0]
        assert mov_estq_oc_fis.fluxo == 'E'
        assert mov_estq_oc_fis.saldo_anterior == 0
        assert mov_estq_oc_fis.saldo_novo == 1
        assert mov_estq_oc_fis.sistema == 'opencart'
        assert mov_estq_oc_fis.job == 'async_altera_estoque'
        assert mov_estq_oc_fis.tipo == 'F'

        #movimentação estoque virtual oc
        mov_estq_oc_virt = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'opencart').all()[1]
        assert mov_estq_oc_virt.fluxo == 'E'
        assert mov_estq_oc_virt.saldo_anterior == 0
        assert mov_estq_oc_virt.saldo_novo == 1
        assert mov_estq_oc_virt.sistema == 'opencart'
        assert mov_estq_oc_virt.job == 'async_altera_estoque'
        assert mov_estq_oc_virt.tipo == 'V'


    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(4, 0, 0)))
    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=4))
    @mock.patch(BaseTest.REQ_TINY, mock.MagicMock(return_value=True))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,'quantity': 3, 'quantity_blocked': 5, 'quantity_fisica': 1, 'codproduto': 1010, 'sku': '123', 'product_option_value_id': 0, 'percentual_sell': 0.20}]))
    def test_desabilita_produto_vender(self, mocker, log):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        payload_tiny = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        assert payload_oc['quantity'] == 2
        assert payload_oc['quantity_fisica'] == 0
        assert mk_tiny.call_count == 1
        assert payload_tiny['estoque']['idProduto'] == '789'
        assert payload_tiny['estoque']['quantidade'] == '2.0'
        assert mk_opencart.call_count == 1

        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 3

        #movimentacao estoque tiny
        mov_estq = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'tiny').first()
        assert mov_estq.fluxo == 'S'
        assert mov_estq.saldo_anterior == 3
        assert mov_estq.saldo_novo == 2
        assert mov_estq.sistema == 'tiny'
        assert mov_estq.job == 'async_altera_estoque'
        assert mov_estq.tipo == 'F'

        #movimentação estoque fisico oc
        mov_estq_oc_fis = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'opencart').all()[0]
        assert mov_estq_oc_fis.fluxo == 'S'
        assert mov_estq_oc_fis.saldo_anterior == 3
        assert mov_estq_oc_fis.saldo_novo == 2
        assert mov_estq_oc_fis.sistema == 'opencart'
        assert mov_estq_oc_fis.job == 'async_altera_estoque'
        assert mov_estq_oc_fis.tipo == 'F'

        #movimentação estoque virtual oc
        mov_estq_oc_virt = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'opencart').all()[1]
        assert mov_estq_oc_virt.fluxo == 'S'
        assert mov_estq_oc_virt.saldo_anterior == 1
        assert mov_estq_oc_virt.saldo_novo == 0
        assert mov_estq_oc_virt.sistema == 'opencart'
        assert mov_estq_oc_virt.job == 'async_altera_estoque'
        assert mov_estq_oc_virt.tipo == 'V'

    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    def test_vende_item_estoque_somente_fisico(self, mocker, log):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mocker.patch(self.GET_SALDO, mock.MagicMock(return_value=5))
        mocker.patch(self.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
        
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        get_produtos_associados = mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
            {'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123', 'quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        payload = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        assert payload_oc['quantity'] == 1
        assert payload_oc['quantity_fisica'] == 1
        assert mk_tiny.call_count == 1
        assert payload['estoque']['idProduto'] == '789'
        assert payload['estoque']['quantidade'] == '1'
        assert mk_opencart.call_count == 1

        assert get_produtos_associados.call_count == 1

        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 3

        #movimentacao estoque tiny
        mov_estq = session.query(MovimentacaoEstoque).filter(MovimentacaoEstoque.sistema == 'tiny').first()
        assert mov_estq.fluxo == 'E'
        assert mov_estq.saldo_anterior == 0
        assert mov_estq.saldo_novo == 1
        assert mov_estq.sistema == 'tiny'
        assert mov_estq.job == 'async_altera_estoque'
        assert mov_estq.tipo == 'F'
        assert mov_estq.opcao_id is None

        #movimentação estoque fisico oc
        mov_estq_oc_fis = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'opencart').all()[0]
        assert mov_estq_oc_fis.fluxo == 'E'
        assert mov_estq_oc_fis.saldo_anterior == 0
        assert mov_estq_oc_fis.saldo_novo == 1
        assert mov_estq_oc_fis.sistema == 'opencart'
        assert mov_estq_oc_fis.job == 'async_altera_estoque'
        assert mov_estq_oc_fis.tipo == 'F'
        assert mov_estq_oc_fis.opcao_id is None

        #movimentação estoque virtual oc
        mov_estq_oc_virt = session.query(MovimentacaoEstoque)\
            .filter(MovimentacaoEstoque.sistema == 'opencart').all()[1]
        assert mov_estq_oc_virt.fluxo == 'E'
        assert mov_estq_oc_virt.saldo_anterior == 0
        assert mov_estq_oc_virt.saldo_novo == 1
        assert mov_estq_oc_virt.sistema == 'opencart'
        assert mov_estq_oc_virt.job == 'async_altera_estoque'
        assert mov_estq_oc_virt.tipo == 'V'
        assert mov_estq_oc_virt.opcao_id is None
        session.close()

        ###tivemos uma venda online e transferimos o produto

        mocker.patch(self.GET_SALDO, mock.MagicMock(return_value=4))
        get_produtos_associados = mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
            {'product_id': 10,
             'quantity': 0,
             'quantity_fisica': 1,
             'codproduto': 1010,
             'sku': '123',
             'quantity_blocked': 5,
             'percentual_sell': 0.2}]))
        job_envio_estoque_site()

        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 3
        assert mk_tiny.call_count == 1
        assert mk_opencart.call_count == 1
        assert get_produtos_associados.call_count == 1


    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    def test_vende_um_estoque_que_tem_estoque_site(self, mocker, log):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mocker.patch(self.GET_SALDO, mock.MagicMock(return_value=5))
        mocker.patch(self.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
        get_produtos_associados = mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
            {'product_id': 10,'quantity': 5, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        payload = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        assert payload_oc['quantity'] == 6
        assert payload_oc['quantity_fisica'] == 1
        assert mk_tiny.call_count == 1
        assert payload['estoque']['idProduto'] == '789'
        assert payload['estoque']['quantidade'] == '6'
        assert mk_opencart.call_count == 1

        ###tivemos uma venda online e transferimos o produto
        ###Não deve ter movimentação
        mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
            {'product_id': 10,
             'quantity': 5,
             'quantity_fisica': 1,
             'codproduto': 1010,
             'sku': '123',
             'quantity_blocked': 5,
             'percentual_sell': 0.2}]))
        job_envio_estoque_site()

        assert mk_opencart.call_count == 1
        assert mk_tiny.call_count == 1


    def test_envio_estoque_produto_com_opcao(self, mocker, log):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch(self.GET_SALDO, mock.MagicMock(return_value=5))
        mocker.patch(self.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
            {'product_id': 10,
             'quantity': 5,
             'quantity_fisica': 0,
             'codproduto': 1010,
             'sku': '123',
             'quantity_blocked': 5,
             'product_option_value_id': 77,
             'percentual_sell': None}]))
        mk_get_tiny_id = mocker.patch(self.GET_TINY_ID, mock.MagicMock(return_value=575))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))

        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        url_oc = mk_opencart._mock_call_args_list[0][0][0]
        payload = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']
        sku = mk_get_tiny_id.call_args[0][0]

        assert sku == '123-77'
        assert '/oc_product_option_value/' in url_oc
        assert payload_oc['quantity'] == 6
        assert payload_oc['quantity_fisica'] == 1
        assert mk_tiny.call_count == 1
        assert payload['estoque']['idProduto'] == '575'
        assert payload['estoque']['quantidade'] == '6'
        assert mk_opencart.call_count == 1

        #tivemos uma venda online de dois produtos e transferimos o produto

        mocker.patch(self.GET_SALDO, mock.MagicMock(return_value=5))
        mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
            {'product_id': 10,
             'quantity': 4,
             'quantity_fisica': 1,
             'codproduto': 1010,
             'sku': '123',
             'quantity_blocked': 5,
             'percentual_sell': 0.2}]))
        job_envio_estoque_site()

        assert mk_opencart.call_count == 1
        assert mk_tiny.call_count == 1


    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=5))
    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
    def test_ml_sem_variacao(self, mocker, log, insert_data):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        url_oc = mk_opencart._mock_call_args_list[0][0][0]
        payload = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        #api opencart
        assert '/oc_product/' in url_oc
        assert payload_oc['quantity'] == 1
        assert payload_oc['quantity_fisica'] == 1
        assert mk_tiny.call_count == 1
        assert payload['estoque']['idProduto'] == '789'
        assert payload['estoque']['quantidade'] == '1'
        assert mk_opencart.call_count == 1

    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=5))
    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
    def test_ml_com_variacao(self, mocker, log, insert_data):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        url_oc = mk_opencart._mock_call_args_list[0][0][0]
        payload = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        #api opencart
        assert '/oc_product/' in url_oc
        assert payload_oc['quantity'] == 1
        assert payload_oc['quantity_fisica'] == 1
        assert mk_tiny.call_count == 1
        assert payload['estoque']['idProduto'] == '789'
        assert payload['estoque']['quantidade'] == '1'
        assert mk_opencart.call_count == 1


    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=5))
    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
    def test_ml_put_exception(self, mocker, log, insert_data):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        url_oc = mk_opencart._mock_call_args_list[0][0][0]
        payload = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        #api opencart
        assert '/oc_product/' in url_oc
        assert payload_oc['quantity'] == 1
        assert payload_oc['quantity_fisica'] == 1
        assert mk_tiny.call_count == 1
        assert payload['estoque']['idProduto'] == '789'
        assert payload['estoque']['quantidade'] == '1'
        assert mk_opencart.call_count == 1


    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=0))
    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(0, 0, 0)))
    @mock.patch(BaseTest.REQ_TINY, mock.MagicMock(return_value=True))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, 
                mock.MagicMock(return_value=[{
                    'product_id': 10,
                    'quantity': 3,
                    'quantity_blocked': 5,
                    'quantity_fisica': 1,
                    'codproduto': 1010,
                    'sku': '123',
                    'product_option_value_id': None,
                    'percentual_sell': 0.20}]))
    def test_desabilita_produto_nova_regra_vmd(self, mocker, log):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.models.get_vmd', mock.MagicMock(return_value=0.10))
        mocker.patch('source.estoque.models.get_cmd', mock.MagicMock(return_value=8))
        mk_opencart = mocker.patch(self.REQ_OPENCART, mock.MagicMock(return_value=True))
        mk_tiny = mocker.patch(self.REQ_TINY, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        url = mk_tiny.call_args[0][0]
        url_oc = mk_opencart._mock_call_args_list[0][0][0]
        payload_tiny = json.loads(url.split('estoque=')[1])
        payload_oc = mk_opencart._mock_call_args_list[0][1]['data']

        # assert opencart
        assert '/oc_product/' in url_oc
        assert payload_oc['quantity'] == 2
        assert payload_oc['quantity_fisica'] == 0
        assert mk_opencart.call_count == 1

        # assert Tiny
        assert payload_tiny['estoque']['idProduto'] == '789'
        assert payload_tiny['estoque']['quantidade'] == '2.0'
        assert mk_tiny.call_count == 1

        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 3
