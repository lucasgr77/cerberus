# encoding: utf-8
import pytest
import mock
import logging
import json
import requests_mock
from ..base import BaseTest

from source.estoque.controller import __get_quantity_stock_blocked__
from source.exceptions import TiException
from source.jobs import job_envio_estoque_site
from source.estoque.opencart import get_produtos_associados
from pytest_mock import mocker
from source.log_server import cerb_logger
from source.database import LocalStorage
from source.estoque.models import MovimentacaoEstoque
from source.estoque import opencart, tiny


REQ_OC_OPTION = 'source.estoque.opencart.req_product_option_value'
VARIACAO_OBRIGATORIA = 'source.estoque.controller.__tem_variacao_obrigatoria__'
OC_URL_BASE = 'http://mock-onlinemotopecas.com.br/api.php/'
PFX_JSON = 'test/job_envio_estoque/json/'

@mock.patch('source.database.Firebird.connect', mock.MagicMock(return_value=1))
@mock.patch('source.database.Firebird.close', mock.MagicMock(return_value=1))
@mock.patch('source.estoque.tiny.confere_atualiza_cadastro', mock.MagicMock(return_value=True))
@mock.patch(VARIACAO_OBRIGATORIA, return_value=False)
class TestEstoqueVirtual(BaseTest):

    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=5))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 3,
                                                                            'quantity_fisica': 1,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 1,
                                                                            'percentual_sell': 0.20}]))
    def test_produto_habilitado_continua_habilitado(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        assert mk_opencart.call_count == 0

    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=4))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 3,
                                                                            'sku': '123',
                                                                            'quantity_fisica': 0,
                                                                            'codproduto': 1010,
                                                                            'quantity_blocked': 1,
                                                                            'product_option_value_id': None,
                                                                            'percentual_sell': 0.20}]))
    def test_exception_consulta_produtos_associados(self, variacao_obrigatoria, log, mocker):
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))

        mk_produtos_associados = mocker.patch.object(opencart, 'get_produtos_associados')
        mk_produtos_associados.side_effect = Exception('Erro esperado')
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        job_envio_estoque_site()
        assert mk_opencart.call_count == 0
        assert mock_tiny.call_count == 0
        assert exception_notifier.call_count == 1

    @mock.patch(BaseTest.GET_SALDO, mock.MagicMock(return_value=6))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 0,
                                                                            'quantity_fisica': 0,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 1,
                                                                            'percentual_sell': 0.20}]))
    def test_tiny_exception(self, variacao_obrigatoria, log, mocker):
        mock_log = mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))

        mock_tiny.side_effect = TiException('Erro esperado Tiny')
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        job_envio_estoque_site()
        assert mk_opencart.call_count == 0
        assert exception_notifier.call_count == 1

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(6, 0, 0)))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 0,
                                                                            'quantity_fisica': 0,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 5,
                                                                            'product_option_value_id': None,
                                                                            'percentual_sell': 0.20}]))
    def test_vende_loja_fisica_saldo_de_6(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 1

        assert sku == '123'
        assert quantity_tiny == 1

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(7, 0, 0)))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 0,
                                                                            'quantity_fisica': 0,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 5,
                                                                            'product_option_value_id': None,
                                                                            'percentual_sell': 0}]))
    def test_vende_loja_fisica_saldo_7(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 1

        assert sku == '123'
        assert quantity_tiny == 1

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(side_effect=[(2, 0, 0),(5, 0, 0)]))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
        {'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 2, 'product_option_value_id': None, 'percentual_sell': 0.20},
        {'product_id': 20,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1020, 'sku': '1234','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
    def test_vende_loja_fisica_anuncio_dois_produtos(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=1))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()
        product_id, quantity, quantity_fisico = mk_opencart.call_args_list[0][0]
        product_id, sku, quantity_tiny = mock_tiny.call_args_list[0][0]

        assert product_id in [20, 10]
        assert quantity == 1
        assert quantity_fisico == 1
        assert sku in ['123', '1234']
        assert quantity_tiny == 1

        product_id, quantity, quantity_fisico = mk_opencart.call_args_list[1][0]
        product_id, sku, quantity_tiny = mock_tiny.call_args_list[1][0]

        assert product_id in [20, 10]
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 2
        assert mock_tiny.call_count == 2
        assert sku in ['123', '1234']
        assert quantity_tiny == 1

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(side_effect=[(6, 0, 0),(1, 0, 0)]))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
        {'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell':0.20},
        {'product_id': 20,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1020, 'sku': '1234','quantity_blocked': 2, 'product_option_value_id': None, 'percentual_sell':0.20}]))
    def test_vende_loja_fisica_anuncio_dois_produtos_passando_1(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 1
        assert mock_tiny.call_count == 1
        assert sku == '123'
        assert quantity_tiny == 1


    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(side_effect=[(6, 0, 0),(1, 0, 0)]))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
        {'product_id': 10,'quantity': 0, 'quantity_fisica': 0, 'codproduto': 1010, 'sku': '123','quantity_blocked': 5, 'product_option_value_id': None, 'percentual_sell': 0.20}]))
    def test_tiny_baixa_estoque_fisico(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 1
        assert mock_tiny.call_count == 1
        assert sku == '123'
        assert quantity_tiny == 1

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(5,0,0)))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 0,
                                                                            'quantity_fisica': 1,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 5,
                                                                            'product_option_value_id': None,
                                                                            'percentual_sell': 0.20}]))
    def test_produto_vendido_sem_movimentacao(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_opcao_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        mk_opencart.call_count == 0
        mock_tiny.call_count == 0


    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(7,0,0)))
    @mock.patch(REQ_OC_OPTION, mock.MagicMock(return_value={'oc_product_option_value': {'records': []}}))
    def test_option_value_id_concatenado(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_opcao_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{
            'product_id': 10,
            'quantity': 0,
            'quantity_fisica': 0,
            'codproduto': 1010,
            'sku': '123',
            'quantity_blocked': 5,
            'product_option_value_id': 23,
            'percentual_sell': 0.20
        }]))
        job_envio_estoque_site()

        product_id, option_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert option_id == 23
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 1

        assert sku == '123-23'
        assert quantity_tiny == 1
        assert mk_opencart.call_count == 1

        mocker.patch(self.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{
            'product_id': 10,
            'quantity': 1,
            'quantity_fisica': 1,
            'codproduto': 1010,
            'sku': '123',
            'quantity_blocked': 5,
            'product_option_value_id': 23,
            'percentual_sell': 0.20
        }]))
        job_envio_estoque_site()
        assert mk_opencart.call_count == 1
        assert quantity == 1
        assert quantity_fisico == 1



    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(17,0,0)))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 2,
                                                                            'quantity_fisica': 0,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 5,
                                                                            'product_option_value_id': None,
                                                                            'percentual_sell': 0.50}]))
    def test_persentual_sell_sucesso(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 8
        assert quantity_fisico == 6
        assert mk_opencart.call_count == 1

        assert sku == '123'
        assert quantity_tiny == 8



    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(15,0,0)))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 5,
                                                                            'quantity_fisica': 0,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 5,
                                                                            'product_option_value_id': None,
                                                                            'percentual_sell': 0.50}]))
    def test_persentual_sell_quantity_5(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 10
        assert quantity_fisico == 5
        assert mk_opencart.call_count == 1

        assert sku == '123'
        assert quantity_tiny == 10


    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(15,0,0)))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[{'product_id': 10,
                                                                            'quantity': 0,
                                                                            'quantity_fisica': 0,
                                                                            'codproduto': 1010,
                                                                            'sku': '123',
                                                                            'quantity_blocked': 5,
                                                                            'product_option_value_id': None,
                                                                            'percentual_sell': None}]))
    def test_persentual_sell_nulo(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 1

        assert sku == '123'
        assert quantity_tiny == 1


    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(15,0,0)))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(
        return_value=[
            {'product_id': 10,
             'quantity': 0,
             'quantity_fisica': 0,
             'codproduto': 1010,
             'sku': '123',
             'quantity_blocked': 5,
             'product_option_value_id': None,
             'percentual_sell': 0}]))
    def test_percentual_sell_zerado(self, variacao_obrigatoria, log, mocker):
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_opencart = mocker.patch.object(opencart, 'atualiza_saldo_oc')
        mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
        mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
        job_envio_estoque_site()

        product_id, quantity, quantity_fisico = mk_opencart.call_args[0]
        product_id, sku, quantity_tiny = mock_tiny.call_args[0]

        assert product_id == 10
        assert quantity == 1
        assert quantity_fisico == 1
        assert mk_opencart.call_count == 1

        assert sku == '123'
        assert quantity_tiny == 1


    def test_qtdb_0(self, v1):
        quantity_blocked = 0
        cmd = 11
        vmd = 0.01
        qtd_bloqueada = __get_quantity_stock_blocked__(123, quantity_blocked, cmd, vmd)
        assert qtd_bloqueada == 0


    def test_cmd_9_qtdb_0(self, mocker):
        quantity_blocked = 0
        cmd = 9
        vmd = 0.02
        qtd_bloqueada = __get_quantity_stock_blocked__(123, quantity_blocked, cmd, vmd)
        assert qtd_bloqueada == 0


    def test_cmd_8_qtdb_1(self, variacao_obrigatoria, log, mocker):
        quantity_blocked = 0
        cmd = 8
        vmd = 0.30
        qtd_bloqueada = __get_quantity_stock_blocked__(123, quantity_blocked, cmd, vmd)
        assert qtd_bloqueada == 1


    def test_cmd_7_qtdb_2(self, variacao_obrigatoria, log, mocker):
        quantity_blocked = 2
        cmd = 7
        vmd = 2.8
        qtd_bloqueada = __get_quantity_stock_blocked__(123, quantity_blocked, cmd, vmd)
        assert qtd_bloqueada == 2


    def test_cmd_15_qtdb_0(self, variacao_obrigatoria, log, mocker):
        quantity = 0
        cmd = 15
        vmd = 10.0
        qtd_bloqueada = __get_quantity_stock_blocked__(123, quantity, cmd, vmd)
        assert qtd_bloqueada == 0


    def test_cmd_erro_quantity_blocked_assume(self, variacao_obrigatoria, log, mocker):
        quantity = 7
        cmd = None
        vmd = 2.00
        qtd_bloqueada = __get_quantity_stock_blocked__(123, quantity, cmd, vmd)
        assert qtd_bloqueada == 7


    def test_vmd_nulo_quantity_blocked_assume(self, variacao_obrigatoria, log, mocker):
        prod = {'quantity_blocked': 8, 'sku':'123'}
        quantity = 8
        cmd = 3
        vmd = None
        qtd_bloqueada = __get_quantity_stock_blocked__(123, quantity, cmd, vmd)
        assert qtd_bloqueada == 8
