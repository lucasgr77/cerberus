# encoding: utf-8
import pytest
import mock
import json
import requests_mock
from ..base import BaseTest

from source.jobs import job_envio_estoque_site
from source.estoque import tiny


VARIACAO_OBRIGATORIA = 'source.estoque.controller.__tem_variacao_obrigatoria__'
CONFERE_ATUALIZA = 'source.estoque.tiny.confere_atualiza_cadastro'
OC_URL_BASE = 'http://mock-onlinemotopecas.com.br/api.php/'
PFX_JSON = 'test/job_envio_estoque/json/'
GET_SALDO = 'source.estoque.models.get_saldo_produto'
PATH_CERB_LOGGER = 'source.log_server.cerb_logger'
REQ_ML = 'source.estoque.mercado_livre.atualiza_saldo_ml_variacao'
ATUALIZA_SALDO_TINY = 'source.estoque.tiny.atualiza_saldo_tiny'

class TestEstoqueComOpcao(BaseTest):

	def test_produto_com_povi_sucesso(self, log, mocker):
  		mocker.patch('source.estoque.opencart.req_products_association',
                 return_value=[[29920,502893159,"s",2,1,0.25]])
		mocker.patch(CONFERE_ATUALIZA, mock.MagicMock(return_value=True))
		mocker.patch(VARIACAO_OBRIGATORIA, return_value=False)
		mocker.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(15, 0, 0)))
		mocker.patch(PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
		mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=2))
		mocker.patch(REQ_ML, mock.MagicMock(return_value=True))
		mock_tiny = mocker.patch(ATUALIZA_SALDO_TINY)
		with requests_mock.Mocker() as m:
			m.get(OC_URL_BASE + 'oc_product_option_value/1',
				  text=open(PFX_JSON + 'oc_product_option_value_objeto.json').read())
			m.get(OC_URL_BASE + 'products_association',
				  text=open(PFX_JSON + 'products_association_list.json').read())
			m.get(OC_URL_BASE + 'oc_product/502893159',
				  text=open(PFX_JSON + 'oc_product.json').read())
			m.put(OC_URL_BASE + 'oc_product/502893159',
				  text='1')
			m.put(OC_URL_BASE + 'oc_product_option_value/1',
				  text='1')
			job_envio_estoque_site()

			product_id, sku, quantity_tiny = mock_tiny.call_args[0]

			assert product_id == 502893159

			# request povi
			last_request = len(m._adapter.request_history) - 1
			assert 'oc_product_option_value/1' in m._adapter.request_history[last_request].path
			payload_update = json.loads(m._adapter.request_history[last_request].text)
			assert payload_update['quantity'] == 3.0
			assert payload_update['quantity_fisica'] == 3.0

			# assert sku == '123-1'
			assert quantity_tiny == 3.0

	def test_produto_com_povi_nao_encontrado(self, log, mocker):
		mocker.patch(CONFERE_ATUALIZA, mock.MagicMock(return_value=True))
		mocker.patch(VARIACAO_OBRIGATORIA, return_value=False)
		mocker.patch(GET_SALDO, mock.MagicMock(return_value=15))
		mocker.patch(PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
		mocker.patch(REQ_ML, mock.MagicMock(return_value=True))
		mock_tiny = mocker.patch.object(tiny, 'atualiza_saldo_tiny')
		mocker.patch(self.REQ_ML, mock.MagicMock(return_value=True))
		exception_notifier = mocker.patch('source.extensions.exception_notifier')
		with requests_mock.Mocker() as m:
			m.get(OC_URL_BASE + 'oc_product_option_value/1',
				  text='Not found (object)', status_code=404)
			m.get(OC_URL_BASE + 'products_association',
				  text=open(PFX_JSON + 'products_association_list.json').read())
			m.get(OC_URL_BASE + 'oc_product/502893159',
				  text=open(PFX_JSON + 'oc_product.json').read())
			m.put(OC_URL_BASE + 'oc_product/502893159',
				  text='1')
			m.put(OC_URL_BASE + 'oc_product_option_value/1',
				  text='1')
			job_envio_estoque_site()

			mock_tiny.call_count == 0
			exception_notifier.call_count == 1
