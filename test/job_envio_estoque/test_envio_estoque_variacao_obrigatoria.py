# encoding: utf-8
import pytest
import mock
import json
import json
from ..base import BaseTest
import requests_mock

from source.jobs import job_envio_estoque_site
from source.estoque.opencart import get_produtos_associados
from pytest_mock import mocker
from source.database import LocalStorage
from source.estoque.models import MovimentacaoEstoque
from source.sistema.models import MercadoLivreToken
from source.catalogo_mercado_livre.models import salvar


PARAM_SALDO = 1
PARAM_SKU = 0
PFX_JSON='test/job_envio_estoque/json/'
PFX_ML='https://mock-api.mercadolibre.com/'
REQ_OC_OPTION = 'source.estoque.opencart.req_product_option_value'
URL_OC_PRODUCT_OPTION_VALUE='http://mock-onlinemotopecas.com.br/api.php/oc_product_option_value'
URL_ML_ATUALIZA_SALDO='https://mock-api.mercadolibre.com/items/mlid1'
ATUALIZA_SALDO_ML_VARIACAO='source.estoque.controller.atualiza_saldo_ml_variacao'
ATUALIZA_SALDO_OC='source.estoque.opencart.atualiza_saldo_oc'
ATUALIZA_SALDO_TINY='source.estoque.tiny.atualiza_saldo_tiny'

@mock.patch('source.database.Firebird.connect', mock.MagicMock(return_value=1))
@mock.patch('source.database.Firebird.close', mock.MagicMock(return_value=1))
@mock.patch('source.estoque.tiny.confere_atualiza_cadastro', mock.MagicMock(return_value=True))
@mock.patch('source.api.get_token', return_value=MercadoLivreToken(access_token='123'))
class TestEnvioEstoqueVariacao(BaseTest):

    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
        {'product_id': 10,
         'quantity': 0,
         'quantity_fisica': 0,
         'codproduto': 1010,
         'sku': '123',
         'quantity_blocked': 5,
         'product_option_value_id': None,
         'percentual_sell': 0.20}]))
    def test_envio_produto_variacao_ml_sucesso(self, get_token, log, mocker):
        sku = '123'
        quantidade_enviada = 1

        salvar('mlid1', sku, [1])
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mk_atualiza_saldo_oc = mocker.patch(ATUALIZA_SALDO_OC)
        mk_atualiza_saldo_tiny = mocker.patch(ATUALIZA_SALDO_TINY)
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        with requests_mock.Mocker() as m:
            m.get(URL_OC_PRODUCT_OPTION_VALUE + '?filter=product_id,eq,10',
                text=open(PFX_JSON + 'oc_product_option_value_sem_opcao.json').read())
            m.put(URL_ML_ATUALIZA_SALDO, text='1')
            job_envio_estoque_site()

            # chamada ml variação
            request_mercado_livre = m._adapter.request_history[1]
            body = json.loads(request_mercado_livre.text)
            assert 'mlid1' in request_mercado_livre.path
            assert body['variations'][0]['available_quantity'] == quantidade_enviada
            assert body['variations'][0]['id'] == 1

            mk_atualiza_saldo_oc.call_count == 1
            mk_atualiza_saldo_tiny.call_count == 1

    @mock.patch(BaseTest.GET_SALDO_V2, mock.MagicMock(return_value=(5, 0, 0)))
    @mock.patch(BaseTest.GET_TINY_ID, mock.MagicMock(return_value=789))
    @mock.patch(BaseTest.PRODUCTS_ASSOCIADOS, mock.MagicMock(return_value=[
        {'product_id': 10,
         'quantity': 0,
         'quantity_fisica': 0,
         'codproduto': 1010,
         'sku': '123',
         'quantity_blocked': 5,
         'product_option_value_id': None,
         'percentual_sell': 0.20}]))
    def test_produto_tem_opcao_com_variacao(self, p1, log, mocker):
        sku = '123'
        quantidade_enviada = 1

        salvar('mlid1', sku, [1])
        mocker.patch(self.PATH_CERB_LOGGER, mock.MagicMock(return_value=log))
        mocker.patch('source.estoque.controller.__get_quantity_stock_blocked__', mock.MagicMock(return_value=5))
        mk_atualiza_saldo_oc = mocker.patch(ATUALIZA_SALDO_OC)
        mk_atualiza_saldo_tiny = mocker.patch(ATUALIZA_SALDO_TINY)
        with requests_mock.Mocker() as m:
            m.get(URL_OC_PRODUCT_OPTION_VALUE + '?filter=product_id,eq,10',
                text=open(PFX_JSON + 'oc_product_option_value_com_opcao.json').read())
            m.put(URL_ML_ATUALIZA_SALDO, text='1', status_code=500)
            job_envio_estoque_site()

            # sem chamada ml variação
            assert len(m._adapter.request_history) == 1

            mk_atualiza_saldo_oc.call_count == 1
            mk_atualiza_saldo_tiny.call_count == 1
