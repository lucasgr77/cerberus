# encoding: utf-8
import mock
import pytest
import requests_mock
from pytest_mock import mocker
from source.api import MlException
from source.database import LocalStorage
from source.catalogo_mercado_livre.controller import (
   download_catalogo
)
from source.catalogo_mercado_livre.models import MlCadastro, MlVariacao
from source.sistema.models import MercadoLivreToken
from ..base import BaseTest


PFX_MERCADO_LIVRE = 'https://mock-api.mercadolibre.com/'
PFX_JSON = 'test/job_catalogo_ml/jsons/'


@mock.patch('source.api.get_token', return_value=MercadoLivreToken(access_token='123'))
@mock.patch('source.catalogo_mercado_livre.controller.get_todos_mlids', return_value=['MLB1815211611'])
class TestCatalogoDownload(BaseTest):

    def test_download_catalogo_sucesso(self, mlids, get_token):
        session = LocalStorage.get_session()
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + 'items/MLB1815211611',
                  text=open(PFX_JSON + 'produto_ml.json').read())
            download_catalogo()
            cadastro = session.query(MlCadastro).first()
            assert cadastro.sku == '1234'
            assert cadastro.variacoes[0].id_variacao == 78000450563
            assert len(cadastro.variacoes) == 3


    def test_download_erro_chamada(self, p1, p2, mocker):
        except_notify = mocker.patch('source.extensions.exception_notifier')
        session = LocalStorage.get_session()
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + 'items/MLB1815211611',
                  text=open(PFX_JSON + 'produto_ml_404.json').read(), status_code=404)
            download_catalogo()
            cadastros = session.query(MlCadastro).all()
            assert len(cadastros) == 0
            assert except_notify.call_count == 1

    def test_salva_estoura_exception(self, p1, p2, mocker):
        except_notify = mocker.patch('source.extensions.exception_notifier')
        mk_salvar = mocker.patch('source.catalogo_mercado_livre.models.salvar',
                                 side_effect=MlException('Erro esperado'))
        session = LocalStorage.get_session()
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + 'items/MLB1815211611',
                  text=open(PFX_JSON + 'produto_ml.json').read())
            download_catalogo()
            cadastros = session.query(MlCadastro).all()
            assert len(cadastros) == 0
            assert mk_salvar.call_count == 1
            assert except_notify.call_count == 1

    def test_salva_sem_variacao(self, p1, p2, mocker):
        except_notify = mocker.patch('source.extensions.exception_notifier')
        session = LocalStorage.get_session()
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + 'items/MLB1815211611',
                  text=open(PFX_JSON + 'produto_sem_variacoes.json').read())
            download_catalogo()
            cadastro = session.query(MlCadastro).first()
            assert cadastro.sku == '1234'
            assert len(cadastro.variacoes) == 0
