import requests_mock
import pytest
import mock
from source.api import MlException
from source.catalogo_mercado_livre.controller import (
   URL_CATALOGO, get_todos_mlids
)
from source.sistema.models import MercadoLivreToken

PFX_MERCADO_LIVRE = 'https://mock-api.mercadolibre.com/'
PFX_JSON = 'test/job_catalogo_ml/jsons/'
offset_zero = '0'
offset_final = '50'


@mock.patch('source.api.get_token', return_value=MercadoLivreToken(access_token='123'))
class TestIterador:

    def test_call_iterator(self, get_token):
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + URL_CATALOGO + offset_zero,
                  text=open(PFX_JSON + 'mercadolivre_offset_0.json').read())
            m.get(PFX_MERCADO_LIVRE + URL_CATALOGO + offset_final,
                  text=open(PFX_JSON + 'mercadolivre_offset_vazio.json').read())
            retorno = get_todos_mlids()
            assert len(retorno) == 50

    def test_api_retorno_vazio(self, get_token):
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + URL_CATALOGO + offset_zero,
                  text=open(PFX_JSON + 'mercadolivre_offset_vazio.json').read())
            m.get(PFX_MERCADO_LIVRE + URL_CATALOGO + offset_final,
                  text=open(PFX_JSON + 'mercadolivre_offset_vazio.json').read())
            retorno = get_todos_mlids()
            assert len(retorno) == 0

    def test_exception_primeira_chamada(self, get_token):
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + URL_CATALOGO + offset_zero,
                  text=open(PFX_JSON + 'forbidden.json').read(), status_code=403)
            with pytest.raises(MlException) as ex:
                get_todos_mlids()
                assert ex is not None

    def test_exception_nas_threads(self, get_token):
        with requests_mock.Mocker() as m:
            m.get(PFX_MERCADO_LIVRE + URL_CATALOGO + offset_zero,
                  text=open(PFX_JSON + 'mercadolivre_offset_0.json').read(), status_code=403)
            m.get(PFX_MERCADO_LIVRE + URL_CATALOGO + offset_final,
                  text=open(PFX_JSON + 'forbidden.json').read())
            with pytest.raises(MlException) as ex:
                get_todos_mlids()
