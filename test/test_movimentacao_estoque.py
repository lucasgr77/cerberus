# -*- coding:utf-8 -*-
import pytest
import mock
import json
import logging
import requests_mock
import datetime

from source.database import LocalStorage
from source.estoque.models import MovimentacaoEstoque
from source.sistema.service import log_movimentacao_estoque, MovimentacaoException


class TestMovimentacaoEstoque:

    @pytest.fixture(autouse=True)
    def setup(self):
        self.__before_test__()
        yield
        self.__after_test__()

    def __before_test__(self):
        LocalStorage.connect(False)

    def __after_test__(self):
        LocalStorage.drop()


    def test_salvar_movimentacao_estoque_sucesso(self):
        product_id = 12345
        loja = 1
        sistema = 'ambiente_soft'
        saldo_anterior = 5
        saldo_novo = 10
        tipo = 'A'
        log_movimentacao_estoque(product_id, loja, sistema, saldo_anterior, saldo_novo, tipo)
        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 1
        assert loja == 1
        assert sistema == 'ambiente_soft'
        assert saldo_anterior == 5
        assert saldo_novo == 10
        assert tipo == 'A'



    def test_salvar_movimentacao_estoque_loja_invalido(self):
        product_id = 12345
        loja = None
        sistema = 'ambiente_soft'
        saldo_anterior = 2
        saldo_novo = 3
        tipo = 'A'
        with pytest.raises(MovimentacaoException) as ex:
            log_movimentacao_estoque(product_id, loja, sistema, saldo_anterior, saldo_novo, tipo)
        assert ex.value[0] == 'Campo loja invalido'
        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 0


    def test_salvar_movimentacao_estoque_sistema_invalido(self):
        product_id = 12345
        loja = 2
        sistema = 3
        job = 'job_vendas_faturadas'
        saldo_anterior = 2
        saldo_novo = 3
        tipo = 'A'
        with pytest.raises(MovimentacaoException) as ex:
            log_movimentacao_estoque(product_id, loja, sistema, saldo_anterior, saldo_novo, tipo)
        assert ex.value[0] == 'Campo sistema invalido'
        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 0

    def test_salvar_movimentacao_estoque_saldo_anterior_invalido(self):
        product_id = 12345
        loja = 2
        sistema = 'ambiente_soft'
        saldo_anterior = 'saldo'
        saldo_novo = 3
        tipo = 'A'
        with pytest.raises(MovimentacaoException) as ex:
            log_movimentacao_estoque(product_id, loja, sistema, saldo_anterior, saldo_novo, tipo)
        assert ex.value[0] == 'Campo saldo_anterior invalido'
        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 0

    def test_salvar_movimentacao_estoque_saldo_novo_invalido(self):
        product_id = 12345
        loja = 2
        sistema = 'ambiente_soft'
        saldo_anterior = 2
        saldo_novo = None
        tipo = 'A'
        with pytest.raises(MovimentacaoException) as ex:
            log_movimentacao_estoque(product_id, loja, sistema, saldo_anterior, saldo_novo, tipo)
        assert ex.value[0] == 'Campo saldo_novo invalido'
        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 0

    def test_salvar_movimentacao_estoque_tipo_invalido(self):
        product_id = 12345
        loja = 2
        sistema = 'ambiente_soft'
        saldo_anterior = 2
        saldo_novo = 3
        tipo = 10
        with pytest.raises(MovimentacaoException) as ex:
            log_movimentacao_estoque(product_id, loja, sistema, saldo_anterior, saldo_novo, tipo)
        assert ex.value[0] == 'Campo tipo invalido'
        session = LocalStorage.get_session()
        assert len(session.query(MovimentacaoEstoque).all()) == 0



