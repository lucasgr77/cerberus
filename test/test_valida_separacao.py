import mock
import pytest
import requests
import requests_mock
import time

from source.exceptions import OcException
from source.estoque.opencart import get_produtos_associados, atualiza_saldo_oc, valida_separacao,req_oc_product
from source.estoque.tiny import atualizar_estoque_tiny, TiException


SEPARAR_ESTOQUES = 'source.notify.service.notificar_separar_estoques'

class TestValidaSeparacao:


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value= {
        "product_id": 502892076,
        "quantity": 0,
        "quantity_fisica": 1
    }))
    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_valida_separacao_sucesso(self, mocker):
        COD_PRODUTO = 35
        QUANTIDADE_VENDIDA = 1
        mock_notificacao= mocker.patch(SEPARAR_ESTOQUES)
        pedido = {
            'id': 1,
            'itens': [{'product_id': 502892076, 'sku': 'a', "quantidade" : QUANTIDADE_VENDIDA}],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [[COD_PRODUTO,502892076,'s']])
        assert mock_notificacao.call_count == 1
        codproduto, quantidade = mock_notificacao.call_args[0]
        assert codproduto == COD_PRODUTO
        assert quantidade == QUANTIDADE_VENDIDA


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={
        "product_id": 502892076,
        "quantity": 1,
        "quantity_fisica": 2
    }))
    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_venda_metade_estoque_fisico(self, mocker):
        COD_PRODUTO = 35
        QUANTIDADE_VENDIDA = 1
        mock_notificacao= mocker.patch(SEPARAR_ESTOQUES)
        pedido = {
            'id': 1,
            'itens': [{'product_id': 502892076, 'sku': 'a', "quantidade" : QUANTIDADE_VENDIDA}],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [[COD_PRODUTO,502892076,'s']])
        assert mock_notificacao.call_count == 1
        codproduto, quantidade = mock_notificacao.call_args[0]
        assert codproduto == COD_PRODUTO
        assert quantidade == QUANTIDADE_VENDIDA


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={
        "product_id": 502892076,
        "quantity": 1,
        "quantity_fisica": 3
    }))
    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_venda_parte_estoque_fisico(self, mocker):
        COD_PRODUTO = 35
        QUANTIDADE_VENDIDA = 2
        mock_notificacao= mocker.patch(SEPARAR_ESTOQUES)
        pedido = {
            'id': 1,
            'itens': [{'product_id': 502892076, 'sku': 'a', "quantidade" : QUANTIDADE_VENDIDA}],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [[COD_PRODUTO,502892076,'s']])
        assert mock_notificacao.call_count == 1
        codproduto, quantidade = mock_notificacao.call_args[0]
        assert codproduto == COD_PRODUTO
        assert quantidade == QUANTIDADE_VENDIDA

    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_venda_metade_uma_venda(self, mocker):
        COD_PRODUTO = 35
        QUANTIDADE_VENDIDA_1 = 3
        QUANTIDADE_VENDIDA_2 = 1
        mock_notificacao= mocker.patch(SEPARAR_ESTOQUES)
        mk_oc_product = mocker.patch('source.estoque.opencart.req_oc_product')
        mk_oc_product.return_value = {
            "product_id": 502892076,
            "quantity": 1,
            "quantity_fisica": 4
        }
        pedido = {
            'id': 1,
            'itens': [{'product_id': 502892076, 'sku': 'a', "quantidade" : QUANTIDADE_VENDIDA_1}],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [[COD_PRODUTO, 502892076, 's']])
        assert mock_notificacao.call_count == 1
        codproduto, quantidade = mock_notificacao.call_args[0]
        assert codproduto == COD_PRODUTO
        assert quantidade == QUANTIDADE_VENDIDA_1
        assert mock_notificacao.call_count == 1

        mk_oc_product.return_value = {
            "product_id": 502892076,
            "quantity": 1,
            "quantity_fisica": 1
        }

        pedido = {
            'id': 1,
            'itens': [{'product_id': 502892076, 'sku': 'a', "quantidade" : QUANTIDADE_VENDIDA_2}],
            'nome_vendedor': 'Mercado Livre'
        }
        #pedido 2
        valida_separacao(pedido, [[COD_PRODUTO, 502892076, 's']])
        assert mock_notificacao.call_count == 1


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value= {
        "product_id": 502892076,
        "quantity": 1,
        "quantity_fisica": 1
    }))
    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_produtos_nao_parametrizado(self, mocker):
        mock_notificacao= mocker.patch(SEPARAR_ESTOQUES)
        pedido = {
            'id': 1,
            'itens': [{'product_id': 502892076, 'sku' : 'a', "quantidade" : 1}],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [[35, 502892076, 'n']])
        assert mock_notificacao.call_count == 0


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value= {
        "sku": "a",
        "product_id": 502892076,
        "quantity": 0,
        "quantity_fisica": 5
    }))
    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_muitos_produtos_parametrizados_sucesso(self, mocker):
        mock_notificacao = mocker.patch(SEPARAR_ESTOQUES)
        pedido = {
            'id': 1,
            'itens':  [{'product_id': 502892076, "sku": "c", "quantidade" : 1},
                        {'product_id': 502892079, "sku": "b", "quantidade" : 1},
                        {'product_id': 502892078, "sku": "a", "quantidade" : 1}],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [[35,502892076,'s'],[39,502892079,'s'],[40,502892078,'s']])
        assert mock_notificacao.call_count == 3


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value= {
        "product_id": 502892076,
        "quantity": 5,
        "quantity_fisica": 5
    }))
    def test_lista_de_entrada_vazia(self, mocker):
        mock_notificacao= mocker.patch(SEPARAR_ESTOQUES)
        pedido = {
            'id': 1,
            'itens': [],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [])
        assert mock_notificacao.call_count == 0


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value= {
        "product_id": 502892076,
        "quantity": 5,
        "quantity_fisica": 0
    }))
    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_venda_sem_estoque_fisico(self, mocker):
        mock_notificacao= mocker.patch(SEPARAR_ESTOQUES)
        pedido = {
            'id': 1,
            'itens': [{'id_produto_tiny': 1, 'sku': 'a', 'quantidade': 1}],
            'nome_vendedor': 'Mercado Livre'
        }
        valida_separacao(pedido, [[35,502892076,'s']])
        assert mock_notificacao.call_count == 0


    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value ={
        "product_id": 502892076,
        "quantity": 0,
        "quantity_fisica":5
    }))
    @mock.patch('source.estoque.opencart.processar_saldo_venda', mock.MagicMock(return_value=True))
    @mock.patch('source.produtos.service.get_oc_id_from_sku', mock.MagicMock(return_value=502892076))
    def test_notificar_separar_estoque_joga_exception(self, mocker):
        mock_request =  mocker.patch(SEPARAR_ESTOQUES)
        mock_request.side_effect = Exception("ERRO!!!")
        MENSAGEM_ERRO = 'ERRO!!!'
        pedido = {
            'id': 1,
            'itens': [{'id_produto_tiny': 1, 'sku': 'a', 'quantidade': 1}],
            'nome_vendedor': 'Mercado Livre'
        }
        with pytest.raises(Exception) as ex:
            valida_separacao(pedido, [[35,502892076,'s']])
        assert ex.value[0] == MENSAGEM_ERRO   

