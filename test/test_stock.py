import mock
import pytest
import requests
import requests_mock
import time

from source.exceptions import OcException
from source.estoque.opencart import get_produtos_associados, atualiza_saldo_oc, req_product_option_value
from source.estoque.tiny import atualizar_estoque_tiny, TiException

VARIACAO_OBRIGATORIA = 'source.estoque.controller.__tem_variacao_obrigatoria__'

@mock.patch(VARIACAO_OBRIGATORIA, return_value=False)
class TestStock:

    @mock.patch('source.estoque.opencart.req_products_association', mock.MagicMock(return_value=[[1,1020300,'n',5,None, None],[2,102022,'s',5, None, None]]))
    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={'sku': '123',
    'product_id': 102022,
    'model': 'Titan 150',
    'quantity_fisica':0,
    'quantity': 7}))
    def test_prodcut_association_sucesso(self, m1):
        result = get_produtos_associados()
        retorno_esperado = [{
            'product_id' : 102022,
            'quantity' : 7,
            'quantity_fisica':0,
            'codproduto': 2,
            'sku': '123',
            'quantity_blocked': 5,
            'product_option_value_id': None,
            'percentual_sell': None
        }]
        assert result == retorno_esperado


    @mock.patch('source.estoque.opencart.req_products_association', mock.MagicMock(return_value=[[5,551060,'s', 5, None, None],[6,885566,'s',5, None, None],[8,156897,'s',5, None, None],[15,778899,'n',5, None, None]]))
    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={'sku': '123',
    'product_id': 102022,
    'quantity_fisica':0,
    'model': 'Titan 150',
    'quantity': 9}))
    def test_product_association_retorno_tres_sucessos(self, m1):
        result = get_produtos_associados()
        retorno_esperado = [{
            'product_id' : 551060, 
            'quantity' : 9,
            'quantity_fisica':0,
            'codproduto': 5,
            'sku': '123',
            'quantity_blocked': 5,
            'product_option_value_id': None,
            'percentual_sell': None

        },
        {
            'product_id' : 885566, 
            'quantity' : 9,
            'quantity_fisica':0,
            'codproduto': 6,
            'sku': '123',
            'quantity_blocked': 5,
            'product_option_value_id': None,
            'percentual_sell': None
        },

        {
            'product_id' : 156897, 
            'quantity' : 9,
            'quantity_fisica':0,
            'codproduto': 8,
            'sku': '123',
            'quantity_blocked': 5,
            'product_option_value_id': None,
            'percentual_sell': None
        }]

        assert result == retorno_esperado



    @mock.patch('source.estoque.opencart.req_products_association', mock.MagicMock(return_value=[[21,551061,'n'],[22,885562,'n'],[23,156863,'n'],[24,778864,'n']]))
    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={'sku': '123',
    'product_id': 102022,
    'model': 'Titan 150',
    'quantity_fisica':0,
    'quantity': 11}))
    def test_product_association_todos_desativados(self, m1):
         result = get_produtos_associados()
         retorno_esperado = []


         assert result == retorno_esperado


    @mock.patch('source.estoque.opencart.req_products_association', mock.MagicMock(return_value=[[21,551061,''],[23,156863,''],[24,778864,'']]))
    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={'sku': '123',
    'product_id': 102022,
    'model': 'Titan 150',
    'quantity_fisica':0,
    'quantity': 11}))
    def test_products_association_esta_vazio(self, m1):
         result = get_produtos_associados()
         retorno_esperado = []
         assert result == retorno_esperado


    @mock.patch('source.estoque.opencart.req_products_association', mock.MagicMock(return_value=[[21,551061,''],[23,156863,''],[24,778864,'']]))
    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={'sku': '123',
    'product_id': 102022,
    'model': 'Titan 150',
    'quanity_fisica': 0,
    'quantity': 11}))
    def testreq_products_associationthrow_exception(self, m1, mocker):
        mock_request = mocker.patch('source.estoque.opencart.req_products_association')
        with pytest.raises(Exception):
            get_produtos_associados()


    @mock.patch('source.estoque.opencart.req_products_association', mock.MagicMock(return_value=[[21,551061,''],[23,156863,''],[24,778864,'']]))
    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={'sku': '123',
    'product_id': 102022,
    'model': 'Titan 150',
    'quantity_fisica':0,
    'quantity': 11}))
    def testreq_products_associationthrow_exception(self, m1, mocker):
        mock_request = mocker.patch('source.estoque.opencart.req_products_association')
        mock_request.side_effect = Exception("Erro na Conexao")
        with pytest.raises(Exception):
            get_produtos_associados()
 
    @mock.patch('source.estoque.opencart.req_products_association', mock.MagicMock(return_value=[[21,551061,''],[23,156863,''],[24,778864,'']]))
    @mock.patch('source.estoque.opencart.req_oc_product', mock.MagicMock(return_value={'sku': '123',
    'product_id': 102022,
    'model': 'Titan 150',
    'quantity_fisica':0,
    'quantity': 11}))
    def testreq_oc_productthrow_exception(self, m1, mocker):
        mock_request = mocker.patch('source.estoque.opencart.req_oc_product')
        mock_request.side_effect = Exception("Erro na Conexao")
        with pytest.raises(Exception):
            get_oc_product()

    @requests_mock.Mocker()
    def test_put_successo(self, m1, m):
        m.put('http://mock-onlinemotopecas.com.br/api.php/oc_product/1', text='{"status":"OK"}')
        atualiza_saldo_oc(1,1,1)

    @requests_mock.Mocker()
    def test_put_error(self, m1, m):
        m.put('http://mock-onlinemotopecas.com.br/api.php/oc_product/1', text='Not Found', status_code=404)
        with pytest.raises(OcException):
            atualiza_saldo_oc(1,1,1)

    @requests_mock.Mocker()
    def test_put_bad_request(self, m1, m):
        MENSAGEM_ERRO = 'Bad Request bla bla'
        m.put('http://mock-onlinemotopecas.com.br/api.php/oc_product/1', text=MENSAGEM_ERRO, status_code=400)
        with pytest.raises(OcException) as ex:
            atualiza_saldo_oc(1,1,1)
            print(ex)
        assert ex.value[0] == MENSAGEM_ERRO


    @requests_mock.Mocker()
    def test_put_servidor_indisponivel(self, m1, m):
        Erro_no_Servidor = 'Servidor indisponivel'
        m.put('http://mock-onlinemotopecas.com.br/api.php/oc_product/1', text=Erro_no_Servidor, status_code=500)
        with pytest.raises(OcException) as ex:
            atualiza_saldo_oc(1,1,1)
        assert ex.value[0] == Erro_no_Servidor 


    @requests_mock.Mocker()
    def test_req_pov_erro_servidor(self, m1, m):
        Erro_no_Servidor = 'Servidor indisponivel'
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_option_value', text=Erro_no_Servidor, status_code=500)
        with pytest.raises(OcException) as ex:
            req_product_option_value(502892804)
        assert ex.value[0] == Erro_no_Servidor

    @requests_mock.Mocker()
    def test_req_pov_successo(self, m1, m):
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_option_value', text='{"status":"OK"}')
        req_product_option_value(502892804)