import requests_mock
import os
import pytest
import time
import datetime
import logging

from source.estoque.tiny import pedido_obter, atualizar_estoque_tiny
from source.pedidos.service import pesquisa_pedidos
from source.produtos.service import get_tiny_id_from_sku
from source.exceptions import TiException


class TestTiny:
    PREFIX = 'test/jsons/pedidos_pesquisa_para_envio_tiny/'

    @pytest.fixture
    def log(self):
        return logging.getLogger('mock')

    def test_load_env(self):
        tiny_url = os.getenv('TINY_URL_ATUALIZAR_ESTOQUE')
        print(tiny_url)
        assert 'http://mock-tiny.com.br/api2/produto.atualizar.estoque.php' == tiny_url
        tiny_url = os.getenv('TINY_URL_PEDIDOS_PESQUISA')
        assert 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php' == tiny_url

    @requests_mock.Mocker()
    def test_get_tiny_id_from_sku_sucesso(self, m):
        m.get('http://mock-tiny.com.br/api2/produtos.pesquisa.php', text='''{
            "retorno": {
                "status_processamento": 3,
                "status": "OK",
                "pagina": "1",
                "numero_paginas": "1",
                "produtos": [
                {
                    "produto": {
                    "id": 46829062,
                    "codigo": 123,
                    "nome": "produto teste",
                    "preco": "1.20",
                    "preco_promocional": "1.10",
                    "preco_custo": "1.05",
                    "preco_custo_medio": "1.02",
                    "unidade": "UN",
                    "tipoVariacao": "P"
                    }
                }
                ]
                }
            }''', status_code=200)
        retorno_metodo = get_tiny_id_from_sku('abc-123')
        assert retorno_metodo == 46829062

    @requests_mock.Mocker()    
    def test_get_product_multiple_products(self, m):
        SKU = 'abc-123'
        CHAMADA_API = 'http://mock-tiny.com.br/api2/produtos.pesquisa.php?token=123&formato=json&pesquisa=abc-123'
        MENSAGEM_ERRO = 'Retorno invalido SKU: {0}, Chamada: {1}'.format(SKU, CHAMADA_API)
        m.get(CHAMADA_API,
            text='''{"retorno": {
                "status_processamento": 3,
                "status": "OK",
                "pagina": "1",
                "numero_paginas": "1",
                "produtos": [
                {
                    "produto": {
                    "id": 46829062,
                    "codigo": "abc-123",
                    "nome": "produto teste",
                    "preco": "1.20",
                    "preco_promocional": "1.10",
                    "preco_custo": "1.05",
                    "preco_custo_medio": "1.02",
                    "unidade": "UN",
                    "tipoVariacao": "P"
                    }
                },
                {
                    "produto": {
                    "id": 46829022,
                    "codigo": 123,
                    "nome": "produto teste abc-123",
                    "preco": "1.20",
                    "preco_promocional": "1.10",
                    "preco_custo": "1.05",
                    "preco_custo_medio": "1.02",
                    "unidade": "UN",
                    "tipoVariacao": "P"
                    }
                }
                ]
                }
            }''', status_code=200)
        retorno_metodo = get_tiny_id_from_sku(SKU)
        assert retorno_metodo == 46829062
        
    @requests_mock.Mocker()    
    def test_get_multiplos_produtos_sem_codigo_equivalente(self, m):
        SKU = 'abc-123'
        CHAMADA_API = 'http://mock-tiny.com.br/api2/produtos.pesquisa.php?token=123&formato=json&pesquisa=abc-123'
        MENSAGEM_ERRO = 'Retorno invalido SKU: {0}, Chamada: {1}'.format(SKU, CHAMADA_API)
        m.get(CHAMADA_API,
            text='''{"retorno": {
                "status_processamento": 3,
                "status": "OK",
                "pagina": "1",
                "numero_paginas": "1",
                "produtos": [
                {
                    "produto": {
                    "id": 46829062,
                    "codigo": "abcccc123",
                    "nome": "produto teste 2 abc-123",
                    "preco": "1.20",
                    "preco_promocional": "1.10",
                    "preco_custo": "1.05",
                    "preco_custo_medio": "1.02",
                    "unidade": "UN",
                    "tipoVariacao": "P"
                    }
                },
                {
                    "produto": {
                    "id": 46829022,
                    "codigo": "123",
                    "nome": "produto teste 1 abc-123",
                    "preco": "1.20",
                    "preco_promocional": "1.10",
                    "preco_custo": "1.05",
                    "preco_custo_medio": "1.02",
                    "unidade": "UN",
                    "tipoVariacao": "P"
                    }
                }
                ]
                }
            }''', status_code=200)
        with pytest.raises(TiException) as ex:
            retorno_metodo = get_tiny_id_from_sku(SKU)
        assert ex.value[0] == MENSAGEM_ERRO
        
    @requests_mock.Mocker()    
    def test_get_product_bad_request(self, m):
        SKU = 'abc-123'
        CHAMADA_API = 'http://mock-tiny.com.br/api2/produtos.pesquisa.php?token=123&formato=json&pesquisa=abc-123'
        MENSAGEM_ERRO = 'A Consulta nao retornou registros'
        m.get(CHAMADA_API,
            text='''{
                    "retorno": {
                        "status_processamento": 2,
                        "status": "Erro",
                        "codigo_erro": 20,
                        "erros": [
                        {
                            "erro": "A Consulta nao retornou registros"
                        }
                        ]
                    }
                    }''', status_code=400)
        with pytest.raises(TiException) as ex:
            retorno_metodo = get_tiny_id_from_sku(SKU)
        assert ex.value[0] == MENSAGEM_ERRO
        
    @requests_mock.Mocker()    
    def test_get_product_multiple_bad_request(self, m):
        SKU = 'abc-123'
        CHAMADA_API = 'http://mock-tiny.com.br/api2/produtos.pesquisa.php?token=123&formato=json&pesquisa=abc-123'
        MENSAGEM_ERRO = 'A Consulta nao retornou registros, A sua mae e muito gorda, Chamada: {0}'.format(CHAMADA_API)
        m.get(CHAMADA_API,
            text='''{
                    "retorno": {
                        "status_processamento": 2,
                        "status": "Erro",
                        "codigo_erro": 20,
                        "erros": [
                        {
                            "erro": "A Consulta nao retornou registros"
                        },
                        {
                            "erro": "A sua mae e muito gorda"
                        }
                        ]
                    }
                    }''', status_code=400)
        with pytest.raises(TiException) as ex:
            get_tiny_id_from_sku(SKU)
            assert ex.value[0] == MENSAGEM_ERRO
        
    @requests_mock.Mocker()    
    def test_get_product_invalid_token(self, m):
        SKU = 'abc-123'
        CHAMADA_API = 'http://mock-tiny.com.br/api2/produtos.pesquisa.php?token=123&formato=json&pesquisa=abc-123'
        MENSAGEM_ERRO = 'token invalido'
        m.get(CHAMADA_API,
            text='''{
                    "retorno": {
                        "status_processamento": 1,
                        "status": "Erro",
                        "codigo_erro": 2,
                        "erros": [
                        {
                            "erro": "token invalido"
                        }
                        ]
                    }
                    }''', status_code=400)
        with pytest.raises(TiException) as ex:
            retorno_metodo = get_tiny_id_from_sku(SKU)
        assert ex.value[0] == MENSAGEM_ERRO

    @requests_mock.Mocker()
    def test_erro_servidor_tiny(self, m):
        date = time.strftime('%Y-%m-%d %H:%M:00', time.localtime())
        Erro_no_Servidor = 'Servidor indisponivel'
        product_id = 1
        id_tiny = 1
        saldo = 1
        payload  = '{"estoque":{"idProduto": "'+str(product_id)+'","tipo": "B","data":"'+str(date)+'" ,"quantidade":"'+str(saldo)+'","observacoes": "Lancamento via Cerberus"}}'
        m.post('http://mock-tiny.com.br/api2/produto.atualizar.estoque.php?token=123&formato=json&estoque='+payload, text=Erro_no_Servidor, status_code=500)
        with pytest.raises(TiException) as ex:
            atualizar_estoque_tiny(product_id, id_tiny, saldo)
        assert ex.value[0] == Erro_no_Servidor    

    @requests_mock.Mocker()
    def test_id_invalido_stokc_tiny(self, m):
        date = time.strftime('%Y-%m-%d %H:%M:00', time.localtime())
        Id_invalido = 'Id Nulo'
        product_id = None
        id_tiny = None
        saldo = 1
        TOKEN = os.getenv('TOKEN_TINY')
        payload = '{"estoque":{"idProduto": "'+str(product_id)+'","tipo": "B","data":"'+str(date)+'" ,"quantidade":"'+str(saldo)+'","observacoes": "Lancamento via Cerberus"}}'
        m.post('http://mock-tiny.com.br/api2/produto.atualizar.estoque.php?token={0}&formato=json&estoque={1}'.format(TOKEN, payload), text=Id_invalido )
        with pytest.raises(TiException) as ex:
            atualizar_estoque_tiny(product_id, id_tiny, saldo)
        assert ex.value[0] == Id_invalido 


    @requests_mock.Mocker()
    def test_sucesso_atualizar_estoque_tiny(self, m):
        date = time.strftime('%Y-%m-%d %H:%M:00', time.localtime())
        product_id = 1
        id_tiny = 1
        saldo = 1
        payload = '{"estoque":{"idProduto": "'+str(product_id)+'","tipo": "B","data":"'+str(date)+'" ,"quantidade":"'+str(saldo)+'","observacoes": "Lancamento via Cerberus"}}'
        m.post('http://mock-tiny.com.br/api2/produto.atualizar.estoque.php?token=123&formato=json&estoque='+payload,
               text='{"retorno":{"status_processamento": "3","status": "OK"}}')
        result = atualizar_estoque_tiny(product_id, id_tiny, saldo)
        assert result is True

  
    @requests_mock.Mocker()
    def test_pedidos_pesquisa_servidor_indisponivel(self, m):
        token = os.getenv('TINY_TOKEN')
        data_de_hoje = datetime.datetime.now().date()
        dois_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
        Erro_no_Servidor = 'Servidor indisponivel'
        m.get('http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=1'.format(token,dois_dias_atras),text=Erro_no_Servidor,status_code=500)
        with pytest.raises(TiException) as ex:
            pesquisa_pedidos()
        assert ex.value[0] == Erro_no_Servidor    



    @requests_mock.Mocker()
    def test_pedidos_pesquisa_sucesso(self, m):
        token = os.getenv('TINY_TOKEN')
        data_de_hoje = datetime.datetime.now().date()
        dois_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
        numero = 706973610
        ref_arquivo_obter = open(self.PREFIX + 'pedido.obter.json','r').read().decode('utf-8')
        ref_arquivo_pesquisar = open(self.PREFIX + 'pedidos.pesquisar.json','r').read().decode('utf-8')
        url = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=1'.format(token,dois_dias_atras)
        url_pag_2 = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=2'.format(token,dois_dias_atras)
        m.get('http://mock-tiny.com.br/api2/pedido.obter.php/?token={0}&formato=json&id={1}'.format( token,numero),text=ref_arquivo_obter)
        m.get(url,text=ref_arquivo_pesquisar)
        m.get(url_pag_2, text='A pagina tentando obter')

        #atrasados
        atrasados = (data_de_hoje - datetime.timedelta(days=31)).strftime('%d/%m/%Y')
        url_atrasados = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&situacao=faturado'.format(token, atrasados)
        nenhum_pedido = open(self.PREFIX + 'nenhum.pedido.json','r').read().decode('utf-8')
        m.get(url_atrasados,text=nenhum_pedido)

        retorno = [{
                'id': u'706973610',
                'valor':149.4,
                'data': u'08/09/2020',
                'nome_vendedor': u'Mercado Livre Normal',
                'frete': 0.0,
                'total_produtos': 149.4,
                'itens':[
                    {
                        'id_produto_tiny': u"492881003",
                        'quantidade': u"1.00",
                        'sku': u"91207-MAT-003",  
                        'valor_unitario': u"149.40",
                        "separacao_loja": "n",
                        "loja": 0
                    }
                ]
            }]
        result = pesquisa_pedidos()
        assert retorno == result



    @requests_mock.Mocker()
    def test_pedidos_pesquisa_2_itens(self, m):
        token = os.getenv('TINY_TOKEN')
        data_de_hoje = datetime.datetime.now().date()
        dois_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
        numero = 706973611
        ref_arquivo = open(self.PREFIX + 'pedidos.pesquisar_2_itens.json','r').read().decode('utf-8')
        ref_arquivo_obter = open(self.PREFIX + 'pedido.obter_outro.json','r').read().decode('utf-8')
        
        #atrasados
        atrasados = (data_de_hoje - datetime.timedelta(days=31)).strftime('%d/%m/%Y')
        url_atrasados = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&situacao=faturado'.format(token, atrasados)
        nenhum_pedido = open(self.PREFIX + 'nenhum.pedido.json','r').read().decode('utf-8')
        m.get(url_atrasados,text=nenhum_pedido)
        
        url = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=1'.format(token,dois_dias_atras)
        url_pag_2 = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=2'.format(token,dois_dias_atras)
        m.get('http://mock-tiny.com.br/api2/pedido.obter.php/?token={0}&formato=json&id={1}'.format( token,numero),text=ref_arquivo_obter)
        m.get(url,text=ref_arquivo)
        m.get(url_pag_2, text='A pagina tentando obter')
        retorno = [{
                'id':  u'706973611',
                'valor':149.4,
                'data': u'08/09/2020',
                'nome_vendedor': u'Mercado Livre Normal',
                'frete': 0.0,
                'total_produtos': 149.4,
                'itens':[ {
                    "id_produto_tiny": u"492881003",
                    "sku": u"91207-MAT-003",
                    "quantidade": u"1.00",
                    "valor_unitario": u"149.40",
                    "separacao_loja": "n",
                    "loja": 0
                },
                    {
                    "id_produto_tiny": u"121212",
                    "sku": u"teste",
                    "quantidade": u"5.00",
                    "valor_unitario": u"200.40",
                    "separacao_loja": "n",
                    "loja": 0
                }
                
                    ]
        }]

        result = pesquisa_pedidos()
        assert retorno == result

    def test_pedidos_pesquisa_2_pedidos_2_itens(self, log):
        token = os.getenv('TINY_TOKEN')
        data_de_hoje = datetime.datetime.now().date()
        dois_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
        numero = 706973611
        numero2 = 101010
        ref_arquivo_obter = open(self.PREFIX + 'pedido.obter_2_pedidos.json','r').read().decode('utf-8')
        ref_arquivo_obter_outro_id = open(self.PREFIX + 'pedido.obter_outro_id.json','r').read().decode('utf-8')
        ref_arquivo = open(self.PREFIX + 'pedidos.pesquisar_2_pedidos.json','r').read().decode('utf-8')
        url = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=1'.format(token,dois_dias_atras)
        url_pag_2 = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=2'.format(token,dois_dias_atras)
        with requests_mock.Mocker() as m:
            m.get('http://mock-tiny.com.br/api2/pedido.obter.php/?token={0}&formato=json&id={1}'.format( token,numero2),text=ref_arquivo_obter_outro_id)
            m.get('http://mock-tiny.com.br/api2/pedido.obter.php/?token={0}&formato=json&id={1}'.format( token,numero),text=ref_arquivo_obter)
            m.get(url,text=ref_arquivo)
            m.get(url_pag_2, text='A pagina tentando obter')

            #atrasados
            atrasados = (data_de_hoje - datetime.timedelta(days=31)).strftime('%d/%m/%Y')
            url_atrasados = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&situacao=faturado'.format(token, atrasados)
            nenhum_pedido = open(self.PREFIX + 'nenhum.pedido.json','r').read().decode('utf-8')
            m.get(url_atrasados,text=nenhum_pedido)

            retorno = [{
                    'id':  u'706973611',
                    'valor':149.4,
                    'data': u'08/09/2020',
                    'nome_vendedor': u'Mercado Livre Normal',
                    'total_produtos': 149.4,
                    'frete': 0.0,
                        "itens":[ {
                            "id_produto_tiny": u"492881003",
                            "sku": u"91207-MAT-003",
                            "quantidade": u"1.00",
                            "valor_unitario": u"149.40",
                            "separacao_loja": "n",
                            "loja": 0
                        },
                        {
                            "id_produto_tiny": u"121212",
                            "sku": u"teste",
                            "quantidade": u"5.00",
                            "valor_unitario": u"200.40",
                            "separacao_loja": "n",
                            "loja": 0
                        }
                        ]
                    },
                    {
                    'id':  u'101010',
                    'valor': 500.4,
                    'data': u'08/09/2020',
                    'nome_vendedor': u'B2W',
                    'total_produtos': 149.4,
                    'frete': 0.0,
                        "itens":[ {
                            "id_produto_tiny": u"12345",
                            "sku": u"91207-MAT-003",
                            "quantidade": u"1.00",
                            "valor_unitario": u"149.40",
                            "separacao_loja": "n",
                            "loja": 0
                        },
                        {
                            "id_produto_tiny": u"54321",
                            "sku": u"teste",
                            "quantidade": u"5.00",
                            "valor_unitario": u"200.40",
                            "separacao_loja": "n",
                            "loja": 0
                        }
                        ]
                    }
                    ]
            result = pesquisa_pedidos(logger=log)
            assert retorno == result

    def test_pedido_com_atrasos(self, log):

        token = os.getenv('TINY_TOKEN')
        data_de_hoje = datetime.datetime.now().date()
        dois_dias_atras = (data_de_hoje - datetime.timedelta(days=7)).strftime('%d/%m/%Y')
        atrasados = (data_de_hoje - datetime.timedelta(days=31)).strftime('%d/%m/%Y')
        numero = 706973611
        numero2 = 101010
        ref_arquivo_obter = open(self.PREFIX + 'pedido.obter_2_pedidos.json','r').read().decode('utf-8')
        ref_arquivo_obter_outro_id = open(self.PREFIX + 'pedido.obter_outro_id.json','r').read().decode('utf-8')
        ref_arquivo = open(self.PREFIX + 'pedidos.pesquisar_2_pedidos.json','r').read().decode('utf-8')
        pedido_dois_itens = open(self.PREFIX + 'pedidos.pesquisar_2_itens.json','r').read().decode('utf-8')
        url = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=1'.format(token,dois_dias_atras)
        url_pag_2 = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&pagina=2'.format(token,dois_dias_atras)
        url_atrasados = 'http://mock-tiny.com.br/api2/pedidos.pesquisa.php/?token={0}&formato=json&dataInicial={1}&situacao=faturado'.format(token, atrasados)
        with requests_mock.Mocker() as m:
            m.get('http://mock-tiny.com.br/api2/pedido.obter.php/?token={0}&formato=json&id={1}'.format( token,numero2),text=ref_arquivo_obter_outro_id)
            m.get('http://mock-tiny.com.br/api2/pedido.obter.php/?token={0}&formato=json&id={1}'.format( token,numero),text=ref_arquivo_obter)
            m.get(url, text=ref_arquivo)
            m.get(url_atrasados, text=pedido_dois_itens)
            m.get(url_pag_2, text='tentando obter')
            m.get(url_pag_2, text='A consulta Nao retornou registros')

            result = pesquisa_pedidos(logger=log)
            assert len(result) == 3
