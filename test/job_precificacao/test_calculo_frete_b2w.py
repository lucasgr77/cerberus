from os import access
import os
import mock
import requests_mock
from source.database import LocalStorage
from source.precificacao.models import ControleSku, sku_esta_habilitado
from ..base import BaseTest
import pytest
from source.precificacao.controller import calculo_preco_frete_b2w, calcula_preco_b2w



class TestCalculoFreteB2w(BaseTest):

    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()

    def test_preco_escapamento(self):
        produto = {
            "oc_product":{
                'length': 14,
                'width': 59,
                'height': 14,
                'weight': 4.52,
                'sku': 'ab10'
            },
            'preco_venda': 309.99
        }
        calculo = calcula_preco_b2w(produto)
        preco_venda_b2w = 309.99 * 1.175 + 5
        frete = 43.99
        assert calculo == str(round(preco_venda_b2w + frete, 2))

    def test_peso_cubico(self):
        produto = {
            "oc_product":{
                'length': 60,
                'width': 30,
                'height': 20,
                'weight': 0.2,
                'sku': 'aba'
            },
            'preco_venda': '100.00'
        }
        calculo = calcula_preco_b2w(produto)
        preco_venda_b2w = 100.00 * 1.175 + 5
        frete = 63.99
        assert calculo == str(round(preco_venda_b2w + frete, 2))


    def test_peso_maximo(self):
        produto = {
            "oc_product":{
                'length': 60,
                'width': 30,
                'height': 20,
                'weight': 29.2,
                'sku': 'aba'
            },
            'preco_venda': '100.00'
        }
        calculo = calcula_preco_b2w(produto)
        preco_venda_b2w = 122.5
        frete = 169.99
        assert calculo == str(round(preco_venda_b2w + frete, 2))


    def test_peso_minimo(self):
        produto = {
            "oc_product":{
                'length': 5,
                'width': 3,
                'height': 2,
                'weight': 0.01,
                'sku': 'aba'
            },
            'preco_venda': '100.00'
        }
        calculo = calcula_preco_b2w(produto)
        preco_venda_b2w = 122.5
        frete = 30.99
        assert calculo == str(round(preco_venda_b2w + frete, 2))

    
    def test_nao_processar_produto_desabilitado(self, mocker, session):
        peso_embalagem = mocker.patch('source.precificacao.controller.peso_embalagem_b2w')
        calculo = mocker.patch('source.precificacao.controller.calcula_preco_b2w')
        session.add(ControleSku(
            sku='abe10',
        ))
        session.commit()
        session.close()
        sku = 'abe10'
        habilitado = sku_esta_habilitado(sku)
        assert habilitado is False
        assert peso_embalagem.call_count == 0


    def test_produto_sem_preco(self, mocker, session):
        peso_embalagem = mocker.patch('source.precificacao.controller.peso_embalagem_b2w')
        calculo = mocker.patch('source.precificacao.controller.calcula_preco_b2w')
        produto = {
            "oc_product":{
                'length': 95,
                'width': 93,
                'height': 2,
                'weight': 0.50,
                'sku': 'abe10'
            },
            'preco_venda': '10.00'
        }
        session.add(ControleSku(
            sku='abe10',
        ))
        session.commit()
        session.close()
        sku = 'abe10'
        habilitado = sku_esta_habilitado(sku)
        calculo = calcula_preco_b2w(produto)
        assert habilitado is False
        assert peso_embalagem.call_count == 0
        assert calculo is None