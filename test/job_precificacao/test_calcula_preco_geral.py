# encoding: utf-8
from os import access
import pytest
import datetime
import mock
from ..base import BaseTest
from source.precificacao.controller import calcula_preco
from source.precificacao.models import PrecificacaoItem
from source.sistema.models import MercadoLivreToken
from source.database import LocalStorage
from source.produtos.models import ProdutoTiny

PFX_PRECIF = 'source.precificacao.'
INSERIDO = 0
CALCULADO_PRECO = 1
FINALIZADO = 2
ERRO = 9
OC_PRODUCT = {
            "product_id": 502893159,"model": "CG","sku": "16013-439-921","upc": "789946806397","ean": "7899468063978",
        "jan": "","isbn": "","mpn": "16013-439-921","location": "","quantity": 1,"stock_status_id": 5,
        "image": "catalog/produtos/rdamasio/29920/boia-carburador-cg-adaptavel-xr200-fan-125-0508-dannixx-0.jpg",
        "manufacturer_id": 19,"shipping": 1,"price": "10.6400","points": 0,"tax_class_id": 0,"date_available": "2020-11-06",
        "weight": "0.00900000","weight_class_id": 1,"length": "15.00000000","width": "13.00000000","height": "2.00000000","length_class_id": 1,
        "subtract": 1,"minimum": 1,"sort_order": 1,"status": 1,"viewed": 95,"date_added": "2020-11-06 00:00:00","date_modified": "2020-11-06 00:00:00",
        "quantity_fisica": 1,"price_ml": None,"price_b2w": None,"price_magalu": None,"date_target_price": None
}
ML_PRODUCT = {
    'cadastro_ml': {
    'category_id': 'MLB243178',
    'id': 'MLID1',
    'title': 'Produto'
}
}

class TestCalculaPrecoGeral:

    @mock.patch(PFX_PRECIF + 'opencart.get_oc_product_sku', return_value=OC_PRODUCT)
    @mock.patch(PFX_PRECIF + 'opencart.get_product_id_by_codproduto', return_value=9999)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_ml', return_value=4)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_b2w', return_value=5)
    @mock.patch(PFX_PRECIF + 'mercado_livre.get_cadastro_ml', return_value=ML_PRODUCT)
    def test_calcula_preco_sem_fixos(self, get_prod_sku,  get_mlid, get_oc_product, ml, b2w, mocker):
        produto = {
            "codproduto":1,
            "preco_compra":1,
            "preco_custo":2,
            "preco_venda":3,
            "data_alt_preco":'2021-01-15'
        }
        mocker.patch(PFX_PRECIF+ 'controller.get_id_ml', return_value='MLID1')
        precificacao = PrecificacaoItem(id=1, codproduto = 1, status=0)
        calcula_preco(produto, precificacao)
        assert precificacao.status == CALCULADO_PRECO
        assert precificacao.preco_ml == 4
        assert precificacao.preco_b2w == 5
        assert precificacao.preco_magalu == 3.48

    @mock.patch(PFX_PRECIF + 'opencart.get_product_id_by_codproduto', return_value=9999)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_b2w', return_value=5)
    @mock.patch(PFX_PRECIF + 'mercado_livre.get_cadastro_ml', return_value=ML_PRODUCT)
    def test_calcula_preco_ml_fixo(self, get_oc_product, b2w, cadastro_ml, mocker):
        price = 78.98
        OC_PRODUCT['price_ml'] = price
        mocker.patch(PFX_PRECIF + 'opencart.get_oc_product_sku', return_value=OC_PRODUCT)
        produto = {
            "codproduto":1,
            "preco_compra":1,
            "preco_custo":2,
            "preco_venda":3,
            "data_alt_preco":'2021-01-15'
        }
        mocker.patch(PFX_PRECIF+ 'controller.get_id_ml', return_value='MLID1')
        precificacao = PrecificacaoItem(id=1, codproduto = 1, status=0)
        calcula_preco(produto, precificacao)
        assert precificacao.status == CALCULADO_PRECO
        assert precificacao.preco_ml == price
        assert precificacao.preco_b2w == 5
        assert precificacao.preco_magalu == 3.48

    @mock.patch(PFX_PRECIF + 'opencart.get_product_id_by_codproduto', return_value=9999)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_ml', return_value=4)
    @mock.patch(PFX_PRECIF + 'mercado_livre.get_cadastro_ml', return_value=ML_PRODUCT)
    def test_calcula_preco_b2w_fixo(self, codproduto, ml, cadastro_ml, mocker):
        price = 75.42
        OC_PRODUCT['price_ml'] = None
        OC_PRODUCT['price_b2w'] = price
        OC_PRODUCT['price_magalu'] = price
        mocker.patch(PFX_PRECIF + 'opencart.get_oc_product_sku', return_value=OC_PRODUCT)
        produto = {
            "codproduto":1,
            "preco_compra":1,
            "preco_custo":2,
            "preco_venda":3,
            "data_alt_preco":'2021-01-15'
        }
        mocker.patch(PFX_PRECIF+ 'controller.get_id_ml', return_value='MLID1')
        precificacao = PrecificacaoItem(id=1, codproduto = 1, status=0)
        calcula_preco(produto, precificacao)
        assert precificacao.status == CALCULADO_PRECO
        assert precificacao.preco_ml == 4
        assert precificacao.preco_b2w == price
        assert precificacao.preco_magalu == price

    @mock.patch(PFX_PRECIF + 'opencart.get_product_id_by_codproduto', return_value=9999)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_ml', return_value=4)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_b2w', return_value=5)
    @mock.patch(PFX_PRECIF + 'mercado_livre.get_cadastro_ml', return_value=ML_PRODUCT)
    def test_calcula_todos_precos_fixo(self, codproduto, ml, b2w, cadastro_ml, mocker):
        price_ml = 12.34
        price_b2w = 98.76
        price_magalu = 3.48
        OC_PRODUCT['price_ml'] = price_ml
        OC_PRODUCT['price_b2w'] = price_b2w
        OC_PRODUCT['price_magalu'] = price_magalu
        mocker.patch(PFX_PRECIF + 'opencart.get_oc_product_sku', return_value=OC_PRODUCT)
        produto = {
            "codproduto":1,
            "preco_compra":1,
            "preco_custo":2,
            "preco_venda":3,
            "data_alt_preco":'2021-01-15'
        }
        mocker.patch(PFX_PRECIF+ 'controller.get_id_ml', return_value='MLID1')
        precificacao = PrecificacaoItem(id=1, codproduto = 1, status=0)
        calcula_preco(produto, precificacao)
        assert precificacao.status == CALCULADO_PRECO
        assert precificacao.preco_ml == price_ml
        assert precificacao.preco_b2w == price_b2w
        assert precificacao.preco_magalu == price_magalu

    @mock.patch(PFX_PRECIF + 'opencart.get_product_id_by_codproduto', return_value=9999)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_ml', return_value=4)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_b2w', return_value=5)
    @mock.patch(PFX_PRECIF + 'mercado_livre.get_cadastro_ml', return_value=ML_PRODUCT)
    def test_calcula_prazo_ultrapassado(self, codproduto, ml, b2w, cadastro_ml, mocker):
        price_ml = 12.34
        price_b2w = 98.76
        price_magalu = 5
        OC_PRODUCT['price_ml'] = price_ml
        OC_PRODUCT['price_b2w'] = price_b2w
        OC_PRODUCT['price_magalu'] = price_magalu
        OC_PRODUCT['date_target_price'] = '2000-01-01'
        mocker.patch(PFX_PRECIF + 'opencart.get_oc_product_sku', return_value=OC_PRODUCT)
        produto = {
            "codproduto":1,
            "preco_compra":1,
            "preco_custo":2,
            "preco_venda":3,
            "data_alt_preco":'2021-01-15'
        }
        mocker.patch(PFX_PRECIF+ 'controller.get_id_ml', return_value='MLID1')
        precificacao = PrecificacaoItem(id=1, codproduto = 1, status=0)
        calcula_preco(produto, precificacao)
        assert precificacao.status == CALCULADO_PRECO
        assert precificacao.preco_ml == 4
        assert precificacao.preco_b2w == 5
        assert precificacao.preco_magalu == 3.48

    @mock.patch(PFX_PRECIF + 'opencart.get_product_id_by_codproduto', return_value=9999)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_ml', return_value=4)
    @mock.patch(PFX_PRECIF + 'controller.calcula_preco_b2w', return_value=5)
    @mock.patch(PFX_PRECIF + 'mercado_livre.get_cadastro_ml', return_value=ML_PRODUCT)
    def test_calcula_preco_prazo_valido(self, codproduto, ml, b2w, cadastro_ml, mocker):
        price_ml = 12.34
        price_b2w = 98.76
        price_magalu = 100
        OC_PRODUCT['price_ml'] = price_ml
        OC_PRODUCT['price_b2w'] = price_b2w
        OC_PRODUCT['price_magalu'] = price_magalu
        OC_PRODUCT['date_target_price'] = '2050-01-01'
        mocker.patch(PFX_PRECIF + 'opencart.get_oc_product_sku', return_value=OC_PRODUCT)
        produto = {
            "codproduto":1,
            "preco_compra":1,
            "preco_custo":2,
            "preco_venda":3,
            "data_alt_preco":'2021-01-15'
        }
        precificacao = PrecificacaoItem(id=1, codproduto = 1, status=0)
        mocker.patch(PFX_PRECIF+ 'controller.get_id_ml', return_value='MLID1')
        calcula_preco(produto, precificacao)
        assert precificacao.status == CALCULADO_PRECO
        assert precificacao.preco_ml == price_ml
        assert precificacao.preco_b2w == price_b2w
        assert precificacao.preco_magalu == price_magalu