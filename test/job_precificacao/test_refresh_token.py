# encoding: utf-8
from os import access
import mock
import requests_mock
import pytest
import json
from pytest_mock import mocker
from source.jobs import job_operacoes
from ..base import BaseTest
from source.api import req_mercado_livre
from source.sistema.models import MercadoLivreToken
from source.database import LocalStorage
from source.precificacao.mercado_livre import consulta_valor_frete, MlException, consulta_taxa_comissao

PFX_ML = 'https://mock-api.mercadolibre.com/'
PFX_JSON = 'test/job_precificacao/jsons/'

class TestRefreshTokenMercadoLivre(BaseTest):
    count = 0
    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()

    def test_sem_token_salvo(self, session):
        assert len(session.query(MercadoLivreToken).all()) == 0
        with pytest.raises(Exception) as ex:
            retorno = req_mercado_livre('http://url-mock')
            assert ex.message == 'Favor configurar token do Mercado Livre no Banco!'

    def test_token_invalido(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'users/307700862/items/search',
                  text=self.call_back_api_bloqueada)
            m.post('https://mock-api.mercadolibre.com/oauth/token?grant_type=refresh_token&client_id=1&client_secret=secret&refresh_token=b',
                text=open(PFX_JSON + 'refresh_token.json').read())
            retorno = req_mercado_livre('https://mock-api.mercadolibre.com/users/307700862/items/search')
            assert 'results' in retorno
            assert m.call_count == 3
        LocalStorage.get_session()
        assert len(session.query(MercadoLivreToken).all()) == 1
        token = session.query(MercadoLivreToken).first()
        assert token.access_token == 'Bearer APP_USR-newtoken'
        assert token.refresh_token == 'TG-123-123'

    def call_back_api_bloqueada(self, request, context):
        if self.count == 0:
            self.count = 1
            context.status_code = 401
            return open(PFX_JSON + 'ml_401.json').read()
        context.status_code = 200
        return open(PFX_JSON + 'ml_itens.json').read()



    def test_frete_gratis_sucesso(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'items/MLB1528737098/shipping_options/free',
                text=open(PFX_JSON + 'test_frete_gratis_sucesso.json').read())
            retorno = consulta_valor_frete('MLB1528737098')
            assert retorno == 49.95
            assert m.call_count == 1


    def test_sem_frete_gratis(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'items/1528737098/shipping_options/free', status_code=404,
                text=open(PFX_JSON + 'sem_frete_gratis.json').read())
            retorno = consulta_valor_frete('1528737098')
            assert retorno == 0
            assert m.call_count == 1


    def test_frete_erro_400(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()
        with requests_mock.Mocker() as m:
            m.get(PFX_ML +'items/22532/shipping_options/free',
                text=open(PFX_JSON +'erro_consulta_frete.json').read())
            with pytest.raises(MlException) as ex:
                retorno = consulta_valor_frete('22532')
            assert ex.value[0] == "Erro na consulta do frete Mercado Livre, Message: invalid item s mode [MLB1528737091]=not_specified"


    def test_taxa_comissao_sucesso(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=55.7&category_id=MLB73311',
                text=open(PFX_JSON + 'consulta_taxa_comissao.json').read())
            retorno = consulta_taxa_comissao('MLB73311', 55.7)
            assert retorno == 11.68
            assert m.call_count == 1


    def test_taxa_comissao_erro(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=55.7&category_id=3311',
                text=open(PFX_JSON + 'consulta_taxa_comissao_erro.json').read())
            with pytest.raises(MlException) as ex:
                retorno = consulta_taxa_comissao('3311', 55.7)
            assert ex.value[0] == 'Erro no retorno consulta_taxa_comissao: Category_id parameter is invalid'


    def test_taxa_comissao_type_id_vazio(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=55.7&category_id=MLB73311',
                text=open(PFX_JSON + 'consulta_taxa_sem_gold_special.json').read())
            with pytest.raises(MlException) as ex:
                retorno = consulta_taxa_comissao('MLB73311', 55.7)
            assert ex.value[0] == 'Erro na consulta do frete Mercado Livre, ID Mercado Livre: MLB73311'