# encoding: utf-8
import pytest
import requests_mock
import json
from ..base import BaseTest
from source.database import LocalStorage
from source.produtos.models import ProdutoTiny
from source.precificacao.controller import execute
from source.precificacao.models import PrecificacaoItem
from source.sistema.models import MercadoLivreToken

PFX_PREC = 'source.precificacao.'
PFX_JSON = 'test/job_precificacao/jsons/'

STATUS_INSERIDO = 0
STATUS_CALCULADO_PRECO = 1
STATUS_FINALIZADO = 2
STATUS_ERRO = 9


class TestJobPrecificacao(BaseTest):

    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()

    @pytest.fixture
    def insert_data(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.add(ProdutoTiny(
            product_id=999,
            codproduto=1
        ))
        session.add(ProdutoTiny(
            product_id=51,
            codproduto=11
        ))
        session.commit()
        session.close()

    def test_produtos_ambiente_com_precificacao_fixa(self, mocker, log, insert_data, session):
        with requests_mock.Mocker() as m:
            m.get('https://mock-api.mercadolibre.com/items/MLID1',
                  text=open(PFX_JSON + 'produto_ml_sem_variacao.json').read())
            mk_get_precos_atualizados = mocker.patch(PFX_PREC + 'ambientesoft.get_precos_atualizados', return_value=[{
                'codproduto': 1,
                'preco_custo': 2,
                'preco_compra': 3,
                'preco_venda': 4,
                'data_alt_preco': '2021-12-12'
            }])
            mocker.patch(PFX_PREC + 'opencart.get_product_id_by_codproduto', return_value=1)
            mk_get_oc_product = mocker.patch(PFX_PREC + 'opencart.get_oc_product_sku', return_value={
                'sku': 'a',
                'product_id': 5,
                'price': 9,
                'price_ml': 10.0,
                'price_b2w': 11.0,
                'price_magalu': 12.00,
                'date_target_price': None,
                'height': 1,
                'weight': 2,
                'length': 3,
                'width': 4
            })
            mk_atualiza_preco = mocker.patch(PFX_PREC + 'controller.atualiza_preco')
            mocker.patch(PFX_PREC + 'opencart.ultimos_produtos_atualizados', return_value=[])
            mocker.patch(PFX_PREC+ 'controller.get_id_ml', return_value='MLID1')
            execute(log)
            precificacao = session.query(PrecificacaoItem).filter().first()
            assert precificacao.status == STATUS_FINALIZADO
            assert mk_get_precos_atualizados.call_count == 1
            assert mk_get_oc_product.call_count == 1
            assert mk_atualiza_preco.call_count == 1

    def test_produtos_ambiente_e_opencart_iguais(self, mocker, log, insert_data, session):
        with requests_mock.Mocker() as m:
            m.get('https://mock-api.mercadolibre.com/items/MLID1',
                  text=open(PFX_JSON + 'produto_ml_sem_variacao.json').read())
            mk_get_precos_atualizados = mocker.patch(PFX_PREC + 'ambientesoft.get_precos_atualizados', return_value=[{
                'codproduto': 1,
                'preco_custo': 2,
                'preco_compra': 3,
                'preco_venda': 4,
                'data_alt_preco': '2021-12-12'
            }])
            mocker.patch(PFX_PREC + 'opencart.get_product_id_by_codproduto', return_value=1)
            mk_get_oc_product = mocker.patch(PFX_PREC + 'opencart.get_oc_product_sku', return_value={
                'sku': 'a',
                'product_id': 5,
                'price': 9,
                'price_ml': 10.0,
                'price_b2w': 11.0,
                'price_magalu': 12.00,
                'date_target_price': None
            })
            mk_atualiza_preco = mocker.patch(PFX_PREC + 'controller.atualiza_preco')
            mocker.patch(PFX_PREC + 'opencart.ultimos_produtos_atualizados', return_value=[{
                'codproduto': 1,
                'product_id': 5,
                'preco_custo': 2,
                'preco_compra': 3,
                'preco_venda': 4,
                'data_alt_preco': '2021-12-12',
                'height': 1,
                'weight': 2,
                'length': 3,
                'width': 4
            }])
            mocker.patch(PFX_PREC+ 'controller.get_id_ml', return_value='MLID1')
            execute(log)
            precificacao = session.query(PrecificacaoItem).filter().first()
            assert precificacao.status == STATUS_FINALIZADO
            assert mk_get_precos_atualizados.call_count == 1
            assert mk_get_oc_product.call_count == 1
            assert mk_atualiza_preco.call_count == 1

    def test_produtos_ambiente_e_opencart_distintos(self, mocker, log, insert_data, session):
        with requests_mock.Mocker() as m:
            logger = mocker.patch('logging.getLogger')
            m.get('https://mock-api.mercadolibre.com/items/MLID1',
                  text=open(PFX_JSON + 'produto_ml_sem_variacao.json').read())
            mk_get_precos_atualizados = mocker.patch(PFX_PREC + 'ambientesoft.get_precos_atualizados', return_value=[{
                'codproduto': 1,
                'preco_custo': 2,
                'preco_compra': 3,
                'preco_venda': 4,
                'data_alt_preco': '2021-12-12'
            }])
            mocker.patch(PFX_PREC + 'opencart.get_product_id_by_codproduto', return_value=1)
            mk_get_oc_product = mocker.patch(PFX_PREC + 'opencart.get_oc_product_sku', return_value={
                'sku': 'a',
                'product_id': 5,
                'price': 9,
                'price_ml': 10.0,
                'price_b2w': 11.0,
                'price_magalu': 12.00,
                'date_target_price': None
            })
            mk_atualiza_preco = mocker.patch(PFX_PREC + 'controller.atualiza_preco')
            mocker.patch(PFX_PREC + 'opencart.ultimos_produtos_atualizados', return_value=[{
                'codproduto': 11,
                'product_id': 51,
                'preco_custo': 2,
                'preco_compra': 3,
                'preco_venda': 4,
                'data_alt_preco': '2021-12-12'
            }])
            mk_get_precos_amb = mocker.patch(PFX_PREC + 'ambientesoft.get_precos_by_codproduto', return_value={
                "codproduto": 11,
                "preco_custo": 99,
                "preco_compra": 98,
                "preco_venda": 97,
                "data_alt_preco": '2021-12-12',
                'height': 1,
                'weight': 2,
                'length': 3,
                'width': 4
            })
            mocker.patch(PFX_PREC+ 'controller.get_id_ml', return_value='MLID1')
            execute(logger)
            precificacao = session.query(PrecificacaoItem).filter().first()
            assert precificacao.status == STATUS_FINALIZADO
            assert mk_get_precos_atualizados.call_count == 1
            assert mk_get_oc_product.call_count == 2
            assert mk_atualiza_preco.call_count == 2
            assert mk_get_precos_amb.call_count == 1

    def test_precificacao_ml_com_variacao(self, mocker, log, insert_data, session):
        with requests_mock.Mocker() as m:
            m.get('https://mock-api.mercadolibre.com/items/MLID1',
                  text=open(PFX_JSON + 'produto_ml_com_variacao.json').read())
            m.put('https://mock-api.mercadolibre.com/items/MLID1', status_code=200,
                  text='{}')
            m.put('https://mock.skyhub.com.br/products/a', status_code=204,
                  text='{}')
            mk_get_precos_atualizados = mocker.patch(PFX_PREC + 'ambientesoft.get_precos_atualizados', return_value=[{
                'codproduto': 1,
                'preco_custo': 2,
                'preco_compra': 3,
                'preco_venda': 4,
                'data_alt_preco': '2021-12-12'
            }])
            mocker.patch(PFX_PREC + 'opencart.get_product_id_by_codproduto', return_value=1)
            mk_get_oc_product = mocker.patch(PFX_PREC + 'opencart.get_oc_product_sku', return_value={
                'sku': 'a',
                'product_id': 5,
                'price': 9,
                'price_ml': 0.0,
                'price_b2w': 0.0,
                'price_magalu': 0.00,
                'date_target_price': None,
                'height': 1,
                'weight': 2,
                'length': 3,
                'width': 4
            })
            mocker.patch(PFX_PREC + 'opencart.ultimos_produtos_atualizados', return_value=[])
            mocker.patch(PFX_PREC+ 'controller.get_id_ml', return_value='MLID1')
            mocker.patch(PFX_PREC + 'controller.calcula_preco_ml', return_value=40)
            mocker.patch(PFX_PREC + 'opencart.post_dc_fila_produtos', return_value=True)
            mocker.patch(PFX_PREC + 'controller.atualiza_preco_oc_product', return_value=True)
            execute(log)
            precificacao = session.query(PrecificacaoItem).filter().first()
            assert precificacao.status == STATUS_FINALIZADO
            assert mk_get_precos_atualizados.call_count == 1
            assert mk_get_oc_product.call_count == 1
            body_request = json.loads(m.request_history[1]._request.body)
            assert 40 == body_request['variations'][0]['price']

    def test_precificacao_ml_sem_variacao(self, mocker, log, insert_data, session):
        with requests_mock.Mocker() as m:
            logger = mocker.patch('logging.getLogger')
            m.get('https://mock-api.mercadolibre.com/items/MLID1',
                  text=open(PFX_JSON + 'produto_ml_sem_variacao.json').read())
            m.put('https://mock-api.mercadolibre.com/items/MLID1', status_code=200,
                  text='{}')
            m.put('https://mock.skyhub.com.br/products/a', status_code=204,
                  text='{}')
            mk_get_precos_atualizados = mocker.patch(PFX_PREC + 'ambientesoft.get_precos_atualizados', return_value=[{
                'codproduto': 1,
                'preco_custo': 2,
                'preco_compra': 3,
                'preco_venda': 4,
                'data_alt_preco': '2021-12-12'
            }])
            mocker.patch(PFX_PREC + 'opencart.get_product_id_by_codproduto', return_value=1)
            mk_get_oc_product = mocker.patch(PFX_PREC + 'opencart.get_oc_product_sku', return_value={
                'sku': 'a',
                'product_id': 5,
                'price': 9,
                'price_ml': 0.0,
                'price_b2w': 0.0,
                'price_magalu': 0.00,
                'date_target_price': None,
                'height': 1,
                'weight': 2,
                'length': 3,
                'width': 4
            })
            mocker.patch(PFX_PREC + 'opencart.ultimos_produtos_atualizados', return_value=[])
            mocker.patch(PFX_PREC+ 'controller.get_id_ml', return_value='MLID1')
            mocker.patch(PFX_PREC + 'controller.calcula_preco_ml', return_value=35)
            mocker.patch(PFX_PREC + 'opencart.post_dc_fila_produtos', return_value=True)
            mocker.patch(PFX_PREC + 'controller.atualiza_preco_oc_product', return_value=True)
            execute(logger)
            precificacao = session.query(PrecificacaoItem).filter().first()
            assert precificacao.status == STATUS_FINALIZADO
            assert mk_get_precos_atualizados.call_count == 1
            assert mk_get_oc_product.call_count == 1
            body_request = json.loads(m.request_history[1]._request.body)
            assert 35 == body_request['price']
