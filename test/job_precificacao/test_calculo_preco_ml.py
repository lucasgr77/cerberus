# encoding: utf-8
import os
import mock
import requests_mock
import pytest
import datetime
import time
import json
from ..base import BaseTest
from source.api import req_mercado_livre
from source.sistema.models import MercadoLivreToken
from source.database import LocalStorage
from source.precificacao.mercado_livre import MlException
from source.precificacao.controller import calcula_preco_ml,calcula_preco_b2w
from source.precificacao.opencart import ultimos_produtos_atualizados

PFX_ML = 'https://mock-api.mercadolibre.com/'
PFX_JSON = 'test/job_precificacao/jsons/json_calcula_preco_ml/'
PFX_PREC = 'source.precificacao.'

PRODUTO = {'codproduto': 4, 'data_alt_preco': datetime.date(2015, 6, 1), 'preco_compra': 16.00,
    'preco_custo': 15.00, 'preco_venda': 10.00, 'id_ml': 'MLB1697146279',
    'cadastro_ml': {
        'category_id': 'MLB243178',
        'id': 'MLB1697146279',
        'title': 'Produto'
    },
    'oc_product': {
        'length': 60,
        'width': 30,
        'height': 20,
        'weight': 0.2,
        'sku': 'aba'
    }
}
PRECIFICACAO = {'codproduto': 4, 'data_alteracao':'2020-12-28', 'id': 3, 'preco_b2w': None,
    'preco_compra': None, 'preco_custo': None, 'preco_magalu': None, 'preco_ml': None, 'preco_oc': None
}

class TestCalculaPrecoMercadoLivre(BaseTest):
    count = 0

    @pytest.fixture
    def session(self):
        return LocalStorage.get_session()

    @pytest.fixture
    def insert_data(self, session):
        session.add(MercadoLivreToken(
            access_token='a',
            refresh_token='b'
        ))
        session.commit()
        session.close()

    @pytest.fixture
    def dias_precificacao(self):
        dias_precificacao = float(os.getenv('DIAS_PRECIFICACAO'))
        hoje = datetime.datetime.now().date()
        data_comparacao = hoje.strftime('%Y-%m-%d')
        return (hoje - datetime.timedelta(days=dias_precificacao)).strftime('%Y-%m-%d')


    def test_calcula_preco_ml_sucesso(self,  insert_data):
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'sites/MLB/search?q=Produto', 
                text=open(PFX_JSON + 'consulta_concorrencia_vazio.json').read())
            m.get('http://mock-onlinemotopecas.com.br/api.php/products_association?filter=codproduto,eq,4',
                text=open(PFX_JSON + 'get_product_id.json').read())
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=10.0&category_id=MLB243178',
                text=open(PFX_JSON + 'consulta_taxa_comissao.json').read())
            m.get(PFX_ML + 'items/MLB1697146279/shipping_options/free',
                text=open(PFX_JSON + 'consulta_valor_frete.json').read())
            m.get(PFX_ML + 'items/MLB1186275172',
                text=open(PFX_JSON + 'get_cadastro_ml.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?q=Painel Titan 150 2009 Allen', 
                text=open(PFX_JSON + 'get_mlid_por_descricao.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?seller_sku=16013-439-921', 
                text=open(PFX_JSON + 'get_mlid_por_sku.json').read())
            retorno = calcula_preco_ml(PRODUTO, PRECIFICACAO)
            assert retorno == '25.80'
            assert m.call_count == 3

    def test_calcula_preco_ml_erro_mlid_sku(self,  insert_data):
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'sites/MLB/search?q=Produto', 
                text=open(PFX_JSON + 'consulta_concorrencia_vazio.json').read())
            m.get('http://mock-onlinemotopecas.com.br/api.php/products_association?filter=codproduto,eq,4',
                text=open(PFX_JSON + 'get_product_id.json').read())
            m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_description/502893159', 
                text=open(PFX_JSON + 'get_product_description.json').read())
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=10.0&category_id=MLB243178',
                text=open(PFX_JSON + 'consulta_taxa_comissao.json').read())
            m.get(PFX_ML + 'items/MLB1697146279/shipping_options/free',
                text=open(PFX_JSON + 'consulta_valor_frete.json').read())
            m.get(PFX_ML + 'items/MLB1710524484',
                text=open(PFX_JSON + 'get_cadastro_ml.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?seller_sku=16013-439-921', 
                text=open(PFX_JSON + 'erro_get_mlid_por_sku.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?q=Boia Carburador', 
                text=open(PFX_JSON + 'get_mlid_por_descricao.json').read())
            retorno = calcula_preco_ml(PRODUTO, PRECIFICACAO)
            assert retorno == '25.80'
            assert m.call_count == 3


    # def test_calcula_preco_ml_erro_mlid_por_descricao_erro_interno(self,  insert_data):
    #     PRODUTO = {'codproduto': 4, 'data_alt_preco': datetime.date(2015, 6, 1), 'preco_compra': 16.00, 
    #     'preco_custo': 15.00, 'preco_venda': 10.00, 'id_ml': 'MLB1186275172'}
    #     PRECIFICACAO = {'codproduto': 4, 'data_alteracao':'2020-12-28', 'id': 3, 'preco_b2w': None,
    #     'preco_compra': None, 'preco_custo': None, 'preco_magalu': None, 'preco_ml': None, 'preco_oc': None}
    #     OC_PRODUCT = {
    #          "product_id": 502893159,"model": "CG","sku": "16013-439-921","upc": "789946806397","ean": "7899468063978",
    #         "jan": "","isbn": "","mpn": "16013-439-921","location": "","quantity": 1,"stock_status_id": 5,
    #         "image": "catalog/produtos/rdamasio/29920/boia-carburador-cg-adaptavel-xr200-fan-125-0508-dannixx-0.jpg",
    #         "manufacturer_id": 19,"shipping": 1,"price": "10.6400","points": 0,"tax_class_id": 0,"date_available": "2020-11-06",
    #         "weight": "0.00900000","weight_class_id": 1,"length": "15.00000000","width": "13.00000000","height": "2.00000000","length_class_id": 1,
    #         "subtract": 1,"minimum": 1,"sort_order": 1,"status": 1,"viewed": 95,"date_added": "2020-11-06 00:00:00","date_modified": "2020-11-06 00:00:00",
    #         "quantity_fisica": 1,"price_ml": None,"price_b2w": None,"price_magalu": None,"date_target_price": None
    #     }
    #     with requests_mock.Mocker() as m:
    #         m.get('http://mock-onlinemotopecas.com.br/api.php/products_association?filter=codproduto,eq,4',
    #             text=open(PFX_JSON + 'get_product_id.json').read())
    #         m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_description/502893159', 
    #             text=open(PFX_JSON + 'get_product_description.json').read())
    #         m.get(PFX_ML + 'sites/MLB/listing_prices?price=10.0&category_id=MLB243178',
    #             text=open(PFX_JSON + 'consulta_taxa_comissao.json').read())
    #         m.get(PFX_ML + 'items/MLB1697146279/shipping_options/free',
    #             text=open(PFX_JSON + 'consulta_valor_frete.json').read())
    #         m.get(PFX_ML + 'items/MLB1186275172',
    #             text=open(PFX_JSON + 'get_cadastro_ml.json').read())
    #         m.get(PFX_ML + 'users/307700862/items/search?q=Boia Carburador', 
    #             text=open(PFX_JSON + 'erro_get_mlid_por_sku.json').read())
    #         m.get(PFX_ML + 'users/307700862/items/search?seller_sku=16013-439-921', 
    #             text=open(PFX_JSON + 'erro_get_mlid_por_sku.json').read(), status_code=403)
    #         with pytest.raises(MlException) as ex:
    #             retorno = calcula_preco_ml(PRODUTO, PRECIFICACAO, OC_PRODUCT)
    #         assert ex.value[0] == 'Erro chamada get_mlid_por_descricao, mensagem: The caller is not authorized to access this resource'


    # def test_calcula_preco_ml_erro_mlid_por_descricao_404(self,  insert_data):
    #     PRODUTO = {'codproduto': 4, 'data_alt_preco': datetime.date(2015, 6, 1), 'preco_compra': 16.00, 
    #     'preco_custo': 15.00, 'preco_venda': 10.00, 'id_ml': 'MLB1186275172'}
    #     PRECIFICACAO = {'codproduto': 4, 'data_alteracao':'2020-12-28', 'id': 3, 'preco_b2w': None,
    #     'preco_compra': None, 'preco_custo': None, 'preco_magalu': None, 'preco_ml': None, 'preco_oc': None}
    #     OC_PRODUCT = {
    #          "product_id": 502893159,"model": "CG","sku": "16013-439-921","upc": "789946806397","ean": "7899468063978",
    #         "jan": "","isbn": "","mpn": "16013-439-921","location": "","quantity": 1,"stock_status_id": 5,
    #         "image": "catalog/produtos/rdamasio/29920/boia-carburador-cg-adaptavel-xr200-fan-125-0508-dannixx-0.jpg",
    #         "manufacturer_id": 19,"shipping": 1,"price": "10.6400","points": 0,"tax_class_id": 0,"date_available": "2020-11-06",
    #         "weight": "0.00900000","weight_class_id": 1,"length": "15.00000000","width": "13.00000000","height": "2.00000000","length_class_id": 1,
    #         "subtract": 1,"minimum": 1,"sort_order": 1,"status": 1,"viewed": 95,"date_added": "2020-11-06 00:00:00","date_modified": "2020-11-06 00:00:00",
    #         "quantity_fisica": 1,"price_ml": None,"price_b2w": None,"price_magalu": None,"date_target_price": None
    #     }
    #     with requests_mock.Mocker() as m:
    #         m.get('http://mock-onlinemotopecas.com.br/api.php/products_association?filter=codproduto,eq,4',
    #             text=open(PFX_JSON + 'get_product_id.json').read())
    #         m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_description/502893159', 
    #             text=open(PFX_JSON + 'get_product_description.json').read())
    #         m.get(PFX_ML + 'sites/MLB/listing_prices?price=10.0&category_id=MLB243178',
    #             text=open(PFX_JSON + 'consulta_taxa_comissao.json').read())
    #         m.get(PFX_ML + 'items/MLB1697146279/shipping_options/free',
    #             text=open(PFX_JSON + 'consulta_valor_frete.json').read())
    #         m.get(PFX_ML + 'items/MLB1186275172',
    #             text=open(PFX_JSON + 'get_cadastro_ml.json').read())
    #         m.get(PFX_ML + 'users/307700862/items/search?q=Boia Carburador', 
    #             text=open(PFX_JSON + 'get_mlid_por_descricao_404.json').read())
    #         m.get(PFX_ML + 'users/307700862/items/search?seller_sku=16013-439-921', 
    #             text=open(PFX_JSON + 'erro_get_mlid_por_sku.json').read(), status_code=403)
    #         with pytest.raises(MlException) as ex:
    #             retorno = calcula_preco_ml(PRODUTO, PRECIFICACAO, OC_PRODUCT)
    #         assert ex.value[0] == 'Erro chamada get_mlid_por_descricao, mensagem: Nenhum produto encontrado'


    def test_calcula_preco_ml_frete_gratis(self,  insert_data):
        with requests_mock.Mocker() as m:
            m.get(PFX_ML + 'sites/MLB/search?q=Produto', 
                text=open(PFX_JSON + 'consulta_concorrencia_vazio.json').read())
            m.get('http://mock-onlinemotopecas.com.br/api.php/products_association?filter=codproduto,eq,4',
                text=open(PFX_JSON + 'get_product_id.json').read())
            m.get('https://onlinemotopecas.com.br/api.php/oc_product_description/502893159', 
                text=open(PFX_JSON + 'get_product_description.json').read())
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=10.0&category_id=MLB243178',
                text=open(PFX_JSON + 'consulta_taxa_comissao.json').read())
            m.get(PFX_ML + 'items/MLB1697146279/shipping_options/free', status_code=400,
                text=open(PFX_JSON + 'consulta_frete_sem_frete.json').read())
            m.get(PFX_ML + 'items/MLB1186275172',
                text=open(PFX_JSON + 'get_cadastro_ml.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?q=Painel Titan 150 2009 Allen', 
                text=open(PFX_JSON + 'get_mlid_por_descricao.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?seller_sku=16013-439-921', 
                text=open(PFX_JSON + 'get_mlid_por_sku.json').read())
            retorno = calcula_preco_ml(PRODUTO, PRECIFICACAO)
            assert retorno == '20.80'
            assert m.call_count == 3


    def test_calcula_preco_ml_frete_gratis_erro(self,  insert_data):
        with requests_mock.Mocker() as m:
            m.get('http://mock-onlinemotopecas.com.br/api.php/products_association?filter=codproduto,eq,4',
                text=open(PFX_JSON + 'get_product_id.json').read())
            m.get('https://onlinemotopecas.com.br/api.php/oc_product_description/502893159', 
                text=open(PFX_JSON + 'get_product_description.json').read())
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=10.0&category_id=MLB243178',
                text=open(PFX_JSON + 'consulta_taxa_comissao.json').read())
            m.get(PFX_ML + 'items/MLB1697146279/shipping_options/free',
                text=open(PFX_JSON + 'consulta_frete_erro_400.json').read())
            m.get(PFX_ML + 'items/MLB1186275172',
                text=open(PFX_JSON + 'get_cadastro_ml.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?q=Painel Titan 150 2009 Allen', 
                text=open(PFX_JSON + 'get_mlid_por_descricao.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?seller_sku=16013-439-921', 
                text=open(PFX_JSON + 'get_mlid_por_sku.json').read())
            with pytest.raises(MlException) as ex:
                retorno = calcula_preco_ml(PRODUTO, PRECIFICACAO)
            assert ex.value[0] == 'Erro na consulta do frete Mercado Livre, Message: invalid item s mode [MLB1528737091]=not_specified'


    def test_calcula_preco_ml_erro_taxa_comissao(self,  insert_data):
        with requests_mock.Mocker() as m:
            m.get('http://mock-onlinemotopecas.com.br/api.php/products_association?filter=codproduto,eq,4',
                text=open(PFX_JSON + 'get_product_id.json').read())
            m.get('https://onlinemotopecas.com.br/api.php/oc_product_description/502893159', 
                text=open(PFX_JSON + 'get_product_description.json').read())
            m.get(PFX_ML + 'sites/MLB/listing_prices?price=10.0&category_id=MLB243178',
                text=open(PFX_JSON + 'erro_taxa_comissao.json').read())
            m.get(PFX_ML + 'items/MLB1697146279/shipping_options/free',
                text=open(PFX_JSON + 'consulta_valor_frete.json').read())
            m.get(PFX_ML + 'items/MLB1186275172',
                text=open(PFX_JSON + 'get_cadastro_ml.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?q=Painel Titan 150 2009 Allen', 
                text=open(PFX_JSON + 'get_mlid_por_descricao.json').read())
            m.get(PFX_ML + 'users/307700862/items/search?seller_sku=16013-439-921', 
                text=open(PFX_JSON + 'get_mlid_por_sku.json').read())
            with pytest.raises(MlException) as ex:
                retorno = calcula_preco_ml(PRODUTO, PRECIFICACAO)
            assert ex.value[0] == 'Erro no retorno consulta_taxa_comissao: Category_id parameter is invalid'



    def test_calcula_preco_b2w_sucesso(self,  insert_data):
        PRODUTO = {
            'codproduto': 4,
            'frete':30.00,
            'data_alt_preco':datetime.date(2015, 6, 1),
            'preco_compra': 8.00,
            'preco_custo': 6.00,
            'preco_venda': 10.00,
            'cadastro_ml': {
                'category_id': 'MLB243178',
                'id': 'MLB1697146279',
                'title': 'Produto'
            },
            'oc_product': {
                'length': 60,
                'width': 30,
                'height': 20,
                'weight': 0.2,
                'sku': 'aba'
            }
        }
        PRECIFICACAO = {
            'codproduto': 4,
            'data_alteracao':'2020-12-28',
            'id': 3,
            'preco_b2w': None,
            'preco_compra': None,
            'preco_custo': None,
            'preco_magalu': None,
            'preco_ml': None,
            'preco_oc': None
        }
        retorno = calcula_preco_b2w(PRODUTO)
        assert retorno == '29.65'


    def test_calcula_preco_b2w_frete_0(self,  insert_data):
        retorno = calcula_preco_b2w(PRODUTO)
        assert retorno == '29.65'

    @mock.patch(PFX_PREC + 'opencart.get_codproduto_by_product_id', return_value=1)
    def test_ultimos_produtos_atualizados_nulo(self, p1, dias_precificacao, log):
        with requests_mock.Mocker() as m:
            m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product?filter=date_modified,gt,' + dias_precificacao,
                text=open(PFX_JSON + 'todos_valores_nulos.json').read())
            retorno = ultimos_produtos_atualizados(log)
            assert m.call_count == 1
            assert retorno == []


    @mock.patch(PFX_PREC + 'opencart.get_codproduto_by_product_id', return_value=1)
    def test_ultimos_produtos_atualizados_todos_produto(self, p1, dias_precificacao, log):
        with requests_mock.Mocker() as m:
            m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product?filter=date_modified,gt,' + dias_precificacao,
                text=open(PFX_JSON + 'todos_valores_cheios.json').read())
            retorno = ultimos_produtos_atualizados(log)
            assert m.call_count == 1
            assert retorno == [{
                'codproduto': 1,
                'product_id': 54,
                'price_ml':10.00,
                'price_b2w': 10.00,
                'price_magalu': 10.00
            }]

    @mock.patch(PFX_PREC + 'opencart.get_codproduto_by_product_id', return_value=1)
    def test_ultimos_produtos_atualizados_passando_um_produto(self, product_id, dias_precificacao, log):
         with requests_mock.Mocker() as m:
            m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product?filter=date_modified,gt,' + dias_precificacao,
                text=open(PFX_JSON + 'produtos_atualizados_um_produto.json').read())
            retorno = ultimos_produtos_atualizados(log)
            assert m.call_count == 1
            assert retorno == [{
                'codproduto': 1,
                'product_id': 54,
                'price_ml':None,
                'price_b2w': 10.00,
                'price_magalu': None
            }]



    @mock.patch(PFX_PREC + 'opencart.get_codproduto_by_product_id', return_value=1)
    def test_ultimos_produtos_atualizados_data_target_cheia(self, product_id, dias_precificacao, log):
         with requests_mock.Mocker() as m:
            m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product?filter=date_modified,gt,' + dias_precificacao,
                text=open(PFX_JSON + 'produtos_atualizados_data_target_cheio.json').read())
            retorno = ultimos_produtos_atualizados(log)
            assert m.call_count == 1
            assert retorno == []

    @mock.patch(PFX_PREC + 'opencart.get_codproduto_by_product_id', return_value=1)
    def test_atualizando_dois_produtos(self, product_id, dias_precificacao, log):
        with requests_mock.Mocker() as m:
            mock_data = 'http://mock-onlinemotopecas.com.br/api.php/oc_product?filter=date_modified,gt,' + dias_precificacao
            m.get(mock_data, text=open(PFX_JSON + 'atualizando_dois_produtos.json').read())
            retorno = ultimos_produtos_atualizados(log)
            assert m.call_count == 1
            assert retorno[0] == {
                'codproduto': 1,
                'product_id': 55,
                'price_ml':10.00,
                'price_b2w': 10.00,
                'price_magalu': 10.00
            }
            assert retorno[1] == {
                'codproduto': 1,
                'product_id': 56,
                'price_ml':20.00,
                'price_b2w': 20.00,
                'price_magalu': 20.00
            }