import pytest
import logging
from source.database import LocalStorage
from pytest_mock import mocker


class BaseTest:
    PATH_CERB_LOGGER = 'source.log_server.cerb_logger'
    PRODUCTS_ASSOCIADOS = 'source.estoque.opencart.get_produtos_associados'
    REQ_TINY = 'source.estoque.tiny.req_tiny'
    REQ_ML = 'source.estoque.mercado_livre.atualiza_saldo_ml_variacao'
    REQ_OPENCART = 'source.estoque.opencart.req_opencart'
    GET_SALDO = 'source.estoque.models.get_saldo_produto'
    GET_SALDO_V2 = 'source.estoque.models.get_saldo_vmd_cmd'
    GET_TINY_ID = 'source.estoque.tiny.get_tiny_id_from_sku'

    @pytest.fixture(autouse=True)
    def setup(self, mocker, monkeypatch):
        monkeypatch.setenv('OC_URL_CB_OPERATIONS',
                           'http://mock-onlinemotopecas.com.br/api.php/cb_operations')
        monkeypatch.setenv('MERCADOLIVRE_URL_BASE',
                           'https://mock-api.mercadolibre.com/')
        monkeypatch.setenv('MERCADOLIVRE_SECRET',
                           'secret')
        monkeypatch.setenv('MERCADOLIVRE_CLIENT_ID',
                           '1')
        monkeypatch.setenv('MERCADOLIVRE_SECRET',
                           'secret')
        monkeypatch.setenv('MERCADOLIVRE_URL_REFRESH_TOKEN',
                           'https://mock-api.mercadolibre.com/oauth/token?grant_type=refresh_token')
        self.__before_test__()
        yield
        self.__after_test__(mocker)

    def __before_test__(self):
        LocalStorage.connect(False)

    def __after_test__(self, mocker):
        mocker.stopall()
        LocalStorage.drop()

    @pytest.fixture
    def log(self):
        return logging.getLogger('mock')