# encoding: utf-8
import mock
import json
import requests_mock
import pytest
from ..base import BaseTest
from source.jobs import job_envio_ab_products

PFX_TINY = 'http://mock-tiny.com.br/api2/'
PFX_OC = 'http://mock-onlinemotopecas.com.br/api.php/'
PFX_JSON = 'test/job_operacao/jsons/'
GET_ALL_AB_PRODUCTS = 'source.produtos.models.get_all_ab_products'
GET_ALL_AB_ESTOQUE = 'source.produtos.models.get_all_estoque'

PFX_TINY = 'http://mock-tiny.com.br/api2/'
PFX_JSON = 'test/job_envio_ab_products/jsons/'


@mock.patch('source.extensions.__horario_aprovado__', return_value=True)
class TestEnvioAbProducts(BaseTest):

    data_ab_products = [{
        'CODPRODUTO': 1,
        'DESCRICAO': 'PARAL DIANT T 150 AZUL',
        'APLICACAO': 'TITAN 150',
        'CODBARRA': '1500150150',
        'CODIGO_FABRICA': '1021-412',
        'PRECO_COMPRA': 11.13,
        'PRECO_CUSTO': 14.15,
        'PRECO_VENDA': 15.15,
        'NCM': '87141000',
        'CEST': '0107600'
    }, {
        'CODPRODUTO': 2,
        'DESCRICAO': 'PARAL TRAS T 150 AZUL',
        'APLICACAO': 'TITAN 150',
        'CODBARRA': '1400140140',
        'CODIGO_FABRICA': '1022-411',
        'PRECO_COMPRA': 11.13,
        'PRECO_CUSTO': 14.15,
        'PRECO_VENDA': 15.15,
        'NCM': '87141000',
        'CEST': '0107600'
    }]
    data_ab_estoque = [{
        'CODFILIAL': 1,
        'CODPRODUTO': 1,
        'SALDO': 5,
    }, {
        'CODFILIAL': 7,
        'CODPRODUTO': 1,
        'SALDO': 2,
    }]

    @mock.patch(GET_ALL_AB_PRODUCTS, mock.MagicMock(return_value=[]))
    @mock.patch(GET_ALL_AB_ESTOQUE, mock.MagicMock(return_value=[]))
    def test_sem_envios(self, mocker):
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1,2', text='[1,1]')

            job_envio_ab_products()

            assert m.call_count == 0

    @mock.patch(GET_ALL_AB_PRODUCTS,
                mock.MagicMock(return_value=data_ab_products))
    @mock.patch(GET_ALL_AB_ESTOQUE,
                mock.MagicMock(return_value=data_ab_estoque))
    def test_envio_normal(self, mocker):
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1,2', text='[1,1]')
            m.put(PFX_OC + 'ab_estoque/1F1,1F7', text='[1,1]')

            job_envio_ab_products()

            assert m.call_count == 2
            assert '1,2' in m.request_history[0].path
            body_put = json.loads(m.request_history[0]._request.body)
            assert body_put[0]['NCM'] == '87141000'
            assert body_put[0]['CEST'] == '0107600'
            assert len(body_put) == 2
            assert '1f1,1f7' in m.request_history[1].path
            body_ab_estoque_post = json.loads(m.request_history[1]._request.body)
            assert len(body_ab_estoque_post) == 2

    @mock.patch(GET_ALL_AB_PRODUCTS, mock.MagicMock(return_value=data_ab_products))
    @mock.patch(GET_ALL_AB_ESTOQUE, mock.MagicMock(return_value=data_ab_estoque))
    def test_envio_um_produto_put_um_produto_post(self, mocker):
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1,2', text='[1,0]')
            m.post(PFX_OC + 'ab_products/', text='1')
            m.put(PFX_OC + 'ab_estoque/1F1,1F7', text='[1,0]')
            m.post(PFX_OC + 'ab_estoque/', text='1')

            job_envio_ab_products()

            assert m.call_count == 4
            assert '1,2' in m.request_history[0].path
            body_put = json.loads(m.request_history[0]._request.body)
            assert len(body_put) == 2
            body_post = json.loads(m.request_history[1]._request.body)
            assert body_post['CODPRODUTO'] == 2
            assert body_post['NCM'] == '87141000'
            assert body_post['CEST'] == '0107600'
            body_post = json.loads(m.request_history[3]._request.body)
            assert body_post['FILIAL'] == 7
            assert body_post['CODPRODUTO'] == 1

    @mock.patch(GET_ALL_AB_PRODUCTS, mock.MagicMock(return_value=data_ab_products))
    @mock.patch(GET_ALL_AB_ESTOQUE, mock.MagicMock(return_value=data_ab_estoque))
    def test_envio_dois_produtos_novos(self, mocker):
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1,2', text='[0,0]')
            m.post(PFX_OC + 'ab_products/', text='1')
            m.put(PFX_OC + 'ab_estoque/1F1,1F7', text='[0,0]')
            m.post(PFX_OC + 'ab_estoque/', text='1')

            job_envio_ab_products()

            assert m.call_count == 6
            assert '1,2' in m.request_history[0].path
            body_put = json.loads(m.request_history[0]._request.body)
            assert len(body_put) == 2
            body_post = json.loads(m.request_history[1]._request.body)
            assert body_post['CODPRODUTO'] == 1
            assert body_post['NCM'] == '87141000'
            assert body_post['CEST'] == '0107600'
            body_post = json.loads(m.request_history[2]._request.body)
            assert body_post['CODPRODUTO'] == 2
            assert body_post['NCM'] == '87141000'
            assert body_post['CEST'] == '0107600'
            body_post = json.loads(m.request_history[5]._request.body)
            assert body_post['FILIAL'] == 7
            assert body_post['CODPRODUTO'] == 1

    @mock.patch(GET_ALL_AB_PRODUCTS, mock.MagicMock(return_value=data_ab_products))
    @mock.patch(GET_ALL_AB_ESTOQUE, mock.MagicMock(return_value=data_ab_estoque))
    def test_erro_retorno_put(self, mocker):
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1,2', text=None, status_code=500)
            m.post(PFX_OC + 'ab_products/', text='1')
            m.put(PFX_OC + 'ab_estoque/1F1,1F7', text=None, status_code=500)
            m.post(PFX_OC + 'ab_estoque/', text='1')

            job_envio_ab_products()

            assert m.call_count == 4

    @mock.patch(GET_ALL_AB_PRODUCTS, mock.MagicMock(return_value=data_ab_products))
    @mock.patch(GET_ALL_AB_ESTOQUE, mock.MagicMock(return_value=data_ab_estoque))
    def test_erro_retorno_post(self, mocker):
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1,2', text='[0,0]')
            m.post(PFX_OC + 'ab_products/', text=None, status_code=500)
            m.put(PFX_OC + 'ab_estoque/1F1,1F7', text='[0,0]')
            m.post(PFX_OC + 'ab_estoque/', text=None, status_code=500)

            job_envio_ab_products()

            assert m.call_count == 10

    @mock.patch(GET_ALL_AB_PRODUCTS, mock.MagicMock(return_value=data_ab_products))
    @mock.patch(GET_ALL_AB_ESTOQUE, mock.MagicMock(return_value=data_ab_estoque))
    def teste_unico_produto(self, mocker):
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1', text='1')
            m.put(PFX_OC + 'ab_estoque/1F1,1F7', text='[1,1]')
            m.post(PFX_OC + 'ab_estoque/', text=None, status_code=200)

            job_envio_ab_products()

            assert m.call_count == 2


    @mock.patch(GET_ALL_AB_PRODUCTS, mock.MagicMock(return_value=data_ab_products))
    @mock.patch(GET_ALL_AB_ESTOQUE, mock.MagicMock(return_value=data_ab_estoque))
    def teste_unico_produto_zerado(self, mocker):
        exception_notifier = mocker.patch('source.extensions.exception_notifier')
        with requests_mock.Mocker() as m:
            m.put(PFX_OC + 'ab_products/1', text='0')
            m.post(PFX_OC + 'ab_products/', text=None, status_code=200)
            m.put(PFX_OC + 'ab_estoque/1F1,1F7', text='[1,1]')
            m.post(PFX_OC + 'ab_estoque/', text=None, status_code=200)

            job_envio_ab_products()

            assert m.call_count == 2