import pytest
import requests_mock
import mock

from source.database import LocalStorage
from source.estoque.tiny import confere_atualiza_cadastro
from source.produtos.models import ProdutoTiny


PFX_TINY = 'http://mock-tiny.com.br/api2/'
PFX_JSON = 'test/jsons/atualiza_cadastro_tiny/'


class TestAtualizaCadastro:

    @pytest.fixture(autouse=True)
    def setup(self):
        self.__before_test__()
        yield
        self.__after_test__()

    def __before_test__(self):
        LocalStorage.connect(False)

    def __after_test__(self):
        LocalStorage.drop()

    @mock.patch('source.produtos.models.get_ncm', mock.MagicMock(return_value='1'))
    @requests_mock.Mocker(kw='m')
    def test_atualiza_um_produto(self, m):
        session = LocalStorage.get_session()
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 0
        m.get(PFX_TINY + 'produtos.pesquisa.php?token=123&formato=json&pesquisa=17010003',
              text=open(PFX_JSON + 'produtos.pesquisa.json').read())
        m.get(PFX_TINY + 'produto.obter.php?token=123&formato=json&id=707064787',
        	text=open(PFX_JSON + 'obter.produto.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product/502892768',
              text=open(PFX_JSON + 'oc_product.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_description/502892768', 
              text=open(PFX_JSON + 'oc_product_description.json').read())
        m.post(PFX_TINY + 'produto.alterar.php?token=123&formato=json''', 
               text='''{"retorno":{"registros":[{"registro":{"status_processamento":3}}]}}''')
        confere_atualiza_cadastro([{                             
                'product_id' : 502892768,
                'quantity' : 1,
                'quantity_fisica': 1,
                'codproduto': 35,
                'sku': '17010003',
                'product_option_value_id': None
            }])
        assert m.call_count == 5
        request_enviado = m.request_history[4]._request.url
        request_enviado = request_enviado.replace('''%20%22''','"').replace('''%22''','"').replace('''%20''',' ')
        assert '"ncm":"1"' in request_enviado
        assert '"alturaEmbalagem":"2.00000000"' in request_enviado
        assert '"larguraEmbalagem":"20.00000000"' in request_enviado
        assert '"comprimentoEmbalagem":"10.00000000"' in request_enviado
        assert '"id":"707064787"' in request_enviado
        assert '"descricao_complementar":"Corrente de Transmissao para Moto"' in request_enviado
        assert '"descricao_complementar":"Corrente de Transmissao para Moto"' in request_enviado
        assert '"preco": 60.11' in request_enviado
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 1

    @mock.patch('source.produtos.models.get_ncm', mock.MagicMock(return_value='1'))
    @requests_mock.Mocker()
    def test_nao_tem_produtos_para_atualizar(self, m):
        session = LocalStorage.get_session()
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 0
        m.get(PFX_TINY + 'produtos.pesquisa.php?token=123&formato=json&pesquisa=17010003', 
              text=open(PFX_JSON + 'produtos.pesquisa.json').read())
        m.get(PFX_TINY + 'produto.obter.php?token=123&formato=json&id=707064787', 
              text=open(PFX_JSON + 'obter.produto.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product/502892768', 
              text=open(PFX_JSON + 'oc_product.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_description/502892768', 
              text=open(PFX_JSON + 'oc_product_description.json').read())

        m.post(PFX_TINY + 'produto.alterar.php?token=123&formato=json''', 
               text='''{"retorno":{"registros":[{"registro":{"status_processamento":3}}]}}''')
        confere_atualiza_cadastro([{                             
                'product_id' : 502892768,
                'quantity' : 1,
                'quantity_fisica': 1,
                'codproduto': 35,
                'sku': '17010003',
                'product_option_value_id': None
            }])

        assert m.call_count == 5

        confere_atualiza_cadastro([{                             
                'product_id' : 502892768,
                'quantity' : 1,
                'quantity_fisica': 1,
                'codproduto': 35,
                'sku': '17010003',
                'product_option_value_id': None
            }])
        assert m.call_count == 5
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 1

    @requests_mock.Mocker()
    def test_produto_ok_no_tiny(self, m):
        session = LocalStorage.get_session()
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 0
        m.get(PFX_TINY + 'produtos.pesquisa.php?token=123&formato=json&pesquisa=17010003', 
              text=open(PFX_JSON + 'produtos.pesquisa.json').read())
        m.get(PFX_TINY + 'produto.obter.php?token=123&formato=json&id=707064787', 
              text=open(PFX_JSON + 'obter.produto-ok.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product/502892768', 
              text=open(PFX_JSON + 'oc_product.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_description/502892768', 
              text=open(PFX_JSON + 'oc_product_description.json').read())
        m.post(PFX_TINY + 'produto.alterar.php?token=123&formato=json''', 
               text='''{"retorno":{"registros":[{"registro":{"status_processamento":3}}]}}''')
        confere_atualiza_cadastro([{                             
                'product_id' : 502892768,
                'quantity' : 1,
                'quantity_fisica': 1,
                'codproduto': 35,
                'sku': '17010003',
                'product_option_value_id': None
            }])

        assert m.call_count == 2
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 1

    @requests_mock.Mocker()
    def test_produto_com_opcao(self, m):
        session = LocalStorage.get_session()
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 0
        m.get(PFX_TINY + 'produtos.pesquisa.php?token=123&formato=json&pesquisa=17010003', 
              text=open(PFX_JSON + 'produtos.pesquisa.json').read())
        m.get(PFX_TINY + 'produto.obter.php?token=123&formato=json&id=707064787', 
              text=open(PFX_JSON + 'obter.produto-ok.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product/502892768', 
              text=open(PFX_JSON + 'oc_product.json').read())
        m.get('http://mock-onlinemotopecas.com.br/api.php/oc_product_description/502892768', 
              text=open(PFX_JSON + 'oc_product_description.json').read())
        m.post(PFX_TINY + 'produto.alterar.php?token=123&formato=json''', 
               text='''{"retorno":{"registros":[{"registro":{"status_processamento":3}}]}}''')
        confere_atualiza_cadastro([{                             
                'product_id' : 502892768,
                'quantity' : 1,
                'quantity_fisica': 1,
                'codproduto': 35,
                'sku': '17010003',
                'product_option_value_id': 1
            },{                             
                'product_id' : 502892768,
                'quantity' : 1,
                'quantity_fisica': 1,
                'codproduto': 35,
                'sku': '17010003',
                'product_option_value_id': 2
            },{                             
                'product_id' : 502892768,
                'quantity' : 1,
                'quantity_fisica': 1,
                'codproduto': 35,
                'sku': '17010003',
                'product_option_value_id': 3
            },])
        
        assert m.call_count == 2
        produtos_tiny = session.query(ProdutoTiny).all()
        assert len(produtos_tiny) == 1
        