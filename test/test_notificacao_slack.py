import mock
import pytest
import requests
import requests_mock
import time
import json

from source.notify.slack import mensagem_slack, mensagens_separacao_slack, SlackException


URL_SLACK = 'http://mock-slack.com'

class TesteNotificacao:


    @requests_mock.Mocker() 
    def test_mensagem_slack_exception(self, m):
        mensagem_envio_esperada = 'Teste sucesso'
        Erro_no_Servidor = 'Servidor indisponivel'
        m.post(URL_SLACK, text=Erro_no_Servidor,status_code=500)
        with pytest.raises(SlackException) as ex:
            mensagem_slack(mensagem_envio_esperada)
        assert ex.value[0] == Erro_no_Servidor    


    @requests_mock.Mocker()
    def test_mensagem_slack_sucesso(self, m):
        mensagem_envio_esperada = 'Teste sucesso'
        m.post(URL_SLACK, status_code=200)
        mensagem_slack(mensagem_envio_esperada)
        body_enviado_slack = json.loads(m._adapter.request_history[0]._request.body)
        assert m.call_count == 1
        assert body_enviado_slack['text'] == mensagem_envio_esperada


    def test_chamada_separacao_slack_sucesso(self, mocker):
        mock_envio_mensagem = mocker.patch('source.notify.slack.mensagem_slack')
        nome_peca = 'A'
        quantidade = 1.0
        localizacao = 'C'
        loja_separacao = 'D'
        mensagens_separacao_slack(nome_peca, quantidade, localizacao, loja_separacao)
        assert mock_envio_mensagem.call_count == 1
        assert nome_peca in mock_envio_mensagem.call_args[0][0]
        assert '1' in mock_envio_mensagem.call_args[0][0]
        assert localizacao in mock_envio_mensagem.call_args[0][0]
        assert loja_separacao in mock_envio_mensagem.call_args[0][0]

    def test_chamada_separacao_slack_vazio(self, mocker):
        mock_envio_mensagem = mocker.patch('source.notify.slack.mensagem_slack')
        Erro = 'String Vazia'
        nome_peca = ''
        quantidade = ''
        localizacao = ''
        loja_separacao = ''
        with pytest.raises(SlackException) as ex:
            mensagens_separacao_slack(nome_peca, quantidade, localizacao, loja_separacao)
            assert nome_peca in mock_envio_mensagem.call_args[0][0]
            assert quantidade in mock_envio_mensagem.call_args[0][0]
            assert localizacao in mock_envio_mensagem.call_args[0][0]
            assert loja_separacao in mock_envio_mensagem.call_args[0][0]
        assert ex.value[0] == Erro 


    def test_chamada_separacao_slack_nulo(self, mocker):
        mock_envio_mensagem = mocker.patch('source.notify.slack.mensagem_slack')
        Erro = 'String Vazia'
        nome_peca = None
        quantidade = None
        localizacao = None
        loja_separacao = None
        with pytest.raises(SlackException) as ex:
            mensagens_separacao_slack(nome_peca, quantidade, localizacao, loja_separacao)
            assert nome_peca in mock_envio_mensagem.call_args[0][0]
            assert quantidade in mock_envio_mensagem.call_args[0][0]
            assert localizacao in mock_envio_mensagem.call_args[0][0]
            assert loja_separacao in mock_envio_mensagem.call_args[0][0]
        assert ex.value[0] == Erro
