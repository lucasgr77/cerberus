import requests_mock
import pytest
import json
from source.job_liberacao_pedido.controller import liberar_pedidos

URL_BASE = 'http://mock-onlinemotopecas.com.br/api.php'
PFX_JSON = 'test/job_liberar_pedido/jsons'


class TestLiberarPedido:

    def test_liberar_pedido(self, monkeypatch):
        monkeypatch.setenv('URL_SLACK_IMPORTACAO_VENDAS', 'http://mock')
        STATUS_DESBLOQUEADO = 2
        with requests_mock.Mocker() as m:
            m.get('{0}/oc_order?filter=order_status_id,eq,0'.format(URL_BASE),
                  text=open('{0}/consulta.json'.format(PFX_JSON)).read())
            m.put('{0}/oc_order/236'.format(URL_BASE),
                  text='1')
            m.post('http://mock')
            liberar_pedidos()

            assert m.call_count == 3
            body_envio = json.loads(m._adapter.request_history[1].text)
            assert body_envio['order_status_id'] == STATUS_DESBLOQUEADO

    def test_sem_pedidos_a_liberar(self):
        with requests_mock.Mocker() as m:
            m.get('{0}/oc_order?filter=order_status_id,eq,0'.format(URL_BASE),
                  text=open('{0}/consulta_vazio.json'.format(PFX_JSON)).read())
            liberar_pedidos()

            assert m.call_count == 1

    def test_erro_ao_liberar(self):
        STATUS_DESBLOQUEADO = 2
        with requests_mock.Mocker() as m:
            m.get('{0}/oc_order?filter=order_status_id,eq,0'.format(URL_BASE),
                  text=open('{0}/consulta.json'.format(PFX_JSON)).read())
            m.put('{0}/oc_order/236'.format(URL_BASE),
                  status_code=500, text='Erro esperado')
            with pytest.raises(Exception) as ex:
                liberar_pedidos()
            assert ex is not None
            assert ex.value.message == 'Erro esperado'
            assert ex.typename == 'OcException'
            assert m.call_count == 3

    def test_erro_ao_consultar_pedido(self):
        STATUS_DESBLOQUEADO = 2
        with requests_mock.Mocker() as m:
            m.get('{0}/oc_order?filter=order_status_id,eq,0'.format(URL_BASE),
                  status_code=500, text='Erro esperado')
            with pytest.raises(Exception) as ex:
                liberar_pedidos()
            assert ex is not None
            assert ex.value.message == 'Erro esperado'
            assert ex.typename == 'OcException'
            assert m.call_count == 2
